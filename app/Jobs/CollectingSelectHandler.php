<?php

namespace App\Jobs;

use App\Events\ApplicationStored;
use App\Helpers\AddLead\Field\Email;
use App\Helpers\AddLead\Field\Phone;
use App\Models\Application;
use App\Models\Site;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Validation\Validator;
use InvalidArgumentException;
use RuntimeException;
use Throwable;

/**
 * Class CollectingSelectHandler
 * @package App\Jobs
 */
class CollectingSelectHandler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public const QUEUE = 'collecting';
    private const ERROR_DELAY = 600;
    private const WAIT_DELAY = 60;

    /**
     * @var Site
     */
    private $site;

    /**
     * Create a new job instance.
     *
     * @param Site $site
     */
    public function __construct(Site $site)
    {
        $this->site = $site;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $site = $this->site;

        try {
            $sourceIds = $this->process($site);
            if (empty($sourceIds)) {
                $site->handled_at = Carbon::now()->addSeconds(self::WAIT_DELAY);
            }
        } catch (Throwable $exception) {
            report($exception);
            $sourceIds = [];
            $site->handled_at = Carbon::now()->addSeconds(self::ERROR_DELAY);
        }

        try {
            $site->saveOrFail();
        } catch (Throwable $exception) {
            report($exception);
        }

        if (empty($sourceIds)) {
            CollectingObserver::dispatch()->onQueue(CollectingObserver::QUEUE);
            return;
        }

        CollectingConfirmHandler::dispatch($site, $sourceIds)->onQueue(CollectingConfirmHandler::QUEUE);
    }

    /**
     * @param \App\Models\Site $site
     * @return int[]
     * @throws Throwable
     */
    private function process(Site $site): array
    {
        $client = new Client();

        try {
            $response = $client->post($site->url, [
                RequestOptions::FORM_PARAMS => [
                    'action' => 'select',
                    'password' => $site->password,
                ],
            ]);
        } catch (Throwable $exception) {
            throw $exception;
        }

        $json = $response->getBody()->getContents();

        try {
            $data = \GuzzleHttp\json_decode($json, true);
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (! is_array($data)) {
            throw new RuntimeException("Invalid data from site with ID = {$site->id}");
        }

        try {
            $validator = $this->getValidator();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $sourceIds = [];
        $values = [];
        foreach ($data as $attributes) {
            if (! is_array($attributes)) {
                throw new RuntimeException("Invalid element from site with ID = {$site->id}");
            }

            $validator->setData($attributes);
            if ($validator->fails()) {
                $errors = $validator->errors();
                if ($errors->has('id')) {
                    throw new InvalidArgumentException("Invalid element from site with ID = {$site->id}");
                }

                $id = $attributes['id'];
                $sourceIds[] = $id;

                $message = sprintf(
                    'Invalid element with ID = %d from site with ID = %d. Invalid attributes: %s',
                    $id,
                    $site->id,
                    implode(', ', $errors->keys())
                );
                $exception = new InvalidArgumentException($message);
                report($exception);
                continue;
            }

            $values[] = $attributes;
        }

        foreach ($values as $attributes) {
            $sourceId = $attributes['id'];
            unset($attributes['id']);
            $attributes['submitted_at'] = Carbon::parse($attributes['submitted_at']);

            try {
                Phone::createIdentity($attributes['sender_phone']);
                $attributes['sender_phone'] = Phone::clear($attributes['sender_phone']);
            } catch (\Throwable $e) {
                $attributes['sender_phone'] = null;
            }

            if (is_string($attributes['sender_email'])) {
                $attributes['sender_email'] = Email::createIdentity($attributes['sender_email']);
            }

            $application = new Application();
            $application->site_id = $site->id;
            $application->source_id = $sourceId;
            if (array_key_exists('extra_fields', $attributes)) {
                $extraFields = json_decode($attributes['extra_fields'], true);
                unset($attributes['extra_fields']);
                $application->extra_fields = $extraFields;
            }
            $application->fill($attributes);

            try {
                $application->saveOrFail();
                event(new ApplicationStored($application));
            } catch (Throwable $exception) {
                report($exception);
                continue;
            }

            $sourceIds[] = $sourceId;
        }

        return $sourceIds;
    }

    /**
     * @return \Illuminate\Validation\Validator
     * @throws \Throwable
     */
    private function getValidator() : Validator
    {
        try {
            $validator = resolve('validator.application');
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $rules = $validator->getRules();
        unset($rules['sender_vk']);
        $validator->setRules($rules);

        return $validator;
    }


    /**
     * @return array
     */
    public function tags()
    {
        return [
            'collecting',
            'select-handler',
            'site_id: ' . $this->site->id,
        ];
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     */
    public function failed(Throwable $exception)
    {
        CollectingObserver::dispatch()->onQueue(CollectingObserver::QUEUE);
    }
}
