<?php

namespace App\Jobs\ExternalApplications;

use App;
use App\Services\ExternalApplications\ExternalApplicationsCollector;
use App\Services\ExternalApplications\Sources\Bitrix24Telephony as Source;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;

/**
 * Class Bitrix24TelephonyCollector
 * @package App\Jobs\ExternalApplications
 */
class Bitrix24TelephonyCollector implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    public const QUEUE = 'external-applications';

    /**
     * @var int
     */
    public const DELAY_BETWEEN_JOBS = 30;

    /**
     * @var int
     */
    public $timeout = 90;

    /**
     * Execute the job.
     *
     * @param \App\Services\ExternalApplications\Sources\Bitrix24Telephony $applicationsSource
     * @return void
     */
    public function handle(Source $applicationsSource)
    {
        /** @var \App\Services\ExternalApplications\ExternalApplicationsCollector $collector */
        $collector = App::make(ExternalApplicationsCollector::class);

        $collector->setSource($applicationsSource);
        $collector->process();

        static::dispatch()->onQueue(static::QUEUE)->delay(self::DELAY_BETWEEN_JOBS);
    }

    /**
     * @return array
     */
    public function tags()
    {
        return [
            'external-applications',
            'bitrix24telephony',
        ];
    }

    /**
     * @param \Throwable|null $exception
     */
    public function failed(Throwable $exception = null)
    {
        static::dispatch()->onQueue(static::QUEUE);
    }
}
