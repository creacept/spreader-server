<?php

namespace App\Jobs;

use App\Helpers\AddLead\Algorithm;
use App\Models\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Carbon;
use Throwable;

/**
 * Class ApplicationHandler
 * @package App\Jobs
 */
class ApplicationHandler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public const QUEUE = 'application';
    private const ERROR_DELAY = 600;

    /**
     * @var Application
     */
    private $application;

    /**
     * Create a new job instance.
     *
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Execute the job.
     *
     * @param Algorithm $algorithm
     * @return void
     */
    public function handle(Algorithm $algorithm)
    {
        $application = $this->application;

        $algorithm->setApplication($application);
        $algorithm->setCrm($application->site->project->crm);
        try {
            $algorithm->execute();
            $application->status = Application::SUCCESS_STATUS;
        } catch (Throwable $exception) {
            report($exception);
            $application->handled_at = Carbon::now()->addSeconds(self::ERROR_DELAY);
        }

        try {
            $application->saveOrFail();
        } catch (Throwable $exception) {
            report($exception);
        }

        ApplicationObserver::dispatch()->onQueue(ApplicationObserver::QUEUE);
    }

    /**
     * @return array
     */
    public function tags()
    {
        return [
            'application',
            'handler',
            'id: ' . $this->application->id,
        ];
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     */
    public function failed(Throwable $exception)
    {
        ApplicationObserver::dispatch()->onQueue(ApplicationObserver::QUEUE);
    }
}
