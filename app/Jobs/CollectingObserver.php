<?php

namespace App\Jobs;

use App\Models\Application;
use App\Models\Site;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Carbon;
use Throwable;

/**
 * Class CollectingObserver
 * @package App\Jobs
 */
class CollectingObserver implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public const QUEUE = 'collecting';
    private const WAIT_DELAY = 60;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $site = Site::wherePublish(true)
                ->whereType(Site::SITE_TYPE)
                ->where('handled_at', '<=', Carbon::now())
                ->whereHas('project', function (Builder $query) {
                    $query->where('publish', true);
                })
                ->orderBy('handled_at')
                ->first();
        } catch (Throwable $exception) {
            report($exception);
            $site = null;
        }

        if ($site instanceof Site) {
            try {
                $this->runHandler($site);
                return;
            } catch (Throwable $exception) {
                report($exception);
            }
        }

        static::dispatch()->onQueue(static::QUEUE)->delay(self::WAIT_DELAY);
    }


    /**
     * @param Site $site
     * @throws Throwable
     */
    private function runHandler(Site $site) : void
    {
        try {
            $sourceIds = Application::select(['source_id'])
                ->whereSiteId($site->id)
                ->whereStatus(Application::RAW_STATUS)
                ->getQuery()
                ->pluck('source_id');
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ($sourceIds->isEmpty()) {
            CollectingSelectHandler::dispatch($site)->onQueue(CollectingSelectHandler::QUEUE);
            return;
        }

        $sourceIds = $sourceIds->toArray();
        CollectingConfirmHandler::dispatch($site, $sourceIds)->onQueue(CollectingConfirmHandler::QUEUE);
    }

    /**
     * @return array
     */
    public function tags()
    {
        return [
            'collecting',
            'observer',
        ];
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     */
    public function failed(Throwable $exception)
    {
        static::dispatch()->onQueue(static::QUEUE);
    }
}
