<?php

namespace App\Jobs;

use App\Models\Application;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Carbon;
use Throwable;

/**
 * Class ApplicationObserver
 * @package App\Jobs
 */
class ApplicationObserver implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public const QUEUE = 'application';
    private const WAIT_DELAY = 30;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $projectKeyName = (new Project())->getKeyName();
            $projectIds = Project::select([$projectKeyName])
                            ->wherePublish(true)
                            ->getQuery()
                            ->pluck($projectKeyName);

            $application = Application::whereStatus(Application::CONFIRMED_STATUS)
                ->where('handled_at', '<=', Carbon::now())
                ->whereHas('site', function (Builder $query) use ($projectIds) {
                    $query
                        ->where('publish', true)
                        ->whereIn('project_id', $projectIds);
                })
                ->orderBy('handled_at')
                ->orderBy('priority', 'desc')
                ->first();
        } catch (Throwable $exception) {
            report($exception);
            $application = null;
        }

        if ($application instanceof Application) {
            ApplicationHandler::dispatch($application)->onQueue(ApplicationHandler::QUEUE);
            return;
        }

        static::dispatch()->onQueue(static::QUEUE)->delay(self::WAIT_DELAY);
    }

    /**
     * @return array
     */
    public function tags()
    {
        return [
            'application',
            'observer',
        ];
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     */
    public function failed(Throwable $exception)
    {
        static::dispatch()->onQueue(static::QUEUE);
    }
}
