<?php

namespace App\Jobs\EveCalls;

use App\Events\ApplicationStored;
use App\Helpers\AddLead\Field\Phone;
use App\Models\Application;
use App\Models\EveCalls\Import as Model;
use App\Models\EveCalls\ImportError;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Row;
use PhpOffice\PhpSpreadsheet\Worksheet\RowIterator;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class Import
 * @package App\Jobs\EveCalls
 */
class Import implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\Models\EveCalls\Import
     */
    private $import;

    /**
     * @var \Illuminate\Validation\Validator
     */
    private $validator;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    /**
     * Create a new job instance.
     *
     * @param \App\Models\EveCalls\Import $import
     */
    public function __construct(Model $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $import = $this->import;
        $import->update(['status' => Model::PROCESSING_STATUS]);

        try {
            $this->initValidator();
            $sheet = $this->getSheet($import);
            $this->handleSheet($sheet, $import);
        } catch (\Throwable $exception) {
            $message = $exception->getMessage();
            $this->handleError($import, $message);
            return;
        }

        $this->removeFile($import);
        $import->update(['status' => Model::COMPLETED_STATUS]);
    }

    /**
     * The job failed to process.
     *
     * @param \Throwable $exception
     */
    public function failed(\Throwable $exception)
    {
        $import = $this->import;
        $message = $exception->getMessage();

        $this->handleError($import, $message);
    }

    /**
     * @throws \Throwable
     */
    private function initValidator() : void
    {
        $this->validator = Validator::make(
            [],
            [
                'sender_phone' => [
                    'required',
                    'string',
                    function ($attribute, $value, $fail) {
                        try {
                            Phone::createIdentity($value);
                        } catch (\Throwable $exception) {
                            $length = Phone::IDENTITY_LENGTH;
                            return $fail("Поле Телефон должно содержать не менее {$length} цифр.");
                        }
                        return null;
                    },
                ],
                'recording_url' => [
                    'required',
                    'string',
                    'url',
                ],
                'recording_date' => [
                    'required',
                    'date',
                ],
            ],
            [],
            [
                'form_name'    => 'Название лида',
                'sender_phone' => 'Телефон',
                'recording_url' => 'Запись разговора',
                'recording_date' => 'Дата звонка',
            ]
        );
    }

    /**
     * @param \App\Models\EveCalls\Import $import
     * @return \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
     * @throws \Throwable
     */
    private function getSheet(Model $import)
    {
        $file = \Storage::disk(Model::DISK)->path($import->file_name);
        $isFile = \File::isFile($file);
        if (! $isFile) {
            throw new \RuntimeException('Не найден файл для обработки');
        }

        try {
            $reader = IOFactory::createReaderForFile($file);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if (! $reader instanceof Xlsx && ! $reader instanceof Xls) {
            throw new \RuntimeException('Неизвестный тип Reader`а');
        }
        $reader->setReadDataOnly(true);

        try {
            $spreadsheet = $reader->load($file);
            $sheet = $spreadsheet->getActiveSheet();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return $sheet;
    }

    /**
     * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet
     * @param \App\Models\EveCalls\Import $import
     * @throws \Throwable
     */
    private function handleSheet(Worksheet $worksheet, Model $import)
    {
        $rowIterator = $worksheet->getRowIterator();
        try {
            $headers = $this->parseHeaders($rowIterator);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        foreach ($rowIterator as $row) {
            if ($row->getRowIndex() === 1) {
                continue;
            }

            $data = $this->parseRow($row, $headers);
            $this->handleData($data, $import);
        }
    }

    /**
     * @param \PhpOffice\PhpSpreadsheet\Worksheet\RowIterator $rowIterator
     * @return array
     */
    private function parseHeaders(RowIterator $rowIterator)
    {
        $rowIterator->rewind();
        if (! $rowIterator->valid()) {
            throw new \RuntimeException('Не найден заголовок таблицы');
        }
        $row = $rowIterator->current();

        $headers = [];
        foreach ($row->getCellIterator() as $cell) {
            $value = $cell->getFormattedValue();
            $value = trim($value);
            $value = ltrim($value, '*');
            $value = Str::upper($value);

            $column = $cell->getColumn();
            switch($value) {
                case 'НОМЕР Б' :
                    $headers[$column] = 'sender_phone';
                    break;
                case 'ДАТА' :
                    $headers[$column] = 'recording_date';
                    break;
                case 'ЗАПИСЬ РАЗГОВОРА' :
                    $headers[$column] = 'recording_url';
                    break;
                default :
                    break;
            }
        }

        if (! in_array('sender_phone', $headers)) {
            throw new \RuntimeException('Столбец "Телефон" не найден в заголовке таблицы');
        }
        if (! in_array('recording_date', $headers)) {
            throw new \RuntimeException('Столбец "Дата" не найден в заголовке таблицы');
        }
        if (! in_array('recording_url', $headers)) {
            throw new \RuntimeException('Столбец "Запись разговора" не найден в заголовке таблицы');
        }

        return $headers;
    }

    /**
     * @param \PhpOffice\PhpSpreadsheet\Worksheet\Row $row
     * @param array $headers
     * @return array
     */
    private function parseRow(Row $row, array $headers)
    {
        $data = [];
        $cellIterator = $row->getCellIterator();
        foreach ($cellIterator as $cell) {
            $column = $cell->getColumn();
            if (! array_key_exists($column, $headers)) {
                continue;
            }

            $key = $headers[$column];
            $value = $cell->getFormattedValue();

            $data[$key] = $value;
        }

        return $data;
    }

    /**
     * @param array $data
     * @param \PhpOffice\PhpSpreadsheet\Worksheet\Row $row
     * @param \App\Models\EveCalls\Import $import
     */
    private function handleData(array $data, Model $import)
    {
        foreach ($data as $key => $value) {
            $value = str_replace(chr(194) . chr(160), ' ', $value); // NO-BREAK SPACE
            $value = trim($value);
            if ($value === '') {
                unset($data[$key]);
            } else {
                $data[$key] = $value;
            }
        }
        if (count($data) === 0) {
            return;
        }

        $validator = $this->validator;
        $validator->setData($data);

        $errors = [];
        if ($validator->passes()) {
            $application = new Application();
            $application->site_id = 3; // sniperfx.ru
            $application->status = Application::CONFIRMED_STATUS;
            $application->form_name = $import->lead_name;
            $application->sender_phone = Phone::addSign($data['sender_phone']);
            $application->submitted_at = Carbon::now();

            $crmRecordingDateField = Arr::get($import->project->evecalls, 'date_field');
            $crmRecordingUrlField = Arr::get($import->project->evecalls, 'recording_field');
            if ($crmRecordingDateField !== null && $crmRecordingUrlField !== null) {
                $application->lead_extra_fields = [
                    $crmRecordingDateField => $data['recording_date'],
                    $crmRecordingUrlField => $data['recording_url'],
                ];
            }

            try {
                $application->saveOrFail();
                event(new ApplicationStored($application));
            } catch (\Throwable $exception) {
                $errors[] = $exception->getMessage();
            }

            if (count($errors) === 0) {
                $import->applications()->attach($application->id);
                return;
            }
        } else {
            $errors = array_merge($errors, $validator->errors()->all());
        }

        $importError = new ImportError();
        $importError->evecallsImport_id = $import->id;
        $importError->data      = $data;
        $importError->errors    = $errors;
        try {
            $importError->saveOrFail();
            $import->errors()->save($importError);
        } catch (\Throwable $exception) {
            report($exception);
        }
    }

    /**
     * @param Model $import
     * @param string $message
     */
    private function handleError(Model $import, string $message)
    {
        $this->removeFile($import);
        $import->update([
            'status' => Model::ERROR_STATUS,
            'error_text' => $message,
        ]);
    }

    /**
     * @param Model $import
     */
    private function removeFile(Model $import)
    {
        \Storage::disk(Model::DISK)->delete($import->file_name);
    }
}
