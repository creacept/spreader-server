<?php

namespace App\Jobs;

use App\Events\ApplicationStored;
use App\Helpers\AddLead\Field\Email;
use App\Helpers\AddLead\Field\Phone;
use App\Helpers\AddLead\Field\VK;
use App\Helpers\Service\MoodleService;
use App\Models\Application;
use App\Models\Import as Model;
use App\Models\ImportError;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet;

/**
 * Class Import
 * @package App\Jobs
 */
class Import implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Model
     */
    private $import;

    /**
     * @var \Illuminate\Validation\Validator
     */
    private $validator;

    /**
     * @var string|null
     */
    private $moodleBaseUrl;

    /**
     * @var string|null
     */
    private $moodleAccessToken;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    /**
     * Create a new job instance.
     *
     * @param Model $import
     */
    public function __construct(Model $import)
    {
        $this->import = $import;

        $moodleCourseId = Arr::get($import->moodle_data, 'course_id');
        if ($moodleCourseId === null) {
            return;
        }

        $site = $import->site()->first();
        $project = $site->load('project')->project;

        $this->moodleBaseUrl = Arr::get($project->moodle, 'base_url');
        $this->moodleAccessToken = Arr::get($project->moodle, 'access_token');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $import = $this->import;
        $import->update(['status' => Model::PROCESSING_STATUS]);

        try {
            $this->initValidator();
            $sheet = $this->getSheet($import);
            $this->handleSheet($sheet, $import);
        } catch (\Throwable $exception) {
            $message = $exception->getMessage();
            $this->handleError($import, $message);
            return;
        }

        $this->removeFile($import);
        $import->update(['status' => Model::COMPLETED_STATUS]);
    }

    /**
     * @throws \Throwable
     */
    private function initValidator() : void
    {
        try {
            $validator = resolve('validator.application');
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $rules = $validator->getRules();
        unset($rules['id']);
        unset($rules['page_url']);
        unset($rules['page_title']);

        $rules['page_url'] = [
            'nullable',
            'url',
        ];

        $rules['sender_vk'][] = function ($attribute, $value, $fail) {
            if (filter_var($value, FILTER_VALIDATE_URL) === false) {
                return null;
            }
            try {
                VK::getIdentityFromUrl($value);
            } catch (\Throwable $exception) {
                return $fail('Поле ВКонтакте должно быть ссылкой или ID.');
            }
            return null;
        };

        $validator->setRules($rules);
        $this->validator = $validator;
    }

    /**
     * The job failed to process.
     *
     * @param \Throwable $exception
     */
    public function failed(\Throwable $exception)
    {
        $import = $this->import;
        $message = $exception->getMessage();

        $this->handleError($import, $message);
    }

    /**
     * @param Model $import
     * @return PhpSpreadsheet\Worksheet\Worksheet
     * @throws \Throwable
     */
    private function getSheet(Model $import)
    {
        $file = \Storage::disk(Model::DISK)->path($import->file_name);
        $isFile = \File::isFile($file);
        if (! $isFile) {
            throw new \RuntimeException('Не найден файл для обработки');
        }

        try {
            $reader = PhpSpreadsheet\IOFactory::createReaderForFile($file);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if (! $reader instanceof PhpSpreadsheet\Reader\Xlsx && ! $reader instanceof PhpSpreadsheet\Reader\Xls) {
            throw new \RuntimeException('Неизвестный тип Reader`а');
        }
        $reader->setReadDataOnly(true);

        try {
            $spreadsheet = $reader->load($file);
            $sheet = $spreadsheet->getActiveSheet();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return $sheet;
    }

    /**
     * @param PhpSpreadsheet\Worksheet\Worksheet $worksheet
     * @param Model $import
     * @throws \Throwable
     */
    private function handleSheet(PhpSpreadsheet\Worksheet\Worksheet $worksheet, Model $import)
    {
        $rowIterator = $worksheet->getRowIterator();
        try {
            $headers = $this->parseHeaders($rowIterator);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        foreach ($rowIterator as $row) {
            if ($row->getRowIndex() === 1) {
                continue;
            }

            $data = $this->parseRow($row, $headers);
            $this->handleData($data, $row, $import);

            if (
                $this->moodleBaseUrl !== null &&
                $this->moodleAccessToken !== null
            ) {
                /** @var \App\Helpers\Service\MoodleService moodleService */
                $moodleService = App::makeWith(MoodleService::class, [
                    'baseUrl' => $this->moodleBaseUrl,
                    'accessToken' => $this->moodleAccessToken,
                ]);
                $this->assignToMoodle($data, $import, $moodleService);
            }
        }
    }

    /**
     * @param PhpSpreadsheet\Worksheet\RowIterator $rowIterator
     * @return array
     * @throws \Throwable
     */
    private function parseHeaders(PhpSpreadsheet\Worksheet\RowIterator $rowIterator)
    {
        $rowIterator->rewind();
        if (! $rowIterator->valid()) {
            throw new \RuntimeException('Не найден заголовок таблицы');
        }
        $row = $rowIterator->current();

        $headers = [];
        foreach ($row->getCellIterator() as $cell) {
            $value = $cell->getFormattedValue();
            $value = trim($value);
            $value = ltrim($value, '*');
            $value = Str::upper($value);

            $column = $cell->getColumn();
            switch($value) {
                case 'НАЗВАНИЕ ЛИДА' :
                    $headers[$column] = 'form_name';
                    break;
                case 'ТЕЛЕФОН' :
                    $headers[$column] = 'sender_phone';
                    break;
                case 'ФИО' :
                    $headers[$column] = 'sender_name';
                    break;
                case 'E-MAIL' :
                    $headers[$column] = 'sender_email';
                    break;
                case 'ВКОНТАКТЕ' :
                    $headers[$column] = 'sender_vk';
                    break;
                case 'UTM_SOURCE' :
                    $headers[$column] = 'utm_source';
                    break;
                case 'UTM_CAMPAIGN' :
                    $headers[$column] = 'utm_campaign';
                    break;
                case 'UTM_CONTENT' :
                    $headers[$column] = 'utm_content';
                    break;
                case 'UTM_MEDIUM' :
                    $headers[$column] = 'utm_medium';
                    break;
                case 'SUB_ID' :
                    $headers[$column] = 'sub_id';
                    break;
                case 'КОММЕНТАРИЙ' :
                    $headers[$column] = 'comment';
                    break;
                case 'URL' :
                    $headers[$column] = 'page_url';
                    break;
                default :
                    break;
            }
        }

        if (! in_array('form_name', $headers)) {
            throw new \RuntimeException('Столбец "Название лида" не найден в заголовке таблицы');
        }

        if (! in_array('sender_email', $headers)) {
            throw new \RuntimeException('Столбец "Email" не найден в заголовке таблицы');
        }

        return $headers;
    }

    /**
     * @param PhpSpreadsheet\Worksheet\Row $row
     * @param array $headers
     * @return array
     */
    private function parseRow(PhpSpreadsheet\Worksheet\Row $row, array $headers)
    {
        $data = [];
        $cellIterator = $row->getCellIterator();
        foreach ($cellIterator as $cell) {
            $column = $cell->getColumn();
            if (! array_key_exists($column, $headers)) {
                continue;
            }

            $key = $headers[$column];
            $value = $cell->getFormattedValue();

            $data[$key] = $value;
        }

        return $data;
    }

    /**
     * @param array $data
     * @param PhpSpreadsheet\Worksheet\Row $row
     * @param Model $import
     */
    private function handleData(array $data, PhpSpreadsheet\Worksheet\Row $row, Model $import)
    {
        foreach ($data as $key => $value) {
            $value = str_replace(chr(194) . chr(160), ' ', $value); // NO-BREAK SPACE
            $value = trim($value);
            if ($value === '') {
                unset($data[$key]);
            } else {
                $data[$key] = $value;
            }
        }
        if (count($data) === 0) {
            return;
        }

        $validator = $this->validator;
        $validator->setData($data);

        $errors = [];
        if ($validator->passes()) {
            try {
                Phone::createIdentity(Arr::get($data, 'sender_phone'));
                $data['sender_phone'] = Phone::clear($data['sender_phone']);
            } catch (\Throwable $e) {
                $data['sender_phone'] = null;
            }

            if (
                Arr::has($data, 'sender_email') &&
                is_string(Arr::get($data, 'sender_email'))
            ) {
                $data['sender_email'] = Email::createIdentity($data['sender_email']);
            }

            if (isset($data['sender_vk']) && filter_var($data['sender_vk'], FILTER_VALIDATE_URL) !== false) {
                try {
                    $data['sender_vk'] = VK::getIdentityFromUrl($data['sender_vk']);
                } catch (\Throwable $exception) {
                    report($exception);
                    unset($data['sender_vk']);
                }
            }

            $application = new Application();
            $application->site_id = $import->site_id;
            $application->priority = $import->priority;
            $application->status = Application::CONFIRMED_STATUS;
            $application->fill($data);
            try {
                $application->saveOrFail();
                event(new ApplicationStored($application));
            } catch (\Throwable $exception) {
                $errors[] = $exception->getMessage();
            }

            if (count($errors) === 0) {
                $import->applications()->attach($application->id, ['index' => $row->getRowIndex()]);
                return;
            }
        } else {
            $errors = array_merge($errors, $validator->errors()->all());
        }

        $importError = new ImportError();
        $importError->import_id = $import->id;
        $importError->index     = $row->getRowIndex();
        $importError->data      = $data;
        $importError->errors    = $errors;
        try {
            $importError->saveOrFail();
            $import->errors()->save($importError);
        } catch (\Throwable $exception) {
            report($exception);
        }
    }

    /**
     * @param array $data
     * @param \App\Models\Import $import
     * @param \App\Helpers\Service\MoodleService $moodleService
     * @return void
     */
    private function assignToMoodle(array $data, Model $import, MoodleService $moodleService)
    {
        $courseId = Arr::get($import->moodle_data, 'course_id');
        $groupId = Arr::get($import->moodle_data, 'group_id');

        $email = Arr::get($data, 'sender_email');
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return;
        }

        try {
            $student = $moodleService->getUser($email);
        } catch (GuzzleException $e) {
            Log::error("Cant't find Moodle user with email {$email}: {$e->getMessage()}");
            return;
        }

        if (! is_array($student)) {
            $emailParts = explode('@', $email);
            $login = $emailParts[0];

            $name = Arr::get($data, 'sender_name');
            if (! is_string($name)) {
                $name = $login;
            }

            $userData = [
                'name' => $name,
                'username' => $login,
                'email' => $email,
            ];

            try {
                $moodleService->createUser($userData);
                $student = $moodleService->getUser($email);
            } catch (GuzzleException $e) {
                Log::error("Can't create Moodle user with email {$email}: {$e->getMessage()}");
                return;
            }
        }

        // If user couldn't be created or selected "only registration", break
        if (! is_array($student) || $courseId === MoodleService::ONLY_REGISTRATION_LABEL) {
            return;
        }

        try {
            $moodleService->assignUserToCourse($student['username'], $courseId, $groupId);
        } catch (GuzzleException $e) {
            Log::error("Can't assign user {$student['username']} to course {$courseId} : {$e->getMessage()}");
            return;
        }
    }

    /**
     * @param Model $import
     * @param string $message
     */
    private function handleError(Model $import, string $message)
    {
        $this->removeFile($import);
        $import->update([
            'status' => Model::ERROR_STATUS,
            'error_text' => $message,
        ]);
    }

    /**
     * @param Model $import
     */
    private function removeFile(Model $import)
    {
        \Storage::disk(Model::DISK)->delete($import->file_name);
    }
}
