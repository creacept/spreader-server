<?php

namespace App\Jobs;

use App\Models\Application;
use App\Models\Site;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Carbon;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Class CollectingConfirmHandler
 * @package App\Jobs
 */
class CollectingConfirmHandler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public const QUEUE = 'collecting';
    private const ERROR_DELAY = 600;
    private const WAIT_DELAY = 60;

    /**
     * @var Site
     */
    private $site;
    /**
     * @var int[]
     */
    private $sourceIds;

    /**
     * Create a new job instance.
     *
     * @param Site $site
     * @param array $sourceIds
     */
    public function __construct(Site $site, array $sourceIds)
    {
        $this->site = $site;
        $this->sourceIds = $sourceIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $site = $this->site;
        $client = new Client();

        try {
            $response = $client->post($site->url, [
                RequestOptions::FORM_PARAMS => [
                    'action' => 'confirm',
                    'password' => $site->password,
                    'id' => $this->sourceIds,
                ],
            ]);
        } catch (Throwable $exception) {
            report($exception);
            $response = null;
            $site->handled_at = Carbon::now()->addSeconds(self::ERROR_DELAY);
        }

        if ($response instanceof ResponseInterface) {
            try {
                Application::whereSiteId($site->id)->whereIn('source_id', $this->sourceIds)->update(['status' => Application::CONFIRMED_STATUS]);
                $site->handled_at = Carbon::now()->addSeconds(self::WAIT_DELAY);
            } catch (Throwable $exception) {
                report($exception);
                $site->handled_at = Carbon::now()->addSeconds(self::ERROR_DELAY);
            }
        }

        try {
            $site->saveOrFail();
        } catch (Throwable $exception) {
            report($exception);
        }

        CollectingObserver::dispatch()->onQueue(CollectingObserver::QUEUE);
    }

    /**
     * @return array
     */
    public function tags()
    {
        return [
            'collecting',
            'confirm-handler',
            'site_id: ' . $this->site->id,
            'source_id: ' . implode(', ', $this->sourceIds),
        ];
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     */
    public function failed(Throwable $exception)
    {
        CollectingObserver::dispatch()->onQueue(CollectingObserver::QUEUE);
    }
}
