<?php

namespace App\Events;

use App\Models\Application;
use Illuminate\Queue\SerializesModels;

class ApplicationStored
{
    use SerializesModels;

    public $application;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }
}
