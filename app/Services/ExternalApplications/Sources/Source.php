<?php

namespace App\Services\ExternalApplications\Sources;

use App\Models\Application;

/**
 * Interface Source
 * @package App\Services\ExternalApplications\Sources
 */
interface Source
{
    /**
     * @return array
     */
    public function getApplications(): array;

    /**
     * @param array $rawApplication
     * @return \App\Models\Application
     */
    public function convertApplication(array $rawApplication): Application;

    /**
     * @param array $rawApplication
     * @param array|null $params
     * @return void
     */
    public function afterApplicationStored(array $rawApplication, array $params = null): void;
}

