<?php

namespace App\Services\ExternalApplications\Sources;

use App\Helpers\AddLead\Field\Email;
use App\Helpers\AddLead\Field\Phone;
use App\Helpers\Bitrix\ClientFactory;
use App\Models\Application;
use App\Models\CRM;
use App\Models\Manager;
use App\Models\Site;
use Bitrix24\CRM\Lead;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use InvalidArgumentException;
use RuntimeException;
use Throwable;

/**
 * Class Bitrix24SupportChat
 * @package App\Services\ExternalApplications\Sources
 */
class Bitrix24SupportChat implements Source
{
    /**
     * @var string
     */
    private const BITRIX24_SOURCES_IDS = ['16|OPENLINE', '32|OPENLINE'];

    /**
     * @var string
     */
    private const BITRIX24_STATUS_ID = 'NEW';

    /**
     * @var \Bitrix24\CRM\Lead
     */
    private $leadsApi;

    /**
     * Bitrix24Telephony constructor.
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @throws \RuntimeException
     */
    public function __construct(ClientFactory $clientFactory) {
        try {
            $crm = CRM::findOrFail(1);
            $client = $clientFactory->create($crm);
            $this->leadsApi = new Lead($client);
        } catch (Throwable $e) {
            throw new RuntimeException('Can\'t find default CRM', null, $e);
        }
    }

    /**
     * Get leads created by Bitrix24's IP-telephony module.
     *
     * @return array
     */
    public function getApplications(): array
    {
        $leadsData = $this->leadsApi->getList(
            ['ID' => 'DESC'],
            ['SOURCE_ID' => self::BITRIX24_SOURCES_IDS, 'STATUS_ID' => self::BITRIX24_STATUS_ID, 'HAS_PHONE' => 'Y'],
            ['ID', 'TITLE', 'NAME', 'ASSIGNED_BY_ID', 'DATE_CREATE', 'PHONE', 'EMAIL', 'UTM_CONTENT', 'UTM_SOURCE', 'UTM_CAMPAIGN', 'UTM_MEDIUM']
        );

        $leads = Arr::get($leadsData, 'result', []);
        return $leads;
    }

    /**
     * @param array $rawApplication
     * @return \App\Models\Application
     */
    public function convertApplication(array $rawApplication): Application
    {
        $application = new Application();
        $application->status = Application::CONFIRMED_STATUS;

        $leadName = Arr::get($rawApplication, 'TITLE');
        $application->sender_name = Arr::get($rawApplication, 'NAME') ?? $leadName;
        $application->form_name = $leadName;

        try {
            $managerId = Arr::get($rawApplication, 'ASSIGNED_BY_ID');
            $projectId = Manager::where('bitrix24_user_id', $managerId)->value('project_id');
            $site = Site::where('project_id', $projectId)->firstOrFail();
            $application->site_id = $site->id;
        } catch (Throwable $e) {
            throw new RuntimeException('Can\'t set default site for application', null, $e);
        }

        $leadPhones = Arr::get($rawApplication, 'PHONE');
        if ($leadPhones === null || ! is_array($leadPhones)) {
            throw new InvalidArgumentException('Lead hasn\'t phone number');
        }
        $leadPhone = Arr::get($leadPhones, '0.VALUE');
        $application->sender_phone = Phone::addSign($leadPhone);

        $leadEmails = Arr::get($rawApplication, 'EMAIL');
        if (is_array($leadPhones)) {
            $leadEmail = Arr::get($leadEmails, '0.VALUE');
            $application->sender_email = Email::createIdentity($leadEmail);
        }

        $createdDate = Arr::get($rawApplication, 'DATE_CREATE');
        $application->submitted_at = Carbon::createFromTimeString($createdDate);

        $application->utm_content = Arr::get($rawApplication, 'UTM_CONTENT');
        $application->utm_medium = Arr::get($rawApplication, 'UTM_MEDIUM');
        $application->utm_source = Arr::get($rawApplication, 'UTM_SOURCE');
        $application->utm_campaign = Arr::get($rawApplication, 'UTM_CAMPAIGN');

        return $application;
    }

    /**
     * @param array $rawApplication
     * @param array|null $params
     */
    public function afterApplicationStored(array $rawApplication, array $params = null): void
    {
        $leadId = Arr::get($rawApplication, 'ID');
        if ($leadId !== null) {
            $this->leadsApi->delete($leadId);
        }
    }
}