<?php

namespace App\Services\ExternalApplications\Sources;

use App\Helpers\AddLead\Field\Email;
use App\Helpers\AddLead\Field\Phone;
use App\Helpers\Bitrix\ClientFactory;
use App\Models\Application;
use App\Models\CRM;
use App\Models\Manager;
use App\Models\Project;
use App\Models\Site;
use Bitrix24\CRM\Lead;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use RuntimeException;
use Throwable;

/**
 * Class Bitrix24CrmForm
 * @package App\Services\ExternalApplications\Sources
 */
class Bitrix24CrmForm implements Source
{
    /**
     * @var string
     */
    private const SUBID_FIELD_ID = 'UF_CRM_1532953513';

    /**
     * @var string
     */
    private const PROJECT_FIELD_ID = 'UF_CRM_1537966310';

    /**
     * @var string
     */
    private const UTMSOURCE_FIELD_ID = 'UF_CRM_1612452790';

    /**
     * @var string
     */
    private const UTMCAMPAIGN_FIELD_ID = 'UF_CRM_1612453963';

    /**
     * @var string
     */
    private const UTMMEDIUM_FIELD_ID = 'UF_CRM_1612452922';

    /**
     * @var string
     */
    private const UTMCONTENT_FIELD_ID = 'UF_CRM_1612452846';

    /**
     * @var string
     */
    private const BITRIX24_SOURCE_ID = 'WEBFORM';

    /**
     * @var string
     */
    private const BITRIX24_STATUS_ID = 'NEW';

    /**
     * @var \Bitrix24\CRM\Lead
     */
    private $leadsApi;

    /**
     * Bitrix24Vk constructor.
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @throws \RuntimeException
     */
    public function __construct(ClientFactory $clientFactory) {
        try {
            $crm = CRM::findOrFail(1);
            $client = $clientFactory->create($crm);
            $this->leadsApi = new Lead($client);
        } catch (Throwable $e) {
            throw new RuntimeException('Can\'t find default CRM', null, $e);
        }
    }

    /**
     * Get leads created by Bitrix24's IP-telephony module.
     *
     * @return array
     */
    public function getApplications(): array
    {
        $leadsData = $this->leadsApi->getList(
            ['ID' => 'DESC'],
            ['SOURCE_ID' => self::BITRIX24_SOURCE_ID, 'STATUS_ID' => self::BITRIX24_STATUS_ID],
            [
                'ID', 'TITLE', 'ASSIGNED_BY_ID', 'DATE_CREATE', 'NAME', 'PHONE', 'EMAIL', self::PROJECT_FIELD_ID,
                self::UTMCONTENT_FIELD_ID, self::UTMSOURCE_FIELD_ID, self::UTMMEDIUM_FIELD_ID, self::UTMCAMPAIGN_FIELD_ID,
                'UTM_SOURCE', 'UTM_CONTENT', 'UTM_MEDIUM', 'UTM_CAMPAIGN', self::SUBID_FIELD_ID,
            ]
        );

        $leads = Arr::get($leadsData, 'result', []);
        return $leads;
    }

    /**
     * @param array $rawApplication
     * @return \App\Models\Application
     */
    public function convertApplication(array $rawApplication): Application
    {
        $application = new Application();
        $application->status = Application::CONFIRMED_STATUS;

        $leadName = Arr::get($rawApplication, 'TITLE');
        $contactName = Arr::get($rawApplication, 'NAME') ?? $leadName;
        $application->form_name = $leadName;
        $application->sender_name = $contactName;

        $leadProject = Arr::get($rawApplication, self::PROJECT_FIELD_ID);
        $projectId = Project::where('lead_identity', $leadProject)->value('id');
        if ($projectId == null) {
            $managerId = Arr::get($rawApplication, 'ASSIGNED_BY_ID');
            $projectId = Manager::where('bitrix24_user_id', $managerId)->value('project_id');
        }
        try {
            $site = Site::where('project_id', $projectId)->firstOrFail();
            $application->site_id = $site->id;
        } catch (Throwable $e) {
            report($e);
            throw new RuntimeException('Can\'t set default site for application', null, $e);
        }

        $leadPhone = null;
        $leadPhones = Arr::get($rawApplication, 'PHONE');
        if ($leadPhones !== null && is_array($leadPhones)) {
            $leadPhone = Arr::get($leadPhones, '0.VALUE');
            $leadPhone = Phone::clear($leadPhone);
        }
        $application->sender_phone = $leadPhone;

        $leadEmail = null;
        $leadEmails = Arr::get($rawApplication, 'EMAIL');
        if ($leadEmails !== null && is_array($leadEmails)) {
            $leadEmail = Arr::get($leadEmails, '0.VALUE');
            $leadEmail = Email::createIdentity($leadEmail);
        }
        $application->sender_email = $leadEmail;

        $createdDate = Arr::get($rawApplication, 'DATE_CREATE');
        $application->submitted_at = Carbon::createFromTimeString($createdDate);

        $utmContent = Arr::get($rawApplication, self::UTMCONTENT_FIELD_ID) ?? Arr::get($rawApplication, 'UTM_CONTENT');
        $application->utm_content = $utmContent;

        $utmSource = Arr::get($rawApplication, self::UTMSOURCE_FIELD_ID) ?? Arr::get($rawApplication, 'UTM_SOURCE');
        $application->utm_source = $utmSource;

        $utmMedium = Arr::get($rawApplication, self::UTMMEDIUM_FIELD_ID) ?? Arr::get($rawApplication, 'UTM_MEDIUM');
        $application->utm_medium = $utmMedium;

        $utmCampaign = Arr::get($rawApplication, self::UTMCAMPAIGN_FIELD_ID) ?? Arr::get($rawApplication, 'UTM_CAMPAIGN');
        $application->utm_campaign = $utmCampaign;

        $subId = Arr::get($rawApplication, self::SUBID_FIELD_ID);
        $application->sub_id = $subId;

        return $application;
    }

    /**
     * @param array $rawApplication
     * @param array|null $params
     */
    public function afterApplicationStored(array $rawApplication, array $params = null): void
    {
        $leadId = Arr::get($rawApplication, 'ID');
        if ($leadId !== null) {
            $this->leadsApi->delete($leadId);
        }
    }
}