<?php

namespace App\Services\ExternalApplications;

use App\Services\ExternalApplications\Sources\Source;
use Throwable;

/**
 * Class ExternalApplicationsCollector
 * @package App\Services\ExternalApplications
 */
class ExternalApplicationsCollector
{
    /**
     * @var \App\Services\ExternalApplications\Sources\Source
     */
    private $source;

    /**
     * @param \App\Services\ExternalApplications\Sources\Source $source
     */
    public function setSource(Source $source)
    {
        $this->source = $source;
    }

    /**
     * Get, convert and store applications from selected source.
     */
    public function process()
    {
        try {
            $rawApplications = $this->source->getApplications();
        } catch (Throwable $e) {
            report($e);
            return;
        }

        foreach ($rawApplications as $rawApplication) {
            try {
                $application = $this->source->convertApplication($rawApplication);
            } catch (Throwable $e) {
                continue;
            }
            $application->save();
            $this->source->afterApplicationStored($rawApplication);
        }
    }
}