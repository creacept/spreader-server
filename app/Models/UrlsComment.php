<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UrlsComment extends Model
{
    protected $table = 'urlsComments';

    protected $fillable = ['urls', 'comment'];

    protected $casts = [
        'urls' => 'array',
    ];
}
