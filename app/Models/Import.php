<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Import
 * @package App\Models
 */
class Import extends Model
{
    /**
     * @var string
     */
    public const DISK = 'import';

    /**
     * @var int
     */
    public const ERROR_STATUS = -1;

    /**
     * @var int
     */
    public const PENDING_STATUS = 0;

    /**
     * @var int
     */
    public const PROCESSING_STATUS = 1;

    /**
     * @var int
     */
    public const COMPLETED_STATUS = 2;

    /**
     * @var int Priority for imported applications.
     */
    public const PRIORITY_NORMAL = 0;

    /**
     * @var int Default priority for applications.
     */
    public const PRIORITY_HIGH = 1;

    /**
     * @var array
     */
    protected $fillable = [
        'status',
        'error_text',
        'moodle_data',
        'priority',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'file_name' => 'string',
        'site_id' => 'integer',
        'user_id' => 'integer',
        'status' => 'integer',
        'error_text' => 'string',
        'moodle_data' => 'array',
        'priority' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $this->belongsTo(Site::class)->withTrashed();
    }

    /**
     * @return mixed
     */
    public function user()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applications()
    {
        return $this->belongsToMany(Application::class, 'import_application')->withPivot(['index']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function errors()
    {
        return $this->hasMany(ImportError::class);
    }
}
