<?php

namespace App\Models\EveCalls;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ImportError
 * @package App\Models\EveCalls
 */
class ImportError extends Model
{
    /**
     * @var string
     */
    protected $table = 'evecallsImport_error';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $casts = [
        'evecallsImport_id' => 'integer',
        'data'      => 'array',
        'errors'    => 'array',
    ];
}
