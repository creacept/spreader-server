<?php

namespace App\Models\EveCalls;

use App\Models\Application;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Import
 * @package App\Models\EveCalls
 */
class Import extends Model
{
    /**
     * @var string
     */
    public const DISK = 'import';

    /**
     * @var int
     */
    public const ERROR_STATUS = -1;

    /**
     * @var int
     */
    public const PENDING_STATUS = 0;

    /**
     * @var int
     */
    public const PROCESSING_STATUS = 1;

    /**
     * @var int
     */
    public const COMPLETED_STATUS = 2;

    /**
     * @var string
     */
    public const DEFAULT_LEAD_NAME = 'Автодозвон';

    /**
     * @var string
     */
    protected $table = 'evecallsImports';

    /**
     * @var array
     */
    protected $fillable = [
        'status',
        'lead_name',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'lead_name' => self::DEFAULT_LEAD_NAME,
    ];

    /**
     * @var array
     */
    protected $casts = [
        'status' => 'integer',
        'user_id' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applications(): BelongsToMany
    {
        return $this->belongsToMany(Application::class, 'evecallsImport_application', 'evecallsImport_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function errors()
    {
        return $this->hasMany(ImportError::class, 'evecallsImport_id');
    }
}
