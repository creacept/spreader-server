<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Application
 * @package App\Models
 */
class Application extends Model
{
    /**
     * @var int
     */
    public const UPDATED_AT = null;

    /**
     * @var int
     */
    public const RAW_STATUS = 0;

    /**
     * @var int
     */
    public const CONFIRMED_STATUS = 1;

    /**
     * @var int
     */
    public const SUCCESS_STATUS = 2;

    /**
     * Special status for applications with denied phone numbers/emails.
     *
     * @var int
     */
    public const REJECTED_STATUS = 3;

    /**
     * @var array
     */
    protected $fillable = [
        'submitted_at',
        'form_name',
        'sender_name',
        'sender_phone',
        'sender_email',
        'sender_vk',
        'page_url',
        'page_title',
        'utm_source',
        'utm_content',
        'utm_campaign',
        'utm_medium',
        'sub_id',
        'comment',
        'extra_fields',
        'lead_extra_fields',
        'sender_extra_fields',
        'priority',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'site_id'      => 'integer',
        'source_id'    => 'integer',
        'status'       => 'integer',
        'handled_at'   => 'datetime',
        'submitted_at' => 'datetime',
        'form_name'    => 'string',
        'sender_name'  => 'string',
        'sender_phone' => 'string',
        'sender_email' => 'string',
        'sender_vk'    => 'string',
        'page_url'     => 'string',
        'page_title'   => 'string',
        'utm_source'   => 'string',
        'utm_content'  => 'string',
        'utm_campaign' => 'string',
        'utm_medium'   => 'string',
        'sub_id'       => 'string',
        'comment'      => 'string',
        'extra_fields' => 'array',
        'lead_extra_fields' => 'array',
        'sender_extra_fields' => 'array',
        'priority' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $this->belongsTo(Site::class)->withTrashed();
    }
}
