<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CRM
 * @package App\Models
 */
class CRM extends Model
{
    public const AUTH_OAUTH2 = 'OAuth2';
    public const AUTH_WEBHOOK = 'Webhook';

    protected $table = 'crm';
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'config' => 'array',
        'settings' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany(Project::class, 'crm_id');
    }
}
