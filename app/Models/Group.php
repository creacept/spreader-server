<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Group
 * @package App\Models
 */
class Group extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'publish',
        'ratio',
     ];

    /**
     * @var array
     */
    protected $casts = [
        'application_number' => 'integer',
        'name' => 'string',
        'project_id' => 'integer',
        'publish' => 'boolean',
        'ratio' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function managers()
    {
        $grammar = \DB::getQueryGrammar();

        return $this->belongsToMany(Manager::class, 'manager_group')
            ->where('publish', true)
            ->orderByRaw(sprintf(
                '%s / %s',
                $grammar->wrap('application_number'),
                $grammar->wrap('ratio')
            ));
    }
}
