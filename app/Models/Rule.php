<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rule
 * @package App\Models
 */
class Rule extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'form_name',
        'group_id',
        'page_title',
        'page_url_domain',
        'page_url_path',
        'publish',
        'sub_id',
        'utm_campaign',
        'utm_content',
        'utm_medium',
        'utm_source',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'form_name' => 'string',
        'group_id' => 'integer',
        'page_title' => 'string',
        'page_url_domain' => 'string',
        'page_url_path' => 'string',
        'priority' => 'integer',
        'publish' => 'boolean',
        'site_id' => 'integer',
        'sub_id' => 'string',
        'utm_campaign' => 'string',
        'utm_content' => 'string',
        'utm_source' => 'string',
        'utm_medium' => 'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
