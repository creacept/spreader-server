<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Site
 * @package App\Models
 */
class Site extends Model
{
    use SoftDeletes;

    public const SITE_TYPE = 0;
    public const IMPORT_TYPE = 1;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'password',
        'publish',
        'url',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'deleted_at' => 'datetime',
        'handled_at' => 'datetime',
        'project_id' => 'integer',
        'publish'    => 'boolean',
        'type'       => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_site')
            ->orderBy('name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rules()
    {
        return $this->hasMany(Rule::class)
            ->with('group')
            ->where('publish', true)
            ->whereNotNull('group_id')
            ->whereHas('group', function (Builder $query) {
                $query->where('publish', true);
            })
            ->orderBy('priority');
    }
}
