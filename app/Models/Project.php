<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Project
 * @package App\Models
 */
class Project extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'contact_identity',
        'crm_id',
        'lead_identity',
        'name',
        'publish',
        'moodle',
        'evecalls',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'contact_identity' => 'integer',
        'lead_identity'    => 'integer',
        'crm_id'           => 'integer',
        'name'             => 'string',
        'publish'          => 'boolean',
        'moodle'           => 'array',
        'evecalls'         => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function crm()
    {
        return $this->belongsTo(CRM::class);
    }
}
