<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ImportError
 * @package App\Models
 */
class ImportError extends Model
{
    /**
     * @var string
     */
    protected $table = 'import_error';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $casts = [
        'import_id' => 'integer',
        'index'     => 'integer',
        'data'      => 'array',
        'errors'    => 'array',
    ];
}
