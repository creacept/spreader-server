<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Manager
 * @package App\Models
 */
class Manager extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'publish',
        'ratio',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'application_number' => 'integer',
        'bitrix24_user_id' => 'integer',
        'project_id' => 'integer',
        'publish' => 'boolean',
        'ratio' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'manager_group')->orderBy('name');
    }
}
