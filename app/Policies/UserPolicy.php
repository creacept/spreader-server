<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class UserPolicy
 * @package App\Policies
 */
class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the models.
     *
     * @param  \App\Models\User $user
     * @return bool
     */
    public function list(User $user)
    {
        return in_array('user.list', $user->permissions);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\User $model
     * @return void
     */
    public function view(User $user, User $model)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User $user
     * @return bool
     */
    public function create(User $user)
    {
        return in_array('user.create', $user->permissions);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\User $model
     * @return bool
     */
    public function update(User $user, User $model)
    {
        return in_array('user.update', $user->permissions);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return bool
     */
    public function delete(User $user, User $model)
    {
        return in_array('user.delete', $user->permissions);
    }
}
