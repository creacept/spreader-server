<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use App\Models\Manager;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ManagerPolicy
 * @package App\Policies
 */
class ManagerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the managers.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function list(User $user, Project $project)
    {
        return in_array("project_{$project->id}.manager.list", $user->permissions);
    }

    /**
     * Determine whether the user can view the manager.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Manager $manager
     * @return void
     */
    public function view(User $user, Project $project, Manager $manager)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can create managers.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function create(User $user, Project $project)
    {
        return in_array("project_{$project->id}.manager.create", $user->permissions);
    }

    /**
     * Determine whether the user can update the manager.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Manager $manager
     * @return bool
     */
    public function update(User $user, Project $project, Manager $manager)
    {
        return in_array("project_{$project->id}.manager.update", $user->permissions);
    }

    /**
     * Determine whether the user can delete the manager.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Manager $manager
     * @return bool
     */
    public function delete(User $user, Project $project, Manager $manager)
    {
        return in_array("project_{$project->id}.manager.delete", $user->permissions);
    }
}
