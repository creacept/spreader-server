<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CRM;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class CRMPolicy
 * @package App\Policies
 */
class CRMPolicy
{
    use HandlesAuthorization;

    /**
     * @param \App\Models\User $user
     * @return bool
     */
    public function list(User $user)
    {
        return in_array('crm.list', $user->permissions);
    }

    /**
     * Determine whether the user can view the CRM.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CRM  $crm
     * @return mixed
     */
    public function view(User $user, CRM $crm)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can create CRM.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array('crm.create', $user->permissions);
    }

    /**
     * Determine whether the user can update the CRM.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CRM  $crm
     * @return mixed
     */
    public function update(User $user, CRM $crm)
    {
        return in_array('crm.update', $user->permissions);
    }

    /**
     * Determine whether the user can delete the CRM.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CRM  $crm
     * @return mixed
     */
    public function delete(User $user, CRM $crm)
    {
        return in_array('crm.delete', $user->permissions);
    }

    /**
     * @param \App\Models\User $user
     * @param \App\Models\CRM $crm
     * @return bool
     */
    public function settings(User $user, CRM $crm)
    {
        return in_array('crm.settings', $user->permissions);
    }
}
