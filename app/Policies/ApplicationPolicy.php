<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use App\Models\Application;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ApplicationPolicy
 * @package App\Policies
 */
class ApplicationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the groups.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function list(User $user, Project $project)
    {
        return in_array("project_{$project->id}.application.list", $user->permissions);
    }

    /**
     * Determine whether the user can view the application.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Application $application
     * @return bool
     */
    public function view(User $user, Project $project, Application $application)
    {
        return in_array("project_{$project->id}.application.view", $user->permissions);
    }

    /**
     * Determine whether the user can create applications.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @return void
     */
    public function create(User $user, Project $project)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can update the applications.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Application $application
     * @return void
     */
    public function update(User $user, Project $project, Application $application)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can delete the application.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Application $application
     * @return void
     */
    public function delete(User $user, Project $project, Application $application)
    {
        throw new \BadMethodCallException('Method not implemented');
    }
}
