<?php

namespace App\Policies;

use App\Models\Site;
use App\Models\User;
use App\Models\Rule;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class RulePolicy
 * @package App\Policies
 */
class RulePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the rules.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Site $site
     * @return bool
     */
    public function list(User $user, Site $site)
    {
        return in_array("project_{$site->project_id}.rule.list", $user->permissions);
    }

    /**
     * Determine whether the user can view the rule.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Site $site
     * @param  \App\Models\Rule $rule
     * @return void
     */
    public function view(User $user, Site $site, Rule $rule)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can create rules.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Site $site
     * @return bool
     */
    public function create(User $user, Site $site)
    {
        return in_array("project_{$site->project_id}.rule.create", $user->permissions);
    }

    /**
     * Determine whether the user can update the rule.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Site $site
     * @param  \App\Models\Rule $rule
     * @return bool
     */
    public function update(User $user, Site $site, Rule $rule)
    {
        return in_array("project_{$site->project_id}.rule.update", $user->permissions);
    }

    /**
     * Determine whether the user can delete the rule.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Site $site
     * @param  \App\Models\Rule $rule
     * @return bool
     */
    public function delete(User $user, Site $site, Rule $rule)
    {
        return in_array("project_{$site->project_id}.rule.delete", $user->permissions);
    }
}
