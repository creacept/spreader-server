<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use App\Models\Import;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ImportPolicy
 * @package App\Policies
 */
class ImportPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the imports.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function list(User $user, Project $project)
    {
        return in_array("project_{$project->id}.import.list", $user->permissions);
    }

    /**
     * Determine whether the user can view the import.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Import  $import
     * @return bool
     */
    public function view(User $user, Project $project, Import $import)
    {
        return (
            in_array("project_{$project->id}.import.view", $user->permissions) ||
            $import->user_id === $user->id
        );
    }

    /**
     * Determine whether the user can create imports.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function create(User $user, Project $project)
    {
        return in_array("project_{$project->id}.import.create", $user->permissions);
    }

    /**
     * Determine whether the user can update the import.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Import $import
     * @return void
     */
    public function update(User $user, Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can delete the import.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Import $import
     * @return void
     */
    public function delete(User $user, Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }
}
