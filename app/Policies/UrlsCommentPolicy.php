<?php
namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class UrlsCommentPolicy
 * @package App\Policies
 */
class UrlsCommentPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function list(User $user)
    {
        return in_array('urlscomment.list', $user->permissions);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return in_array('urlscomment.create', $user->permissions);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return in_array('urlscomment.update', $user->permissions);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function delete(User $user)
    {
        return in_array('urlscomment.delete', $user->permissions);
    }
}
