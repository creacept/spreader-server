<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use App\Models\Site;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class SitePolicy
 * @package App\Policies
 */
class SitePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the sites.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function list(User $user, Project $project)
    {
        return in_array("project_{$project->id}.site.list", $user->permissions);
    }

    /**
     * Determine whether the user can view the site.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Site $site
     * @return void
     */
    public function view(User $user, Project $project, Site $site)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can create sites.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function create(User $user, Project $project)
    {
        return in_array("project_{$project->id}.site.create", $user->permissions);
    }

    /**
     * Determine whether the user can update the site.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Site $site
     * @return bool
     */
    public function update(User $user, Project $project, Site $site)
    {
        return in_array("project_{$project->id}.site.update", $user->permissions);
    }

    /**
     * Determine whether the user can delete the site.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Site $site
     * @return bool
     */
    public function delete(User $user, Project $project, Site $site)
    {
        return in_array("project_{$project->id}.site.delete", $user->permissions);
    }
}
