<?php

namespace App\Policies\EveCalls;

use App\Models\EveCalls\Import;
use App\Models\Project;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ImportPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the imports.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function list(User $user, Project $project)
    {
        return in_array("project_{$project->id}.evecalls-import.list", $user->permissions);
    }

    /**
     * Determine whether the user can view the import.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Project $project
     * @param \App\Models\EveCalls\Import $import
     * @return bool
     */
    public function view(User $user, Project $project, Import $import)
    {
        return (
            in_array("project_{$project->id}.evecalls-import.view", $user->permissions) ||
            $import->user_id === $user->id
        );
    }

    /**
     * Determine whether the user can create imports.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function create(User $user, Project $project)
    {
        return in_array("project_{$project->id}.evecalls-import.create", $user->permissions);
    }

    /**
     * Determine whether the user can update the import.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Project $project
     * @param \App\Models\EveCalls\Import $import
     * @return void
     */
    public function update(User $user, Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can delete the import.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Project $project
     * @param \App\Models\EveCalls\Import $import
     * @return void
     */
    public function delete(User $user, Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }
}
