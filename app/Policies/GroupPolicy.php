<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use App\Models\Group;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class GroupPolicy
 * @package App\Policies
 */
class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the groups.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function list(User $user, Project $project)
    {
        return in_array("project_{$project->id}.group.list", $user->permissions);
    }

    /**
     * Determine whether the user can view the group.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Group $group
     * @return void
     */
    public function view(User $user, Project $project, Group $group)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Determine whether the user can create groups.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @return bool
     */
    public function create(User $user, Project $project)
    {
        return in_array("project_{$project->id}.group.create", $user->permissions);
    }

    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Group $group
     * @return bool
     */
    public function update(User $user, Project $project, Group $group)
    {
        return in_array("project_{$project->id}.group.update", $user->permissions);
    }

    /**
     * Determine whether the user can delete the group.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Project $project
     * @param  \App\Models\Group $group
     * @return bool
     */
    public function delete(User $user, Project $project, Group $group)
    {
        return in_array("project_{$project->id}.group.delete", $user->permissions);
    }
}
