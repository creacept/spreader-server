<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 09.08.2018
 * Time: 17:55
 */

namespace App\Helpers\AddLead\Field;

use Illuminate\Support\Str;

/**
 * Class Email
 * @package App\Helpers\AddLead\Field
 */
class Email
{
    /**
     * @param string $email
     * @return string
     */
    public static function createIdentity(string $email): string
    {
        return Str::lower($email);
    }
}