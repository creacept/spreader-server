<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 08.11.2018
 * Time: 16:55
 */

namespace App\Helpers\AddLead\Field;

use function GuzzleHttp\Psr7\uri_for;
use Illuminate\Support\Str;

/**
 * Class VK
 * @package App\Helpers\AddLead\Field
 */
class VK
{
    /**
     * @param string $url
     * @return string
     * @throws \Throwable
     */
    public static function getIdentityFromUrl(string $url) : string
    {
        try {
            $uri = uri_for($url);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $host = $uri->getHost();
        if ($host !== 'vk.com' && $host !== 'm.vk.com') {
            throw new \InvalidArgumentException("Invalid domain in \"{$url}\"");
        }

        $path = $uri->getPath();
        if (Str::startsWith($path, '/')) {
            $path = Str::substr($path, 1);
        }
        if (Str::endsWith($path, '/')) {
            $path = Str::substr($path, 0, -1);
        }
        if ($path === '' || Str::contains($path, '/')) {
            throw new \InvalidArgumentException("Invalid identity in \"{$url}\"");
        }
        $identity = rawurldecode($path);

        return $identity;
    }

    /**
     * @param string $identity
     * @return string
     */
    public static function getUrlFromIdentity(string $identity) : string
    {
        return 'https://vk.com/' . rawurlencode($identity);
    }
}