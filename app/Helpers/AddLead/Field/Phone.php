<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 01.08.2018
 * Time: 15:54
 */

namespace App\Helpers\AddLead\Field;

use Illuminate\Support\Str;
use InvalidArgumentException;

/**
 * Class Phone
 * @package App\Helpers\AddLead\Field
 */
class Phone
{
    public const IDENTITY_LENGTH = 10;

    /**
     * @param string $phone
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function createIdentity(string $phone): string
    {
        $identity = preg_replace('/[^0-9]+/', '', $phone);

        $identity = Str::substr($identity, -1 * self::IDENTITY_LENGTH);
        if (Str::length($identity) < self::IDENTITY_LENGTH) {
            throw new InvalidArgumentException(sprintf(
                'Invalid phone "%s": minimum allowed length is %d digits, but the phone contains only %d digits.',
                $phone,
                self::IDENTITY_LENGTH,
                Str::length($identity)
            ));
        }

        return $identity;
    }

    /**
     * @param string $phone
     * @return string
     */
    public static function clear(string $phone): string
    {
        return preg_replace('/[^0-9+]+/', '', $phone);
    }

    /**
     * @param string $phone
     * @return string
     */
    public static function addSign(string $phone): string
    {
        $phone = self::clear($phone);
        if (mb_substr($phone, 0, 1) === '+') {
            return $phone;
        }

        return "+{$phone}";
    }
}