<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 09.08.2018
 * Time: 17:36
 */

namespace App\Helpers\AddLead\Task;

use App\Helpers\AddLead\Field\Email;
use App\Helpers\AddLead\Field\Phone;
use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Models\Application;
use App\Models\CRM;
use App\Models\Project;
use Bitrix24\CRM\Contact;
use Illuminate\Support\Arr;
use RuntimeException;
use Throwable;

/**
 * Class UpdatingContact
 * @package App\Helpers\AddLead\Task
 */
class UpdatingContact implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var Application
     */
    private $application;
    /**
     * @var array
     */
    private $contact;

    /**
     * @return TaskInterface|null
     * @throws Throwable
     */
    public function execute(): ?TaskInterface
    {
        if (! $this->application instanceof Application) {
            throw new RuntimeException('Use method "setApplication" for set application');
        }
        if (! is_array($this->contact)) {
            throw new RuntimeException('Use method "setContact" for set contact');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        try {
            $this->update();
        } catch (Throwable $exception) {
            throw $exception;
        }

        $contact = $this->contact;
        $contactId = $contact['ID'];
        $bitrix24UserId = $contact['ASSIGNED_BY_ID'];

        $task = resolve(ManagerVerification::class);
        $task->setContactId($contactId);
        $task->setApplication($this->application);
        $task->setBitrix24UserId($bitrix24UserId);
        return $task;
    }

    /**
     * @param Application $application
     */
    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    /**
     * @param array $contact
     */
    public function setContact(array $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @throws \Throwable
     */
    private function update()
    {
        $contact = $this->contact;
        $fields = [];

        $emailFields = $this->updateEmails();
        $fields = array_merge($fields, $emailFields);

        $phoneFields = $this->updatePhones();
        $fields = array_merge($fields, $phoneFields);

        $vkFields = $this->updateVK();
        $fields = array_merge($fields, $vkFields);

        $identityFields = $this->updateIdentity();
        $fields = array_merge($fields, $identityFields);

        $surveyFields = $this->application->sender_extra_fields;
        if (is_array($surveyFields) && count($surveyFields) > 0) {
            $fields = array_merge($fields, $surveyFields);
        }

        if (count($fields) === 0) {
            return;
        }

        try {
            $client = $this->clientFactory->create($this->crm);
            $api = new Contact($client);
            $api->update($contact['ID'], $fields);
        } catch (Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @return array
     */
    private function updateEmails(): array
    {
        $fields = [];
        $application = $this->application;
        if (! is_string($application->sender_email)) {
            return $fields;
        }
        $senderEmail = Email::createIdentity($application->sender_email);

        $contact = $this->contact;
        foreach (($contact['EMAIL'] ?? []) as $data) {
            $email = Email::createIdentity($data['VALUE']);
            if ($email === $senderEmail) {
                return $fields;
            }
        }

        $fields['EMAIL'] = [[
            'VALUE_TYPE' => 'HOME',
            'VALUE' => $application->sender_email,
        ]];

        return $fields;
    }

    private function updatePhones(): array
    {
        $fields = [];
        $application = $this->application;
        if (! is_string($application->sender_phone)) {
            return $fields;
        }

        try {
            Phone::createIdentity($application->sender_phone);
            $senderPhone = Phone::clear($application->sender_phone);
        } catch (Throwable $e) {
            return $fields;
        }

        $contact = $this->contact;
        foreach (($contact['PHONE'] ?? []) as $data) {
            $phone = Phone::clear($data['VALUE']);
            if ($phone === $senderPhone) {
                return $fields;
            }
        }

        $fields['PHONE'] = [[
            'VALUE_TYPE' => 'MOBILE',
            'VALUE' => $senderPhone,
        ]];

        return $fields;
    }

    /**
     * @return array
     */
    private function updateVK(): array
    {
        $fields = [];
        $application = $this->application;
        if (! is_string($application->sender_vk)) {
            return $fields;
        }

        $contact = $this->contact;
        foreach (($contact['WEB'] ?? []) as $data) {
            if ($data['VALUE_TYPE'] === 'VK' && $data['VALUE'] === $application->sender_vk) {
                return $fields;
            }
        }

        $fields['WEB'] = [[
            'VALUE_TYPE' => 'VK',
            'VALUE' => $application->sender_vk,
        ]];

        return $fields;
    }

    /**
     * @return array
     */
    private function updateIdentity(): array
    {
        $fields = [];
        $project = $this->application->site->project;
        if (! $project instanceof Project) {
            return $fields;
        }

        $contact = $this->contact;
        $identity = ($contact[Arr::get($this->crm->config, 'contact.identity')] ?? null);
        if ($identity === null || (int) $identity !== $project->contact_identity) {
            $fields[Arr::get($this->crm->config, 'contact.identity')] = $project->contact_identity;
        }

        return $fields;
    }
}