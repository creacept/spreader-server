<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 26.07.2018
 * Time: 19:30
 */

namespace App\Helpers\AddLead\Task;

use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\Bitrix\Workflow;
use App\Models\Application;
use App\Models\CRM;
use App\Models\Project;
use Bitrix24\CRM\Lead;
use Illuminate\Support\Arr;
use RuntimeException;
use Throwable;

/**
 * Class CreatingLead
 * @package App\Helpers\AddLead\Task
 */
class CreatingLead implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var string
     */
    private const CRM_CONVERTED_STATUS_ID = '53';

    /**
     * @var \Bitrix24\CRM\Lead
     */
    private $api;
    
    /**
     * @var int
     */
    private $contactId;
    /**
     * @var Application
     */
    private $application;
    /**
     * @var int|null
     */
    private $bitrix24UserId;
    /**
     * @var int|null
     */
    private $activeLeadId;
    /**
     * @var int|null
     */
    private $activeDealId;
    /**
     * @var bool
     */
    private $isNew;

    /**
     * CreatingLead constructor.
     */
    public function __construct()
    {
        $this->isNew = false;
    }

    /**
     * @return TaskInterface|null
     * @throws Throwable
     */
    public function execute(): ?TaskInterface
    {
        if (! $this->application instanceof Application) {
            throw new RuntimeException('Use method "setApplication" for set application');
        }
        if (! is_int($this->contactId)) {
            throw new RuntimeException('Use method "setContactId" for set contact ID');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        try {
            $client = $this->clientFactory->create($this->crm);
            $this->api = new Lead($client);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $fields = [
            'CONTACT_ID' => $this->contactId,
            'TITLE' => $this->application->form_name,
            'PHONE' => [[
                'VALUE_TYPE' => 'MOBILE',
                'VALUE' => $this->application->sender_phone,
            ]],
        ];

        if (is_string($this->application->page_url)) {
            $fields[Arr::get($this->crm->config, 'lead.url')] = $this->application->page_url;
        }

        if (is_string($this->application->page_title)) {
            $fields[Arr::get($this->crm->config, 'lead.title')] = $this->application->page_title;
        }

        if ($this->isNew) {
            $fields[Arr::get($this->crm->config, 'lead.is_new')] = 1;
        }

        if (is_int($this->bitrix24UserId)) {
            $fields['ASSIGNED_BY_ID'] = $this->bitrix24UserId;
        }

        if (is_string($this->application->sender_email)) {
            $fields['EMAIL'] = [[
                'VALUE_TYPE' => 'HOME',
                'VALUE' => $this->application->sender_email,
            ]];
        }

        if (is_string($this->application->sender_vk)) {
            $fields['WEB'] = [[
                'VALUE_TYPE' => 'VK',
                'VALUE' => $this->application->sender_vk,
            ]];
        }

        $project = $this->application->site->project;
        if ($project instanceof Project) {
            $fields[Arr::get($this->crm->config, 'lead.identity')] = $project->lead_identity;
        }

        // Complicated logic for applications depends on email and phone
        if (is_string($this->application->sender_phone)) {
            if (is_int($this->activeDealId)) {
                $fields = $this->markLeadAsJunk($fields);
            } elseif (is_int($this->activeLeadId)) {
                $this->markActiveLeadAsConverted();
            }
        } else {
            if (is_int($this->activeLeadId)) {
                $fields = $this->markLeadAsJunk($fields);
            }
        }
        //

        if (is_string($this->application->utm_campaign)) {
            $fields['UTM_CAMPAIGN'] = $this->application->utm_campaign;
        }

        if (is_string($this->application->utm_content)) {
            $fields['UTM_CONTENT'] = $this->application->utm_content;
        }

        if (is_string($this->application->utm_medium)) {
            $fields['UTM_MEDIUM'] = $this->application->utm_medium;
        }

        if (is_string($this->application->utm_source)) {
            $fields['UTM_SOURCE'] = $this->application->utm_source;
        }

        if (is_string($this->application->sub_id)) {
            $fields[Arr::get($this->crm->config, 'lead.sub_id')] = $this->application->sub_id;
        }

        if (is_string($this->application->comment)) {
            $fields['COMMENTS'] = $this->application->comment;
        }

        $extraFieldsKey = Arr::get($this->crm->config, 'lead.extra_fields');
        if ($extraFieldsKey !== null && is_array($this->application->extra_fields)){
            $extraFieldsString = "";
            foreach ($this->application->extra_fields as $index => $extra_field){
                $extraFieldsString .= "$index: $extra_field;";
            }
            $fields[$extraFieldsKey] = $extraFieldsString;
        }

        $leadExtraFields = $this->application->lead_extra_fields;
        if (is_array($leadExtraFields)) {
            foreach ($leadExtraFields as $fieldId => $value) {
                $fields[$fieldId] = $value;
            }
        }

        try {
            $result = $this->api->add($fields, ['REGISTER_SONET_EVENT' => 'Y']);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $leadId = $result['result'];

        try {
            $client = $this->clientFactory->create($this->crm);
            $this->api = new Workflow($client);
            $this->api->start(
                Arr::get($this->crm->config, 'workflow.after_create_lead'),
                ['crm', 'CCrmDocumentLead', $leadId]
            );
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (! is_int($this->activeLeadId) && ! is_int($this->activeDealId)) {
            return null;
        }

        try {
            $task = resolve(DuplicatingNotification::class);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $task->setLeadId($leadId);
        $task->setLeadTitle($this->application->form_name);
        $task->setActiveLeadId($this->activeLeadId);
        $task->setActiveDealId($this->activeDealId);
        $task->setBitrix24UserId($this->bitrix24UserId);
        return $task;
    }

    /**
     * @param int $contactId
     */
    public function setContactId(int $contactId): void
    {
        $this->contactId = $contactId;
    }

    /**
     * @param Application $application
     */
    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    /**
     * @param int|null $bitrix24UserId
     */
    public function setBitrix24UserId(?int $bitrix24UserId): void
    {
        $this->bitrix24UserId = $bitrix24UserId;
    }

    /**
     * @param int|null $activeLeadId
     */
    public function setActiveLeadId(?int $activeLeadId): void
    {
        $this->activeLeadId = $activeLeadId;
    }

    /**
     * @param int|null $activeDealId
     */
    public function setActiveDealId(?int $activeDealId): void
    {
        $this->activeDealId = $activeDealId;
    }

    /**
     * @param bool $isNew
     */
    public function setIsNew(bool $isNew): void
    {
        $this->isNew = $isNew;
    }

    /**
     * @param array $fields
     * @return array
     */
    private function markLeadAsJunk(array $fields): array
    {
        $fields[Arr::get($this->crm->config, 'lead.is_new')] = 0;
        $fields['STATUS_ID'] = 'JUNK';
        return $fields;
    }

    /**
     * @return bool
     */
    private function markActiveLeadAsConverted(): bool
    {
        $result = $this->api->update(
            $this->activeLeadId,
            ['STATUS_ID' => self::CRM_CONVERTED_STATUS_ID],
            ['REGISTER_SONET_EVENT' => 'Y']
        );

        return Arr::get($result, 'error') ? false : true;
    }
}
