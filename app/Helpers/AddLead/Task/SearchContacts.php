<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 20.07.2018
 * Time: 11:21
 */

namespace App\Helpers\AddLead\Task;

use App\Helpers\AddLead\Field\Email;
use App\Helpers\AddLead\Field\Phone;
use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Models\Application;
use App\Models\CRM;
use App\Models\Project;
use Bitrix24\CRM\Contact;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Support\Arr;
use RuntimeException;
use Throwable;

/**
 * Class SearchContacts
 * @package App\Helpers\AddLead\Task
 */
class SearchContacts implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var \Illuminate\Database\Connectors\ConnectionFactory
     */
    private $connectionFactory;

    /**
     * @var \App\Models\Application
     */
    private $application;

    /**
     * @var \Bitrix24\CRM\Contact
     */
    private $api;

    /**
     * SearchContacts constructor.
     * @param \Illuminate\Database\Connectors\ConnectionFactory $connectionFactory
     */
    public function __construct(ConnectionFactory $connectionFactory)
    {
        $this->connectionFactory = $connectionFactory;
    }

    /**
     * @return TaskInterface|null
     * @throws Throwable
     */
    public function execute(): ?TaskInterface
    {
        if (! $this->application instanceof Application) {
            throw new RuntimeException('Use method "setApplication" for set application');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        if (! is_string($this->application->sender_email)) {
            return null;
        }

        $emailIdentity = Email::createIdentity($this->application->sender_email);
        try { $phoneIdentity = Phone::createIdentity($this->application->sender_phone); }
            catch (Throwable $exception) { $phoneIdentity = null; }

        try {
            $client = $this->clientFactory->create($this->crm);
            $this->api = new Contact($client);

            $contactsByPhone = $this->getContactsByPhone($phoneIdentity);
            $contactsByEmail = $this->getContactsByEmail($emailIdentity);
            $uniqueContacts = $this->removeDuplicatedContacts($contactsByPhone, $contactsByEmail);

            if (count($uniqueContacts) === 0) {
                $task = resolve(CreatingContact::class);
                $task->setApplication($this->application);
            } elseif (count($uniqueContacts) === 1) {
                $contact = reset($uniqueContacts);
                $task = resolve(UpdatingContact::class);
                $task->setApplication($this->application);
                $task->setContact($contact);
            } else {
                $task = resolve(GluingContacts::class);
                $task->setApplication($this->application);
                $task->setContacts($uniqueContacts);
            }
        } catch (Throwable $exception) {
            throw $exception;
        }

        return $task;
    }

    /**
     * @param \App\Models\Application $application
     * @return void
     */
    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    /**
     * @param string $email
     * @return array
     * @throws \Throwable
     */
    private function getContactsByEmail(string $email): array
    {
        $contacts = [];
        try {
            $list = $this->api->getList(
                [Arr::get($this->crm->config, 'contact.worked_at') => 'DESC'],
                ['EMAIL' => $email, 'HAS_EMAIL' => 'Y'],
                ['ASSIGNED_BY_ID', 'PHONE', 'EMAIL', 'WEB', Arr::get($this->crm->config, 'contact.identity')]
            );
            foreach ($list['result'] as $contact) {
                $contacts[$contact['ID']] = $contact;
            }
        } catch (Throwable $exception) {
            throw $exception;
        }

        $contacts = $this->filterContacts($contacts);
        return $contacts;
    }

    /**
     * @param string|null $phone
     * @return array
     * @throws \Throwable
     */
    private function getContactsByPhone(string $phone = null): array
    {
        if ($phone === null) {
            return [];
        }

        $contactIdentityField = Arr::get($this->crm->config, 'lead.contact_id');
        if ($contactIdentityField === null) {
            throw new RuntimeException('Field "contact`s identity" is not filled for crm ' . $this->crm->name);
        }

        $contacts = [];
        try {
            $list = $this->api->getList(
                [Arr::get($this->crm->config, 'contact.worked_at') => 'DESC'],
                ['?'.$contactIdentityField => $phone, 'HAS_PHONE' => 'Y'],
                ['ASSIGNED_BY_ID', 'PHONE', 'EMAIL', 'WEB', Arr::get($this->crm->config, 'contact.identity')]
            );
            foreach ($list['result'] as $contact) {
                $contacts[$contact['ID']] = $contact;
            }
        } catch (Throwable $exception) {
            throw $exception;
        }

        $contacts = $this->filterContacts($contacts);
        return $contacts;
    }

    /**
     * @param array $contacts
     * @return array
     */
    private function filterContacts(array $contacts): array
    {
        return array_filter($contacts, function (array $contact) {
            $identity = $contact[Arr::get($this->crm->config, 'contact.identity')];
            if ($identity === null) {
                return true;
            }
            $identity = (int) $identity;

            $project = $this->application->site->project;
            if (! $project instanceof Project) {
                return true;
            }

            return ($identity === $project->contact_identity);
        });
    }

    /**
     * @param array $contactsByPhone
     * @param array $contactsByEmail
     * @return array
     */
    private function removeDuplicatedContacts(array $contactsByPhone, array $contactsByEmail): array
    {
        $allContacts = array_map('serialize', array_merge($contactsByPhone, $contactsByEmail));
        $allUniqueContacts = array_unique($allContacts);

        return array_map('unserialize', $allUniqueContacts);
    }
}