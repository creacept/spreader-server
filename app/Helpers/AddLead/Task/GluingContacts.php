<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 27.07.2018
 * Time: 11:28
 */

namespace App\Helpers\AddLead\Task;

use App\Helpers\AddLead\Field\Email;
use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Models\Application;
use App\Models\CRM;
use Bitrix24\CRM\Contact;
use Bitrix24\CRM\Deal\Deal;
use Bitrix24\CRM\Lead;
use Bitrix24\Im\Im;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RuntimeException;
use Throwable;

/**
 * Class GluingContacts
 * @package App\Helpers\AddLead\Task
 */
class GluingContacts implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var \Illuminate\Database\Connectors\ConnectionFactory
     */
    private $connectionFactory;
    /**
     * @var Application
     */
    private $application;
    /**
     * @var array
     */
    private $contacts;

    /**
     * GluingContacts constructor.
     * @param \Illuminate\Database\Connectors\ConnectionFactory $connectionFactory
     */
    public function __construct(ConnectionFactory $connectionFactory)
    {
        $this->connectionFactory = $connectionFactory;
    }

    /**
     * @return TaskInterface|null
     * @throws Throwable
     */
    public function execute(): ?TaskInterface
    {
        if (! $this->application instanceof Application) {
            throw new RuntimeException('Use method "setApplication" for set application');
        }
        if (! is_array($this->contacts)) {
            throw new RuntimeException('Use method "setContacts" for set contacts');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        if (empty($this->contacts)) {
            Log::error("Contacts can not be empty. Application ID = {$this->application->id}");
            return null;
        }

        $formattedContacts = print_r($this->contacts, true);

        $contacts = $this->contacts;
        $contact = array_shift($contacts);
        foreach ($contacts as $duplicate) {
            try {
                $this->handleContact($contact, $duplicate);
            } catch (Throwable $exception) {
                throw $exception;
            }
        }

        try {
            $client = $this->clientFactory->create($this->crm);
            $api = new Contact($client);
            $result = $api->get($contact['ID']);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $contact = $result['result'];

        try {
            $task = resolve(UpdatingContact::class);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $task->setContact($contact);
        $task->setApplication($this->application);

        return $task;
    }

    /**
     * @param Application $application
     */
    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    /**
     * @param array $contacts
     */
    public function setContacts(array $contacts): void
    {
        $this->contacts = $contacts;
    }

    /**
     * @param array $contact
     * @param array $duplicate
     * @throws Throwable
     */
    private function handleContact(array $contact, array $duplicate): void
    {
        $contactId = $contact['ID'];
        $bitrix24UserId = $contact['ASSIGNED_BY_ID'];
        $duplicateContactId = $duplicate['ID'];

        try {
            $this->handleLeads($duplicateContactId, $contactId, $bitrix24UserId);
            $this->handleDeals($duplicateContactId, $contactId, $bitrix24UserId);
            $this->glueContact($contact, $duplicate);
        } catch (Throwable $exception) {
            throw $exception;
        }

        try {
            $client = $this->clientFactory->create($this->crm);
            $api = new Contact($client);
            $api->delete($duplicate['ID']);
        } catch (Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param int $duplicateContactId
     * @param int $contactId
     * @param int $bitrix24UserId
     * @throws Throwable
     */
    private function handleLeads(int $duplicateContactId, int $contactId, int $bitrix24UserId): void
    {
        try {
            $client = $this->clientFactory->create($this->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $api = new Lead($client);

        $leads = [];
        $start = 0;
        do {
            try {
                $result = $api->getList(
                    [],
                    ['CONTACT_ID' => $duplicateContactId],
                    ['ASSIGNED_BY_ID', 'STATUS_SEMANTIC_ID', 'TITLE'],
                    $start
                );
            } catch (Throwable $exception) {
                throw $exception;
            }
            $start = $result['next'] ?? false;

            foreach ($result['result'] as $lead) {
                $leads[] = $lead;
            }
        } while ($start !== false);

        foreach ($leads as $lead) {
            try {
                $this->handleLead($lead, $contactId, $bitrix24UserId);
            } catch (Throwable $exception) {
                throw $exception;
            }
        }
    }

    /**
     * @param array $lead
     * @param int $contactId
     * @param int $bitrix24UserId
     * @throws Throwable
     */
    private function handleLead(array $lead, int $contactId, int $bitrix24UserId): void
    {
        try {
            $connection = $this->connectionFactory->make([
                'driver' => 'mysql',
                'host' => Arr::get($this->crm->config, 'db.host'),
                'port' => Arr::get($this->crm->config, 'db.port'),
                'database' => Arr::get($this->crm->config, 'db.database'),
                'username' => Arr::get($this->crm->config, 'db.login'),
                'password' => Arr::get($this->crm->config, 'db.password'),
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ]);

            DB::transaction(function () use ($connection, $lead, $contactId, $bitrix24UserId) {
                $connection
                    ->table('b_crm_lead_contact')
                    ->where('LEAD_ID', $lead['ID'])
                    ->where('IS_PRIMARY', 'Y')
                    ->update(['CONTACT_ID' => $contactId]);

                $connection
                    ->table('b_crm_lead')
                    ->where('ID', $lead['ID'])
                    ->update([
                        'ASSIGNED_BY_ID' => $bitrix24UserId,
                        'CONTACT_ID' => $contactId,
                    ]);
            });

            $connection->disconnect();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ((int) $lead['ASSIGNED_BY_ID'] === $bitrix24UserId || $lead['STATUS_SEMANTIC_ID'] !== 'P') {
            return;
        }

        $message = sprintf(
            'Вы стали новым ответственным для лида [URL=/crm/lead/details/%d/]%s[/URL]',
            $lead['ID'],
            e($lead['TITLE'])
        );
        try {
            $this->sendMessage($message, $bitrix24UserId);
        } catch (Throwable $exception) {
            report($exception);
        }
    }

    /**
     * @param int $duplicateContactId
     * @param int $contactId
     * @param int $bitrix24UserId
     * @throws Throwable
     */
    private function handleDeals(int $duplicateContactId, int $contactId, int $bitrix24UserId): void
    {
        try {
            $client = $this->clientFactory->create($this->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $api = new Deal($client);

        $deals = [];
        $start = 0;
        do {
            try {
                $result = $api->getList(
                    [],
                    ['CONTACT_ID' => $duplicateContactId],
                    ['ASSIGNED_BY_ID', 'STAGE_SEMANTIC_ID', 'TITLE'],
                    $start
                );
            } catch (Throwable $exception) {
                throw $exception;
            }
            $start = $result['next'] ?? false;

            foreach ($result['result'] as $deal) {
                $deals[] = $deal;
            }
        } while ($start !== false);

        foreach ($deals as $deal) {
            try {
                $this->handleDeal($deal, $contactId, $bitrix24UserId);
            } catch (Throwable $exception) {
                throw $exception;
            }
        }
    }

    /**
     * @param array $deal
     * @param int $contactId
     * @param int $bitrix24UserId
     * @throws Throwable
     */
    private function handleDeal(array $deal, int $contactId, int $bitrix24UserId): void
    {
        try {
            $connection = $this->connectionFactory->make([
                'driver' => 'mysql',
                'host' => Arr::get($this->crm->config, 'db.host'),
                'port' => Arr::get($this->crm->config, 'db.port'),
                'database' => Arr::get($this->crm->config, 'db.database'),
                'username' => Arr::get($this->crm->config, 'db.login'),
                'password' => Arr::get($this->crm->config, 'db.password'),
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ]);

            DB::transaction(function () use ($connection, $deal, $contactId, $bitrix24UserId) {
                $connection
                    ->table('b_crm_deal_contact')
                    ->where('DEAL_ID', $deal['ID'])
                    ->where('IS_PRIMARY', 'Y')
                    ->update(['CONTACT_ID' => $contactId]);
                $connection
                    ->table('b_crm_deal')
                    ->where('ID', $deal['ID'])
                    ->update([
                        'ASSIGNED_BY_ID' => $bitrix24UserId,
                        'CONTACT_ID' => $contactId,
                    ]);
            });

            $connection->disconnect();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ((int) $deal['ASSIGNED_BY_ID'] === $bitrix24UserId || $deal['STAGE_SEMANTIC_ID'] !== 'P') {
            return;
        }

        $message = sprintf(
            'Вы стали новым ответственным для сделки [URL=/crm/deal/details/%d/]%s[/URL]',
            $deal['ID'],
            e($deal['TITLE'])
        );
        try {
            $this->sendMessage($message, $bitrix24UserId);
        } catch (Throwable $exception) {
            report($exception);
        }
    }

    /**
     * @param string $message
     * @param int $bitrix24UserId
     * @throws Throwable
     */
    private function sendMessage(string $message, int $bitrix24UserId) : void
    {
        try {
            $client = $this->clientFactory->create($this->crm);
            $api = new Im($client);
            $api->notify($bitrix24UserId, $message, 'SYSTEM');
        } catch (Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param array $contact
     * @param array $duplicate
     * @throws Throwable
     */
    private function glueContact(array $contact, array $duplicate)
    {
        static $existingPhones = null;
        if (! is_array($existingPhones)) {
            $existingPhones = [];
            $phones =  ($contact['PHONE'] ?? []);
            foreach ($phones as $data) {
                $existingPhones[] = $data['VALUE'];
            }
        }

        static $existingEmails = null;
        if (! is_array($existingEmails)) {
            $existingEmails = [];
            $emails = ($contact['EMAIL'] ?? []);
            foreach ($emails as $data) {
                $existingEmails[] = $data['VALUE'];
            }
        }

        static $existingVK = null;
        if (! is_array($existingVK)) {
            $existingVK = [];
            $sites = ($contact['WEB'] ?? []);
            foreach ($sites as $data) {
                if ($data['VALUE_TYPE'] === 'VK') {
                    $existingVK[] = $data['VALUE'];
                }
            }
        }

        $fields = [];
        $phones = ($duplicate['PHONE'] ?? []);
        foreach ($phones as $data) {
            $value = $data['VALUE'];
            if (in_array($value, $existingPhones)) {
                continue;
            }

            $existingPhones[] = $value;
            $fields['PHONE'][] = [
                'VALUE_TYPE' => $data['VALUE_TYPE'],
                'VALUE' => $value,
            ];
        }

        $emails = ($duplicate['EMAIL'] ?? []);
        foreach ($emails as $data) {
            $value = $data['VALUE'];
            $email = Email::createIdentity($value);
            if (in_array($email, $existingEmails)) {
                continue;
            }

            $existingEmails[] = $value;
            $fields['EMAIL'][] = [
                'VALUE_TYPE' => $data['VALUE_TYPE'],
                'VALUE' => $value,
            ];
        }

        $sites = ($duplicate['WEB'] ?? []);
        foreach ($sites as $data) {
            if ($data['VALUE_TYPE'] !== 'VK') {
                continue;
            }

            $value = $data['VALUE'];
            if (in_array($value, $existingVK)) {
                continue;
            }

            $existingVK[] = $value;
            $fields['WEB'][] = [
                'VALUE_TYPE' => 'VK',
                'VALUE' => $value,
            ];
        }

        $surveyFields = $this->application->sender_extra_fields;
        if (is_array($surveyFields) && count($surveyFields) > 0) {
            $fields = array_merge($fields, $surveyFields);
        }

        if (count($fields) === 0) {
            return;
        }

        try {
            $client = $this->clientFactory->create($this->crm);
            $api = new Contact($client);
            $api->update($contact['ID'], $fields);
        } catch (Throwable $exception) {
            throw $exception;
        }
    }
}