<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 23.07.2018
 * Time: 11:20
 */

namespace App\Helpers\AddLead\Task;

use App\Helpers\AddLead\Field\Phone;
use App\Helpers\AddLead\Field\VK;
use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\Service\ManagerResolverService;
use App\Models\Application;
use App\Models\CRM;
use App\Models\Manager;
use App\Models\Project;
use Bitrix24\CRM\Contact;
use Illuminate\Support\Arr;
use RuntimeException;
use Throwable;

/**
 * Class CreatingContact
 * @package App\Helpers\AddLead\Task
 */
class CreatingContact implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var ManagerResolverService
     */
    private $managerResolverService;
    /**
     * @var Application
     */
    private $application;

    /**
     * CreatingContact constructor.
     * @param ManagerResolverService $managerResolverService
     */
    public function __construct(ManagerResolverService $managerResolverService)
    {
        $this->managerResolverService = $managerResolverService;
    }

    /**
     * @return TaskInterface|null
     * @throws Throwable
     */
    public function execute(): ?TaskInterface
    {
        if (! $this->application instanceof Application) {
            throw new RuntimeException('Use method "setApplication" for set application');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        try {
            $client = $this->clientFactory->create($this->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $api = new Contact($client);

        $contactIdentityField = Arr::get($this->crm->config, 'lead.contact_id');
        if ($contactIdentityField === null) {
            throw new RuntimeException('Field "contact identificator" not filled for crm ' . $this->crm->name);
        }

        if (! is_string($this->application->sender_email)) {
            \Log::warning("Application {$this->application->id} didn't have an email");
            return null;
        }

        $fields = [
            'NAME' => $this->application->sender_name
                ?? $this->application->sender_phone
                ?? $this->application->sender_email
        ];

        if (is_string($this->application->sender_phone)) {
            $fields[$contactIdentityField] = Phone::createIdentity($this->application->sender_phone);
            $fields['PHONE'] = [[
                'VALUE_TYPE' => 'MOBILE',
                'VALUE' => $this->application->sender_phone,
            ]];
        }

        $fields['EMAIL'] = [[
            'VALUE_TYPE' => 'HOME',
            'VALUE' => $this->application->sender_email,
        ]];

        try {
            $manager = $this->managerResolverService->resolve($this->application);
        } catch (Throwable $exception) {
            throw $exception;
        }

        $bitrix24UserId = null;
        if ($manager instanceof Manager) {
            $bitrix24UserId = $manager->bitrix24_user_id;
        }

        if (is_int($bitrix24UserId)) {
            $fields['ASSIGNED_BY_ID'] = $bitrix24UserId;
        }

        $project = $this->application->site->project;
        if ($project instanceof Project) {
            $fields[Arr::get($this->crm->config, 'contact.identity')] = $project->contact_identity;
        }

        if (is_string($this->application->sender_vk)) {
            $fields['WEB'] = [[
                'VALUE_TYPE' => 'VK',
                'VALUE' => $this->application->sender_vk,
            ]];
            $fields[Arr::get($this->crm->config, 'contact.vk')] = VK::getUrlFromIdentity($this->application->sender_vk);
        }

        $surveyFields = $this->application->sender_extra_fields;
        if (is_array($surveyFields) && count($surveyFields) > 0) {
            $fields = array_merge($fields, $surveyFields);
        }

        try {
            $result = $api->add($fields, ['REGISTER_SONET_EVENT' => 'Y']);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $contactId = $result['result'];

        try {
            $task = resolve(CreatingLead::class);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $task->setContactId($contactId);
        $task->setApplication($this->application);
        $task->setBitrix24UserId($bitrix24UserId);
        $task->setIsNew(true);
        return $task;
    }

    /**
     * @param Application $application
     */
    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }
}