<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 27.07.2018
 * Time: 14:00
 */

namespace App\Helpers\AddLead\Task;

use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Models\CRM;
use Bitrix24\CRM\LiveFeedMessage\LiveFeedMessage;
use Bitrix24\Task\Item;
use Illuminate\Support\Carbon;
use RuntimeException;
use Throwable;

/**
 * Class DuplicatingNotification
 * @package App\Helpers\AddLead\Task
 */
class DuplicatingNotification implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var int
     */
    private $leadId;
    /**
     * @var string
     */
    private $leadTitle;
    /**
     * @var int|null
     */
    private $activeLeadId;
    /**
     * @var int|null
     */
    private $activeDealId;
    /**
     * @var int|null
     */
    private $bitrix24UserId;

    /**
     * @return TaskInterface|null
     */
    public function execute(): ?TaskInterface
    {
        if (! is_int($this->leadId)) {
            $exception = new RuntimeException('Use method "setLeadId" for set lead ID');
            report($exception);
            return null;
        }
        if (! is_string($this->leadTitle)) {
            $exception = new RuntimeException('Use method "setLeadTitle" for set lead title');
            report($exception);
            return null;
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        if (is_int($this->activeLeadId)) {
            $entityTypeId = 1;
            $entityId = $this->activeLeadId;
            $crmTask = "L_{$this->activeLeadId}";
        } elseif (is_int($this->activeDealId)) {
            $entityTypeId = 2;
            $entityId = $this->activeDealId;
            $crmTask = "D_{$this->activeDealId}";
        } else {
            return null;
        }

        $message = sprintf(
            'Новая заявка [URL=/crm/lead/details/%d/]%s[/URL]',
            $this->leadId,
            e($this->leadTitle)
        );

        if (is_int($this->bitrix24UserId)) {
            try {
                $client = $this->clientFactory->create($this->crm);
                $api = new Item($client);
                $api->add([
                    'TITLE' => 'Повторная заявка',
                    'RESPONSIBLE_ID' => $this->bitrix24UserId,
                    'DESCRIPTION' => $message,
                    'DEADLINE' => Carbon::now()->addDay()->toIso8601String(),
                    'UF_CRM_TASK' => [$crmTask],
                ]);
            } catch (\Throwable $exception) {
                report($exception);
            }
        }

        try {
            $client = $this->clientFactory->create($this->crm);
            $api = new LiveFeedMessage($client);
            $api->add('Новая заявка', $message, [], $entityTypeId, $entityId);
        } catch (Throwable $exception) {
            report($exception);
        }

        return null;
    }

    /**
     * @param int $leadId
     */
    public function setLeadId(int $leadId): void
    {
        $this->leadId = $leadId;
    }

    /**
     * @param string $leadTitle
     */
    public function setLeadTitle(string $leadTitle): void
    {
        $this->leadTitle = $leadTitle;
    }

    /**
     * @param int|null $activeLeadId
     */
    public function setActiveLeadId(?int $activeLeadId): void
    {
        $this->activeLeadId = $activeLeadId;
    }

    /**
     * @param int|null $activeDealId
     */
    public function setActiveDealId(?int $activeDealId): void
    {
        $this->activeDealId = $activeDealId;
    }

    /**
     * @param int|null $bitrix24UserId
     */
    public function setBitrix24UserId(?int $bitrix24UserId): void
    {
        $this->bitrix24UserId = $bitrix24UserId;
    }
}