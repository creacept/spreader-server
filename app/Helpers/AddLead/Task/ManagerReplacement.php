<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 09.08.2018
 * Time: 16:52
 */

namespace App\Helpers\AddLead\Task;

use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\Service\ManagerResolverService;
use App\Models\Application;
use App\Models\CRM;
use App\Models\Manager;
use Bitrix24\CRM\Contact;
use RuntimeException;
use Throwable;

/**
 * Class ManagerReplacement
 * @package App\Helpers\AddLead\Task
 */
class ManagerReplacement implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var ManagerResolverService
     */
    private $managerResolverService;
    /**
     * @var int
     */
    private $contactId;
    /**
     * @var Application
     */
    private $application;
    /**
     * @var int|null
     */
    private $bitrix24UserId;

    /**
     * ManagerReplacement constructor.
     * @param ManagerResolverService $managerResolverService
     */
    public function __construct(ManagerResolverService $managerResolverService)
    {
        $this->managerResolverService = $managerResolverService;
    }

    /**
     * @return TaskInterface|null
     * @throws Throwable
     */
    public function execute(): ?TaskInterface
    {
        if (! $this->application instanceof Application) {
            throw new RuntimeException('Use method "setApplication" for set application');
        }
        if (! is_int($this->contactId)) {
            throw new RuntimeException('Use method "setContactId" for set contact ID');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        try {
            $manager = $this->managerResolverService->resolve($this->application);
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ($manager instanceof Manager) {
            $this->bitrix24UserId = $manager->bitrix24_user_id;
            try {
                $client = $this->clientFactory->create($this->crm);
                $api = new Contact($client);
                $api->update(
                    $this->contactId,
                    ['ASSIGNED_BY_ID' => $this->bitrix24UserId],
                    ['REGISTER_SONET_EVENT' => 'Y']
                );
            } catch (Throwable $exception) {
                throw $exception;
            }
        }

        try {
            $task = resolve(SearchActiveDeal::class);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $task->setApplication($this->application);
        $task->setContactId($this->contactId);
        $task->setBitrix24UserId($this->bitrix24UserId);
        return $task;
    }

    /**
     * @param int $contactId
     */
    public function setContactId(int $contactId): void
    {
        $this->contactId = $contactId;
    }

    /**
     * @param Application $application
     */
    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    /**
     * @param int|null $bitrix24UserId
     */
    public function setBitrix24UserId(?int $bitrix24UserId): void
    {
        $this->bitrix24UserId = $bitrix24UserId;
    }
}