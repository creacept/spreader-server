<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 26.07.2018
 * Time: 20:17
 */

namespace App\Helpers\AddLead\Task;

use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Models\Application;
use App\Models\CRM;
use Bitrix24\CRM\Deal\Deal;
use Illuminate\Support\Arr;
use RuntimeException;
use Throwable;

/**
 * Class SearchActiveDeal
 * @package App\Helpers\AddLead\Task
 */
class SearchActiveDeal implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var int
     */
    private $contactId;
    /**
     * @var Application
     */
    private $application;
    /**
     * @var int|null
     */
    private $bitrix24UserId;

    /**
     * @return TaskInterface|null
     * @throws Throwable
     */
    public function execute(): ?TaskInterface
    {
        if (! $this->application instanceof Application) {
            throw new RuntimeException('Use method "setApplication" for set application');
        }
        if (! is_int($this->contactId)) {
            throw new RuntimeException('Use method "setContactId" for set contact ID');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        try {
            $activeDealId = $this->getActiveDealId();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ($activeDealId === null) {
            try {
                $task = resolve(SearchActiveLead::class);
            } catch (\Throwable $exception) {
                throw $exception;
            }
            $task->setContactId($this->contactId);
            $task->setApplication($this->application);
            $task->setBitrix24UserId($this->bitrix24UserId);
            return $task;
        }

        try {
            $task = resolve(CreatingLead::class);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $task->setContactId($this->contactId);
        $task->setApplication($this->application);
        $task->setBitrix24UserId($this->bitrix24UserId);
        $task->setActiveDealId($activeDealId);
        return $task;
    }

    /**
     * @return int|null
     * @throws \Throwable
     */
    private function getActiveDealId() : ?int
    {
        $ignoredCategories = Arr::get($this->crm->settings, 'ignored_deal_categories', []);

        try {
            $client = $this->clientFactory->create($this->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $api = new Deal($client);

        $start = 0;
        do {
            try {
                $result = $api->getList(
                    ['DATE_CREATE' => 'DESC'],
                    [
                        'STAGE_SEMANTIC_ID' => 'P',
                        'CONTACT_ID' => $this->contactId,
                    ],
                    ['ID', 'CATEGORY_ID'],
                    $start
                );
                $start = $result['next'] ?? false;

                foreach ($result['result'] as $data) {
                    $categoryId = (int) $data['CATEGORY_ID'];
                    if (! in_array($categoryId, $ignoredCategories)) {
                        return $data['ID'];
                    }
                }
            } catch (Throwable $exception) {
                throw $exception;
            }
        } while ($start !== false);

        return null;
    }

    /**
     * @param int $contactId
     */
    public function setContactId(int $contactId): void
    {
        $this->contactId = $contactId;
    }

    /**
     * @param Application $application
     */
    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    /**
     * @param int|null $bitrix24UserId
     */
    public function setBitrix24UserId(?int $bitrix24UserId): void
    {
        $this->bitrix24UserId = $bitrix24UserId;
    }
}