<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 09.08.2018
 * Time: 16:19
 */

namespace App\Helpers\AddLead\Task;

use App\Helpers\Algorithm\TaskInterface;
use App\Models\Application;
use App\Models\Manager;
use RuntimeException;
use Throwable;

/**
 * Class ManagerVerification
 * @package App\Helpers\AddLead\Task
 */
class ManagerVerification implements TaskInterface
{
    /**
     * @var Application
     */
    private $application;
    /**
     * @var int
     */
    private $contactId;
    /**
     * @var int
     */
    private $bitrix24UserId;

    /**
     * @return TaskInterface|null
     * @throws Throwable
     */
    public function execute(): ?TaskInterface
    {
        if (! $this->application instanceof Application) {
            throw new RuntimeException('Use method "setApplication" for set application');
        }

        if (! is_int($this->contactId)) {
            throw new RuntimeException('Use method "setContactId" for set contact ID');
        }

        if (! is_int($this->bitrix24UserId)) {
            throw new RuntimeException('Use method "setBitrix24UserId" for set user ID of Bitrix24');
        }

        $projectId = $this->application->site->project_id;

        try {
            $exists = Manager::whereProjectId($projectId)
                ->whereBitrix24UserId($this->bitrix24UserId)
                ->wherePublish(true)
                ->exists();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ($exists) {
            try {
                $task = resolve(SearchActiveDeal::class);
            } catch (Throwable $exception) {
                throw $exception;
            }
            $task->setApplication($this->application);
            $task->setContactId($this->contactId);
            $task->setBitrix24UserId($this->bitrix24UserId);
            return $task;
        }

        try {
            $task = resolve(ManagerReplacement::class);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $task->setContactId($this->contactId);
        $task->setApplication($this->application);
        return $task;
    }

    /**
     * @param Application $application
     */
    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    /**
     * @param int $contactId
     */
    public function setContactId(int $contactId): void
    {
        $this->contactId = $contactId;
    }

    /**
     * @param int $bitrix24UserId
     */
    public function setBitrix24UserId(int $bitrix24UserId): void
    {
        $this->bitrix24UserId = $bitrix24UserId;
    }
}