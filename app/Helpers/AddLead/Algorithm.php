<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 19.07.2018
 * Time: 12:25
 */

namespace App\Helpers\AddLead;

use App\Helpers\AddLead\Task\SearchContacts;
use App\Helpers\Algorithm\AbstractAlgorithm;
use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Models\Application;
use App\Models\CRM;
use RuntimeException;
use Throwable;

/**
 * Class Algorithm
 * @package App\Helpers\AddLead
 */
class Algorithm extends AbstractAlgorithm
{
    /**
     * @var \App\Helpers\AddLead\Task\SearchContacts
     */
    private $task;
    /**
     * @var \App\Helpers\Bitrix\ClientFactory
     */
    private $clientFactory;
    /**
     * @var \App\Models\CRM
     */
    private $crm;
    /**
     * @var Application
     */
    private $application;

    /**
     * Algorithm constructor.
     * @param \App\Helpers\AddLead\Task\SearchContacts $task
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     */
    public function __construct(SearchContacts $task, ClientFactory $clientFactory)
    {
        $this->task = $task;
        $this->clientFactory = $clientFactory;
    }

    /**
     * @param Application $application
     */
    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    /**
     * @param \App\Models\CRM $crm
     */
    public function setCrm(CRM $crm): void
    {
        $this->crm = $crm;
    }

    /**
     * @return TaskInterface
     * @throws Throwable
     */
    protected function getFirstTask(): TaskInterface
    {
        if (! $this->application instanceof Application) {
            throw new RuntimeException('Use method "setApplication" for set application');
        }

        $task = $this->task;
        $task->setApplication($this->application);

        return $task;
    }

    /**
     * @param \App\Helpers\Algorithm\TaskInterface $task
     * @return \App\Helpers\Algorithm\TaskInterface
     */
    protected function prepareTask(TaskInterface $task): TaskInterface
    {
        if (! $task instanceof ClientAwareInterface) {
            return $task;
        }

        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        $task->setClientFactory($this->clientFactory);
        $task->setCrm($this->crm);

        return $task;
    }
}