<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 20.03.2019
 * Time: 12:29
 */

namespace App\Helpers\Algorithm;

use App\Helpers\Bitrix\ClientFactory;
use App\Models\CRM;

/**
 * Interface ClientAwareInterface
 * @package App\Helpers\Algorithm
 */
interface ClientAwareInterface
{
    /**
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     */
    public function setClientFactory(ClientFactory $clientFactory): void;

    /**
     * @param \App\Models\CRM $crm
     */
    public function setCrm(CRM $crm): void;
}