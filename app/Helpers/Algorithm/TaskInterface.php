<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 19.07.2018
 * Time: 17:28
 */

namespace App\Helpers\Algorithm;

/**
 * Interface StageInterface
 * @package App\Helpers\Algorithm
 */
interface TaskInterface
{
    /**
     * @return TaskInterface|mixed
     */
    public function execute();
}