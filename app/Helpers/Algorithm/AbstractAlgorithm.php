<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 19.07.2018
 * Time: 17:36
 */

namespace App\Helpers\Algorithm;

use Throwable;

/**
 * Class AbstractAlgorithm
 * @package App\Helpers\Algorithm
 */
abstract class AbstractAlgorithm
{
    /**
     * @return mixed
     * @throws \Throwable
     */
    final public function execute()
    {
        try {
            $task = $this->getFirstTask();
            do {
                $this->prepareTask($task);
                $task = $task->execute();
            } while ($task instanceof TaskInterface);
        } catch (Throwable $exception) {
            throw $exception;
        }

        return $task;
    }

    /**
     * @return TaskInterface
     */
    abstract protected function getFirstTask(): TaskInterface;

    /**
     * @param \App\Helpers\Algorithm\TaskInterface $task
     * @return \App\Helpers\Algorithm\TaskInterface
     */
    protected function prepareTask(TaskInterface $task): TaskInterface
    {
        return $task;
    }
}