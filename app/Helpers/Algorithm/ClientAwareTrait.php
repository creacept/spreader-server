<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 20.03.2019
 * Time: 12:30
 */

namespace App\Helpers\Algorithm;

use App\Helpers\Bitrix\ClientFactory;
use App\Models\CRM;

/**
 * Trait ClientAwareTrait
 * @package App\Helpers\Algorithm
 */
trait ClientAwareTrait
{
    /**
     * @var \App\Helpers\Bitrix\ClientFactory
     */
    protected $clientFactory;
    /**
     * @var \App\Models\CRM
     */
    protected $crm;

    /**
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     */
    public function setClientFactory(ClientFactory $clientFactory): void
    {
        $this->clientFactory = $clientFactory;
    }

    /**
     * @param \App\Models\CRM $crm
     */
    public function setCrm(CRM $crm): void
    {
        $this->crm = $crm;
    }
}