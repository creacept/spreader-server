<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 19.03.2019
 * Time: 14:25
 */

namespace App\Helpers\Bitrix;

use App\Helpers\Authorization\AuthorizationService;
use App\Models\CRM;
use Illuminate\Config\Repository;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Arr;

/**
 * Class ClientFactory
 * @package App\Helpers\Bitrix
 */
class ClientFactory
{
    /**
     * @var \Illuminate\Config\Repository
     */
    private $config;
    /**
     * @var \App\Helpers\Authorization\AuthorizationService
     */
    private $authorizationService;
    /**
     * @var \Illuminate\Routing\UrlGenerator
     */
    private $urlGenerator;

    /**
     * ClientFactory constructor.
     * @param \Illuminate\Config\Repository $config
     * @param \Illuminate\Routing\UrlGenerator $urlGenerator
     * @param \App\Helpers\Authorization\AuthorizationService $authorizationService
     */
    public function __construct(Repository $config, UrlGenerator $urlGenerator, AuthorizationService $authorizationService)
    {
        $this->config = $config;
        $this->urlGenerator = $urlGenerator;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @param \App\Models\CRM $crm
     * @return \App\Helpers\Bitrix\ApiClient|\App\Helpers\Bitrix\WebhookClient
     * @throws \Throwable
     */
    public function create(CRM $crm): AbstractClient
    {
        $type = Arr::get($crm->config, 'auth.type');

        if ($type === CRM::AUTH_OAUTH2) {
            try {
                $client = $this->createApiForWorking($crm);
            } catch (\Throwable $exception) {
                throw $exception;
            }
            return $client;
        }

        if ($type === CRM::AUTH_WEBHOOK) {
            try {
                $client = $this->createWebhook($crm);
            } catch (\Throwable $exception) {
                throw $exception;
            }
            return $client;
        }

        throw new \InvalidArgumentException("Invalid auth type in CRM with ID={$crm->id}");
    }

    /**
     * @param \App\Models\CRM $crm
     * @return \App\Helpers\Bitrix\ApiClient
     * @throws \Throwable
     */
    private function createApiForWorking(CRM $crm): ApiClient
    {
        try {
            $client = $this->createApi($crm);

            $domain = Arr::get($crm->config, 'auth.domain');
            $client->setDomain($domain);

            $client = $this->authorizationService->populateClient($client, $crm, $this);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return $client;
    }

    /**
     * @param \App\Models\CRM $crm
     * @return \App\Helpers\Bitrix\ApiClient
     * @throws \Throwable
     */
    public function createApiForAuthorization(CRM $crm): ApiClient
    {
        try {
            $client = $this->createApi($crm);

            $client->setDomain('oauth.bitrix.info');
            $redirectUrl = $this->urlGenerator->route('authorization.show', ['crm' => $crm]);
            $client->setRedirectUri($redirectUrl);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return $client;
    }

    /**
     * @param \App\Models\CRM $crm
     * @return \App\Helpers\Bitrix\ApiClient
     * @throws \Throwable
     */
    private function createApi(CRM $crm): ApiClient
    {
        try {
            $client = new ApiClient();

            $scope = $this->config->get('bitrix24.scope');
            $client->setApplicationScope($scope);
            $clientId = Arr::get($crm->config, 'auth.client_id');
            $client->setApplicationId($clientId);
            $clientSecret = Arr::get($crm->config, 'auth.client_secret');
            $client->setApplicationSecret($clientSecret);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return $client;
    }

    /**
     * @param \App\Models\CRM $crm
     * @return \App\Helpers\Bitrix\WebhookClient
     * @throws \Throwable
     */
    private function createWebhook(CRM $crm): WebhookClient
    {
        try {
            $client = new WebhookClient();

            $scope = $this->config->get('bitrix24.scope');
            $client->setApplicationScope($scope);
            $domain = Arr::get($crm->config, 'auth.webhook');
            $client->setDomain($domain);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return $client;
    }
}