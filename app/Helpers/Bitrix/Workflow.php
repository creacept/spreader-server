<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 01.10.2018
 * Time: 14:34
 */

namespace App\Helpers\Bitrix;

use Bitrix24\Bitrix24Entity;
use Throwable;

/**
 * Class Workflow
 * @package App\Helpers\Bitrix
 */
class Workflow extends Bitrix24Entity
{
    /**
     * @param $templateId
     * @param array $documentId
     * @return array
     * @throws Throwable
     */
    public function start($templateId, array $documentId)
    {
        try {
            $result = $this->client->call(
                'bizproc.workflow.start',
                [
                    'TEMPLATE_ID' => $templateId,
                    'DOCUMENT_ID' => $documentId,
                    'PARAMETERS' => null,
                ]
            );
        } catch (Throwable $exception) {
            throw $exception;
        }

        return $result;
    }
}