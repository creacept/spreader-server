<?php

namespace App\Helpers\Bitrix;

/**
 * Class WebhookConfig
 * @package App\Helpers\Bitrix
 */
abstract class WebhookConfig
{
    /**
     * @var string
     */
    const FIELD_SITE_URL = 'UF_CRM_5B5F1231597FE';

    /**
     * @var string
     */
    const FIELD_PAGE_TITLE = 'UF_CRM_5B5F12315F56A';

    /**
     * @var string
     */
    const FIELD_SITE_ID = 'UF_CRM_1565791089';

    /**
     * @var string
     */
    const FIELD_SURVEY_RESULTS = 'UF_CRM_1572346492';

    /**
     * @var array
     */
    const FIELDS_SURVEY = [
        'UF_CRM_1574092126',
        'UF_CRM_1574092261',
        'UF_CRM_1574092311',
        'COMMENTS',
        'UF_CRM_1574092373',
        'UF_CRM_1574092398',
    ];

    /**
     * @var string
     */
    const WEBHOOK_REST_API_URL = 'https://new.crmcreacept.ru/rest/161/1rsip6m4vbvfv2gv/';

    /**
     * @var string
     */
    const ACCESS_TOKEN_CONTACT_ADD = 'bds3715rzou2kzy9z04cicjlsegd1gme';

    /**
     * @var array Pairs of Bitrix24's siteid and spreader's siteid
     */
    const BITRIX24_SITES_MAP = [
        1912 => 4,  // academypoker.ru
        1913 => 19, // academypoker.eu
        1914 => 12, // poker-shkola.ru
        1915 => 5,  // pokerup.ru
        1916 => 17, // pokermtt.eu
        1917 => 21, // horizonfx.org
        1918 => 1,  // academyfx.ru
        1919 => 28, // fxpriceaction.ru
        1920 => 6,  // pro-ts.ru
        1921 => 3,  // sniperfx.ru
        1922 => 27, // torgovye-roboty.ru
    ];
}