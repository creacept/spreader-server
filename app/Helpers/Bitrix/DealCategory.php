<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 16.11.2018
 * Time: 14:14
 */

namespace App\Helpers\Bitrix;

use Bitrix24\Bitrix24Entity;

/**
 * Class DealCategory
 * @package App\Helpers\Bitrix
 */
class DealCategory extends Bitrix24Entity
{
    /**
     * Get list of leadcategory items.
     * @link https://dev.1c-bitrix.ru/rest_help/crm/category/crm_dealcategory_list.php
     * @param array $order - order of task items
     * @param array $filter - filter array
     * @param array $select - array of columns to select
     * @param integer $start - entity number to start from (usually returned in 'next' field of previous "crm.dealcategory.list" API call)
     * @return array
     * @throws \Throwable
     */
    public function getList($order = [], $filter = [], $select = [], $start = 0)
    {
        $fullResult = $this->client->call(
            'crm.dealcategory.list',
            [
                'order' => $order,
                'filter'=> $filter,
                'select'=> $select,
                'start'	=> $start,
            ]
        );
        return $fullResult;
    }
}