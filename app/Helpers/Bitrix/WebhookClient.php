<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 11.10.2018
 * Time: 11:36
 */

namespace App\Helpers\Bitrix;

use Bitrix24\Exceptions;
use Illuminate\Support\Str;

/**
 * Class WebhookClient
 * @package App\Helpers\Bitrix
 */
class WebhookClient extends AbstractClient
{
    /**
     * @param string $domain
     * @return true
     * @throws Exceptions\Bitrix24Exception
     */
    public function setDomain($domain)
    {
        $result = parent::setDomain($domain);

        if (is_string($this->domain) && ! Str::endsWith($this->domain, '/')) {
            $this->domain .= '/';
        }

        return $result;
    }

    /**
     * @param string $methodName
     * @param array $additionalParameters
     * @return array
     * @throws Exceptions\Bitrix24ApiException
     * @throws Exceptions\Bitrix24BadGatewayException
     * @throws Exceptions\Bitrix24EmptyResponseException
     * @throws Exceptions\Bitrix24Exception
     * @throws Exceptions\Bitrix24IoException
     * @throws Exceptions\Bitrix24MethodNotFoundException
     * @throws Exceptions\Bitrix24PaymentRequiredException
     * @throws Exceptions\Bitrix24PortalDeletedException
     * @throws Exceptions\Bitrix24PortalRenamedException
     * @throws Exceptions\Bitrix24SecurityException
     * @throws Exceptions\Bitrix24TokenIsExpiredException
     * @throws Exceptions\Bitrix24TokenIsInvalidException
     * @throws Exceptions\Bitrix24WrongClientException
     */
    protected function _call($methodName, array $additionalParameters = array())
    {
        if (null === $this->getDomain()) {
            throw new Exceptions\Bitrix24Exception('domain not found, you must call setDomain method before');
        }
        if ('' === $methodName) {
            throw new Exceptions\Bitrix24Exception('method name not found, you must set method name');
        }

        $url = $this->domain . $methodName . '.json';
        // save method parameters for debug
        $this->methodParameters = $additionalParameters;
        // is secure api-call?
        $isSecureCall = false;
        if (array_key_exists('state', $additionalParameters)) {
            $isSecureCall = true;
        }
        // execute request
        $this->log->info('call bitrix24 method', array(
            'BITRIX24_DOMAIN' => $this->domain,
            'METHOD_NAME' => $methodName,
            'METHOD_PARAMETERS' => $additionalParameters
        ));
        $requestResult = $this->executeRequest($url, $additionalParameters);
        // check errors and throw exception if errors exists
        $this->handleBitrix24APILevelErrors($requestResult, $methodName, $additionalParameters);
        // handling security sign for secure api-call
        if ($isSecureCall) {
            if (array_key_exists('signature', $requestResult)) {
                // check signature structure
                if (strpos($requestResult['signature'], '.') === false) {
                    throw new Exceptions\Bitrix24SecurityException('security signature is corrupted');
                }
                if (null === $this->getMemberId()) {
                    throw new Exceptions\Bitrix24Exception('member-id not found, you must call setMemberId method before');
                }
                if (null === $this->getApplicationSecret()) {
                    throw new Exceptions\Bitrix24Exception('application secret not found, you must call setApplicationSecret method before');
                }
                // prepare
                $key = md5($this->getMemberId() . $this->getApplicationSecret());
                $delimiterPosition = strrpos($requestResult['signature'], '.');
                $dataToDecode = substr($requestResult['signature'], 0, $delimiterPosition);
                $signature = base64_decode(substr($requestResult['signature'], $delimiterPosition + 1));
                // compare signatures
                $hash = hash_hmac('sha256', $dataToDecode, $key, true);
                if ($hash !== $signature) {
                    throw new Exceptions\Bitrix24SecurityException('security signatures not same, bad request');
                }
                // decode
                $arClearData = json_decode(base64_decode($dataToDecode), true);
                // handling json_decode errors
                $jsonErrorCode = json_last_error();
                if (null === $arClearData && (JSON_ERROR_NONE !== $jsonErrorCode)) {
                    /**
                     * @todo add function json_last_error_msg()
                     */
                    $errorMsg = 'fatal error in function json_decode.' . PHP_EOL . 'Error code: ' . $jsonErrorCode . PHP_EOL;
                    throw new Exceptions\Bitrix24Exception($errorMsg);
                }
                // merge dirty and clear data
                unset($arClearData['state']);
                $requestResult ['result'] = array_merge($requestResult ['result'], $arClearData);
            } else {
                throw new Exceptions\Bitrix24SecurityException('security signature in api-response not found');
            }
        }
        return $requestResult;
    }
}