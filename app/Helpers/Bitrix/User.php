<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 28.08.2018
 * Time: 17:39
 */

namespace App\Helpers\Bitrix;

use Bitrix24\User\User as BaseUser;
use Throwable;

/**
 * Class User
 * @package App\Helpers\Bitrix
 */
class User extends BaseUser
{
    /**
     * @param $SORT
     * @param $ORDER
     * @param $FILTER
     * @param int $start
     * @return array
     * @throws Throwable
     */
    public function get($SORT, $ORDER, $FILTER, $start = 0)
    {
        try {
            $result = $this->client->call('user.get', [
                'SORT' => $SORT,
                'ORDER' => $ORDER,
                'FILTER' => $FILTER,
                'start' => $start,
            ]);
        } catch (Throwable $exception) {
            throw $exception;
        }

        return $result;
    }
}