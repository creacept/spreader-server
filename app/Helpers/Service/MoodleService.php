<?php

namespace App\Helpers\Service;

use Exception;
use GuzzleHttp\Client;
use RuntimeException;
use UnexpectedValueException;

/**
 * Class MoodleService
 * @package App\Helpers\Service
 */
class MoodleService
{
    /**
     * @var int
     */
    protected const MOODLE_STUDENT_GROUP_ID = 5;

    /**
     * @var int
     */
    protected const MOODLE_PREMIUM_STUDENT_GROUP_ID = 10;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $accessToken;

    /**
     * @var string
     */
    public const ONLY_REGISTRATION_LABEL = 'registration';

    /**
     * @var int
     */
    public const FREE_COURSES_CATEGORIES_IDS = [ 1 ];

    /**
     * MoodleService constructor.
     * @param string $baseUrl
     * @param string $accessToken
     */
    public function __construct(string $baseUrl, string $accessToken)
    {
        if (! filter_var($baseUrl, FILTER_VALIDATE_URL)) {
            throw new UnexpectedValueException('Base url is not valid', 500);
        }
        $baseUrl = rtrim($baseUrl, '/');
        $this->baseUrl = $baseUrl . '/webservice/rest/server.php';

        $this->accessToken = $accessToken;
        $this->httpClient = new Client();
    }

    /**
     * @param int[]|null $categoriesIds
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCourses(array $categoriesIds = null): array
    {
        $courses = $this->doRequest('core_course_get_courses', 'GET');
        if (is_array($categoriesIds) && count($categoriesIds) !== 0) {
            $courses = array_filter($courses, function ($course) use ($categoriesIds) {
                return in_array($course['categoryid'], $categoriesIds);
            });
        }

        $coursesWithGroups = array_map(function ($course) {
            $course['groups'] = $this->getCourseGroups($course['id']);
            return $course;
        }, $courses);

        // Default course
        unset($coursesWithGroups[0]);

        return $coursesWithGroups;
    }

    /**
     * @param int $courseId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCourseGroups(int $courseId): array
    {
        $params = ['courseid' => $courseId];
        $groups = $this->doRequest('core_group_get_course_groups', 'GET', $params);

        return $groups;
    }

    /**
     * @param string $email
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUser(string $email): ?array
    {
        $params = [
            'criteria' => [
                [
                    'key' => 'email',
                    'value' => $email,
                ],
            ],
        ];

        $findedUsers = $this->doRequest('core_user_get_users', 'GET', $params);
        if (! is_array($findedUsers)) {
            return null;
        }

        $moodleUser = array_shift($findedUsers['users']);
        if (! is_array($moodleUser)) {
            return null;
        }

        return $moodleUser;
    }

    /**
     * @param array $userData
     * @return int|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createUser(array $userData): ?int
    {
        $params = [
            'users' => [
                [
                    'firstname' => array_key_exists('name', $userData) ? $userData['name'] : $userData['username'],
                    'lastname' => array_key_exists('lastname', $userData) ? $userData['lastname'] : 'User',
                    'username' => mb_strtolower($userData['username']),
                    'email' => $userData['email'],
                ]
            ]
        ];

        if (! array_key_exists('password', $userData) || ! is_string($userData['password'])) {
            $params['users'][0]['createpassword'] = 1;
        }

        $moodleUsersData = $this->doRequest('core_user_create_users', 'POST', $params);

        $tmpUserData = array_values($moodleUsersData);
        $tmpUserData = array_shift($tmpUserData);
        if (! is_array($moodleUsersData) || is_string($tmpUserData)) {
            return null;
        }
        if (! array_key_exists('id', $tmpUserData)) {
            return null;
        }

        $moodleUserId = $moodleUsersData[0]['id'];
        return $moodleUserId;
    }

    /**
     * @param string $username
     * @param int $courseId
     * @param int|null $groupId
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function assignUserToCourse(string $username, int $courseId, int $groupId = null): void
    {
        $params = [
            'username' => $username,
            'roleid' => self::MOODLE_PREMIUM_STUDENT_GROUP_ID,
            'courses' => [
                [
                    'id' => $courseId,
                ],
            ],
        ];

        if (is_int($groupId)) {
            $apiMethod = 'joomdle_multiple_enrol_to_course_and_group';
            $params['courses'][0]['group_id'] = $groupId;
        } else {
            $apiMethod = 'joomdle_multiple_enrol';
        }

        $assigned = $this->doRequest($apiMethod, 'POST', $params);
        return;
    }

    /**
     * Make a request to Moodle API.
     *
     * @param string $action
     * @param string $method
     * @param array $data
     * @param bool $returnRaw
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    private function doRequest($action, $method, array $data = null, $returnRaw = false)
    {
        $method = strtoupper($method);
        if (! in_array($method, ['GET', 'POST'])) {
            $method = 'GET';
        }

        $formData = [];
        $queryParams = [
            'wstoken' => $this->accessToken,
            'wsfunction' => $action,
            'moodlewsrestformat' => 'json',
        ];

        if (is_array($data) && count($data) > 0) {
            if ($method === 'GET') {
                $queryParams = array_merge($queryParams, $data);
            } elseif ($method === 'POST') {
                $formData = $data;
            }
        }

        $response = $this->httpClient->request($method, $this->baseUrl, ['query' => $queryParams, 'form_params' => $formData]);

        $code = $response->getStatusCode();
        if ($code !== 200) {
            $reason = $response->getReasonPhrase();
            throw new RuntimeException("API response error, reason: {$reason}", $code);
        }

        $content = $response->getBody()->getContents();

        if (! $returnRaw) {
            try {
                $content = \GuzzleHttp\json_decode($content, true);
            } catch (Exception $e) {
                throw new Exception('Bad JSON response from API', 500, $e);
            }
        }

        return $content;
    }
}