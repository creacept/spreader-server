<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 26.07.2018
 * Time: 16:56
 */

namespace App\Helpers\Service;

use App\Models\Application;
use App\Models\Group;
use App\Models\Manager;
use App\Models\Project;
use App\Models\Site;
use Throwable;

/**
 * Class ManagerResolverService
 * @package App\Helpers\Service
 */
class ManagerResolverService
{
    /**
     * @param Application $application
     * @return Manager|null
     * @throws Throwable
     */
    public function resolve(Application $application) : ?Manager
    {
        $site = $application->site;

        try {
            $manager = $this->resolveByRules($site, $application);
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ($manager instanceof Manager) {
            return $manager;
        }

        try {
            $manager = $this->resolveByGroups($site);
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ($manager instanceof Manager) {
            return $manager;
        }

        try {
            $manager = $this->resolveByManagers($site);
        } catch (Throwable $exception) {
            throw $exception;
        }

        return $manager;
    }

    /**
     * @param Site $site
     * @param Application $application
     * @return Manager|null
     * @throws Throwable
     */
    private function resolveByRules(Site $site, Application $application) : ?Manager
    {
        $manager = null;

        foreach ($site->rules as $rule) {
            $result = true;

            if (is_string($rule->form_name)) {
                $result = ($result && $application->form_name === $rule->form_name);
            }

            if (is_string($rule->page_url_domain)) {
                if (is_string($application->page_url)) {
                    $domain = parse_url($application->page_url, PHP_URL_HOST);
                    $result = ($result && $domain === $rule->page_url_domain);
                } else {
                    $result = false;
                }
            }

            if (is_string($rule->page_url_path)) {
                if (is_string($application->page_url)) {
                    $path = parse_url($application->page_url, PHP_URL_PATH);
                    if ($path === null) {
                        $path = '/';
                    }
                    $result = ($result && $path === $rule->page_url_path);
                } else {
                    $result = false;
                }
            }

            if (is_string($rule->page_title)) {
                $result = ($result && is_string($application->page_title) && $application->page_title === $rule->page_title);
            }

            if (is_string($rule->utm_source)) {
                $result = ($result && is_string($application->utm_source) && mb_strpos($application->utm_source, $rule->utm_source) === 0);
            }

            if (is_string($rule->utm_content)) {
                $result = ($result && is_string($application->utm_content) && mb_strpos($application->utm_content, $rule->utm_content) === 0);
            }

            if (is_string($rule->utm_campaign)) {
                $result = ($result && is_string($application->utm_campaign) && mb_strpos($application->utm_campaign, $rule->utm_campaign) === 0);
            }

            if (is_string($rule->utm_medium)) {
                $result = ($result && is_string($application->utm_medium) && mb_strpos($application->utm_medium, $rule->utm_medium) === 0);
            }

            if (is_string($rule->sub_id)) {
                $result = ($result && is_string($application->sub_id) && mb_strpos($application->sub_id, $rule->sub_id) === 0);
            }

            if (! $result) {
                continue;
            }

            $group = $rule->group;
            try {
                $manager = $group->managers->first();
            } catch (Throwable $exception) {
                throw $exception;
            }

            if (! $manager instanceof Manager) {
                continue;
            }

            try {
                Group::whereId($group->id)->increment('application_number');
                Manager::whereId($manager->id)->increment('application_number');
            } catch (Throwable $exception) {
                throw $exception;
            }

            return $manager;
        }

        return $manager;
    }

    /**
     * @param Site $site
     * @return Manager|null
     * @throws Throwable
     */
    private function resolveByGroups(Site $site) : ?Manager
    {
        try {
            $groups = $site->groups;
        } catch (Throwable $exception) {
            throw $exception;
        }
        $manager = null;


        $group = $groups->filter(function (Group $group) {
            return $group->publish;
        })
            ->sort(function (Group $group1, Group $group2) {
            $proportion1 = $group1->application_number / $group1->ratio;
            $proportion2 = $group2->application_number / $group2->ratio;
            return ($proportion1 <=> $proportion2);
        })->first();
        if (! $group instanceof Group) {
            return $manager;
        }

        try {
            $manager = $group->managers->first();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (! $manager instanceof Manager) {
            return $manager;
        }

        try {
            Group::whereId($group->id)->increment('application_number');
            Manager::whereId($manager->id)->increment('application_number');
        } catch (Throwable $exception) {
            throw $exception;
        }

        return $manager;
    }

    /**
     * @param Site $site
     * @return Manager|null
     * @throws Throwable
     */
    private function resolveByManagers(Site $site) : ?Manager
    {
        $manager = null;
        if (! $site->project instanceof Project) {
            return $manager;
        }

        $grammar = \DB::getQueryGrammar();
        try {
            $manager = Manager::whereProjectId($site->project_id)
                ->wherePublish(true)
                ->orderByRaw(sprintf(
                    '%s / %s',
                    $grammar->wrap('application_number'),
                    $grammar->wrap('ratio')
                ))
                ->first();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (! $manager instanceof Manager) {
            return $manager;
        }

        try {
            Manager::whereId($manager->id)->increment('application_number');
        } catch (Throwable $exception) {
            throw $exception;
        }

        return $manager;
    }
}