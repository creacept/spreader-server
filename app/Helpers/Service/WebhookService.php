<?php

namespace App\Helpers\Service;

use App\Events\ApplicationStored;
use App\Helpers\AddLead\Field\Email;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\Bitrix\WebhookConfig;
use App\Models\Application;
use App\Models\CRM;
use Bitrix24\CRM\Contact;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Throwable;

/**
 * Class WebhookService
 * @package App\Helpers\Service
 */
class WebhookService
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $httpClient;

    /**
     * @var
     */
    private $contactApi;

    /**
     * WebhookService constructor.
     */
    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => WebhookConfig::WEBHOOK_REST_API_URL,
        ]);
    }

    /**
     * Get a contact's data from Bitrix24.
     *
     * @param int $id
     * @return array
     */
    public function getContact(int $id) : array
    {
        try {
            $response = $this->httpClient->request('GET', 'crm.contact.get', [
                'query' => [
                    'id' => $id,
                ],
            ]);
        } catch (GuzzleException $exception) {
            report($exception);
            return [];
        }

        $responseStatusCode = $response->getStatusCode();
        if ($responseStatusCode !== 200) {
            return [];
        }

        $responseBodyJSON = $response->getBody()->getContents();
        $responseBody = \GuzzleHttp\json_decode($responseBodyJSON, true);
        $contact = Arr::get($responseBody, 'result', []);

        return $contact;
    }


    /**
     * Find a contact in Bitrix24 by phone number.
     *
     * @param string $phone
     * @param int $crmId
     * @return array
     * @throws \libphonenumber\NumberParseException
     */
    public function findContactByPhone(string $phone, int $crmId)
    {
        if (! $this->contactApi instanceof Contact) {
            $this->setContactApi($crmId);
        }

        try {
            $phoneNumberUtil = PhoneNumberUtil::getInstance();
            $contactPhone = $phoneNumberUtil->parse($phone, 'AUTO');
        } catch (NumberParseException $e) {
            throw $e;
        }

        $isValidPhone = $phoneNumberUtil->isValidNumber($contactPhone);
        if (! $isValidPhone) {
            throw new InvalidArgumentException('Invalid phone number');
        }

        $contactPhone = $phoneNumberUtil->format($contactPhone, PhoneNumberFormat::E164);
        $matchedContacts = $this->contactApi->getList(
            [],
            ['PHONE' =>  $contactPhone],
            ['ID']
        );

        return $matchedContacts;
    }

    /**
     * Update a contact's survey information in Bitrix24.
     *
     * @param int $contactId
     * @param array $requestData
     * @param int $crmId
     * @return bool
     */
    public function updateContactData(int $contactId, array $requestData, int $crmId): bool
    {
        if (! $this->contactApi instanceof Contact) {
            $this->setContactApi($crmId);
        }

        unset($requestData['phone']);
        unset($requestData['crm_id']);

        $surveyResults = '';
        foreach ($requestData as $label => $value) {
            $surveyResults .= "{$label}: {$value}" . PHP_EOL;
        }

        $updateFields = [WebhookConfig::FIELD_SURVEY_RESULTS => $surveyResults];
        $result = $this->contactApi->update($contactId, $updateFields);

        return Arr::get($result, 'result', false);
    }

    /**
     * Delete a contact from Bitrix24.
     *
     * @param int $id
     * @return bool
     */
    public function deleteContact(int $id) : bool
    {
        try {
            $response = $this->httpClient->request('GET', 'crm.contact.delete', [
                'query' => [
                    'id' => $id,
                ],
            ]);
        } catch (GuzzleException $exception) {
            report($exception);
            return false;
        }

        $responseStatusCode = $response->getStatusCode();
        if ($responseStatusCode !== 200) {
            return false;
        }

        $responseBodyJSON = $response->getBody()->getContents();
        $responseBody = \GuzzleHttp\json_decode($responseBodyJSON, true);
        $isDeleted = Arr::get($responseBody, 'result', false);

        return $isDeleted;
    }

    /**
     * Create a new application from contact's data.
     *
     * @param array $contact
     * @return \App\Models\Application|null
     * @throws \libphonenumber\NumberParseException
     */
    public function createApplicationFromContact(array $contact): ?Application
    {
        $sourceId = Arr::get($contact, 'ID');

        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        $parsedPhone = Arr::get($contact, 'PHONE.0.VALUE');
        $parsedPhone = $phoneNumberUtil->parse($parsedPhone, 'AUTO');

        $phone = $phoneNumberUtil->isValidNumber($parsedPhone)
            ? $phoneNumberUtil->format($parsedPhone, PhoneNumberFormat::E164)
            : null;

        $email = Arr::has($contact, 'EMAIL.0.VALUE')
            ? Email::createIdentity(Arr::get($contact, 'EMAIL.0.VALUE'))
            : null;

        $applicationName = Arr::get($contact, WebhookConfig::FIELD_PAGE_TITLE);
        if ($applicationName === null) {
            return null;
        }

        $siteId = Arr::get($contact, WebhookConfig::FIELD_SITE_ID, '0');
        $bitrix24Sites = array_keys(WebhookConfig::BITRIX24_SITES_MAP);
        if (! in_array($siteId, $bitrix24Sites)) {
            return null;
        }

        $siteId = WebhookConfig::BITRIX24_SITES_MAP[$siteId];
        $contactName = Arr::get($contact, 'NAME');

        $submittedAt = Arr::get($contact, 'DATE_CREATE');
        $submittedAt = Carbon::parse($submittedAt);
        $pageUrl = Arr::get($contact, WebhookConfig::FIELD_SITE_URL);

        $utmSource = Arr::get($contact, 'UTM_SOURCE');
        if ($utmSource === null) {
            $utmSource = Arr::get($contact, 'UF_CRM_1599485051');
        }
        $utmMedium = Arr::get($contact, 'UTM_MEDIUM');
        if ($utmMedium === null) {
            $utmMedium = Arr::get($contact, 'UF_CRM_1599485080');
        }
        $utmCampaign = Arr::get($contact, 'UTM_CAMPAIGN');
        if ($utmCampaign === null) {
            $utmCampaign = Arr::get($contact, 'UF_CRM_1599485154');
        }
        $utmContent = Arr::get($contact, 'UTM_CONTENT');
        if ($utmContent === null) {
            $utmContent = Arr::get($contact, 'UF_CRM_1599485470');
        }

        $senderExtraFields = [];
        foreach (WebhookConfig::FIELDS_SURVEY as $surveyFieldId) {
            $contactSurveyField = Arr::get($contact, $surveyFieldId);
            if (is_string($contactSurveyField) && trim($contactSurveyField) !== '') {
                $senderExtraFields[$surveyFieldId] = $contactSurveyField;
            }
        }

        $application = new Application();
        $application->sender_phone = $phone;
        $application->sender_email = $email;
        $application->submitted_at = $submittedAt;
        $application->source_id = $sourceId;
        $application->site_id = $siteId;
        $application->status = Application::CONFIRMED_STATUS;
        $application->handled_at = Carbon::now();
        $application->form_name = $applicationName;
        $application->sender_name = $contactName;
        $application->page_title = $applicationName;
        $application->page_url = $pageUrl;
        $application->utm_source = $utmSource;
        $application->utm_medium = $utmMedium;
        $application->utm_campaign = $utmCampaign;
        $application->utm_content = $utmContent;
        $application->sender_extra_fields = $senderExtraFields;

        try {
            $application->saveOrFail();
            event(new ApplicationStored($application));
        } catch (Throwable $exception) {
            report($exception);
            return null;
        }

        return $application;
    }

    /**
     * @param int $crmId
     * @return bool
     */
    protected function setContactApi(int $crmId): bool
    {
        try {
            $crm = CRM::findOrFail($crmId);
            /** @var \App\Helpers\Bitrix\ClientFactory $clientFactory */
            $clientFactory = App::make(ClientFactory::class);
            $client = $clientFactory->create($crm);
        } catch (Throwable $exception) {
            $errorMessage = $exception->getMessage();
            Log::error("Survey: can't create API client: {$errorMessage}");
            return false;
        }

        $this->contactApi = new Contact($client);
        return true;
    }
}