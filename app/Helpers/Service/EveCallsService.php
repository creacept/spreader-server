<?php

namespace App\Helpers\Service;

use GuzzleHttp\Client;
use UnexpectedValueException;

/**
 * Class EveCallsService
 * @package App\Helpers\Service
 */
class EveCallsService
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected const BASE_URL = 'https://api.evecalls.com';

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * EveCallsService constructor.
     * @param string $apiKey
     */
    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
        $this->httpClient = new Client([
            'base_url' => self::BASE_URL,
        ]);
    }
}