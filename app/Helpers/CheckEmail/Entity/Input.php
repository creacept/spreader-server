<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 21.02.2019
 * Time: 14:54
 */

namespace App\Helpers\CheckEmail\Entity;

use App\Models\Site;
use Illuminate\Support\Carbon;

/**
 * Class Input
 * @package App\Helpers\CheckEmail
 */
class Input
{
    /**
     * @var \App\Models\Site
     */
    private $site;
    /**
     * @var string
     */
    private $email;
    /**
     * @var \Illuminate\Support\Carbon
     */
    private $date;
    /**
     * @var string
     */
    private $formName;
    /**
     * @var string|null
     */
    private $pageUrl;
    /**
     * @var string|null
     */
    private $pageTitle;
    /**
     * @var string|null
     */
    private $utmSource;
    /**
     * @var string|null
     */
    private $utmContent;
    /**
     * @var string|null
     */
    private $utmCampaign;
    /**
     * @var string|null
     */
    private $utmMedium;
    /**
     * @var string|null
     */
    private $subId;

    /**
     * Input constructor.
     * @param \App\Models\Site $site
     * @param string $email
     * @param \Illuminate\Support\Carbon $date
     * @param string $formName
     * @param string|null $pageUrl
     * @param string|null $pageTitle
     * @param string|null $utmSource
     * @param string|null $utmContent
     * @param string|null $utmCampaign
     * @param string|null $utmMedium
     * @param string|null $subId
     */
    public function __construct(
        Site $site,
        string $email,
        Carbon $date,
        string $formName = null,
        ?string $pageUrl = null,
        ?string $pageTitle = null,
        ?string $utmSource = null,
        ?string $utmContent = null,
        ?string $utmCampaign = null,
        ?string $utmMedium = null,
        ?string $subId = null
    ) {
        $this->site = $site;
        $this->email = $email;
        $this->date = $date;
        $this->formName = $formName;
        $this->pageUrl = $pageUrl;
        $this->pageTitle = $pageTitle;
        $this->utmSource = $utmSource;
        $this->utmContent = $utmContent;
        $this->utmCampaign = $utmCampaign;
        $this->utmMedium = $utmMedium;
        $this->subId = $subId;
    }

    /**
     * @return \App\Models\Site
     */
    public function getSite(): Site
    {
        return $this->site;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getDate(): Carbon
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getFormName(): string
    {
        return $this->formName;
    }

    /**
     * @return string|null
     */
    public function getPageUrl(): ?string
    {
        return $this->pageUrl;
    }

    /**
     * @return string|null
     */
    public function getPageTitle(): ?string
    {
        return $this->pageTitle;
    }

    /**
     * @return string|null
     */
    public function getUtmSource(): ?string
    {
        return $this->utmSource;
    }

    /**
     * @return string|null
     */
    public function getUtmContent(): ?string
    {
        return $this->utmContent;
    }

    /**
     * @return string|null
     */
    public function getUtmCampaign(): ?string
    {
        return $this->utmCampaign;
    }

    /**
     * @return string|null
     */
    public function getUtmMedium(): ?string
    {
        return $this->utmMedium;
    }

    /**
     * @return string|null
     */
    public function getSubId(): ?string
    {
        return $this->subId;
    }
}