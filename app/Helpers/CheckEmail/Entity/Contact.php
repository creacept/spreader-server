<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 22.02.2019
 * Time: 10:17
 */

namespace App\Helpers\CheckEmail\Entity;

/**
 * Class Contact
 * @package App\Helpers\CheckEmail\Entity
 */
class Contact
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $managerId;

    /**
     * Contact constructor.
     * @param int $id
     * @param int $managerId
     */
    public function __construct(int $id, int $managerId)
    {
        $this->id = $id;
        $this->managerId = $managerId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getManagerId(): int
    {
        return $this->managerId;
    }
}