<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 22.02.2019
 * Time: 10:48
 */

namespace App\Helpers\CheckEmail\Entity;

use Illuminate\Support\Carbon;

/**
 * Class Lead
 * @package App\Helpers\CheckEmail\Entity
 */
class Lead
{
    /**
     * @var int
     */
    private $contactId;
    /**
     * @var \Illuminate\Support\Carbon
     */
    private $createdAt;

    /**
     * Lead constructor.
     * @param int $contactId
     * @param \Illuminate\Support\Carbon $createdAt
     */
    public function __construct(int $contactId, Carbon $createdAt)
    {
        $this->contactId = $contactId;
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getContactId(): int
    {
        return $this->contactId;
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }
}