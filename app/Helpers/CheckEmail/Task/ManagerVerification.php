<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 22.02.2019
 * Time: 11:38
 */

namespace App\Helpers\CheckEmail\Task;

use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\CheckEmail\Entity\Contact;
use App\Helpers\CheckEmail\Entity\Input;
use App\Models\Manager;

/**
 * Class ManagerVerification
 * @package App\Helpers\CheckEmail\Task
 */
class ManagerVerification implements TaskInterface
{
    /**
     * @var \App\Helpers\CheckEmail\Entity\Contact
     */
    private $contact;
    /**
     * @var \App\Helpers\CheckEmail\Entity\Input
     */
    private $input;

    /**
     * @return \App\Helpers\CheckEmail\Task\AddingTask|\App\Helpers\CheckEmail\Task\ManagerReplacement
     * @throws \Throwable
     */
    public function execute()
    {
        if (! $this->contact instanceof Contact) {
            throw new \RuntimeException('Use method "setContact" for set contact');
        }
        if (! $this->input instanceof Input) {
            throw new \RuntimeException('Use method "setInput" for set input');
        }

        $projectId = $this->input->getSite()->project_id;
        $bitrix24UserId = $this->contact->getManagerId();
        try {
            $exists = Manager::whereProjectId($projectId)
                ->whereBitrix24UserId($bitrix24UserId)
                ->wherePublish(true)
                ->exists();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if ($exists) {
            try {
                $task = resolve(AddingTask::class);
            } catch (\Throwable $exception) {
                throw $exception;
            }
            $task->setContact($this->contact);
            $task->setInput($this->input);
            return $task;
        }

        try {
            $task = resolve(ManagerReplacement::class);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $task->setContact($this->contact);
        $task->setInput($this->input);

        return $task;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Contact $contact
     */
    public function setContact(Contact $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Input $input
     */
    public function setInput(Input $input): void
    {
        $this->input = $input;
    }
}