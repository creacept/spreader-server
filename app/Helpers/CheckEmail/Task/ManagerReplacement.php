<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 22.02.2019
 * Time: 12:10
 */

namespace App\Helpers\CheckEmail\Task;

use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\CheckEmail\Entity\Contact;
use App\Helpers\CheckEmail\Entity\Input;
use App\Helpers\Service\ManagerResolverService;
use App\Models\Application;
use App\Models\CRM;
use App\Models\Manager;
use Bitrix24\CRM\Contact as ApiContact;

/**
 * Class ManagerReplacement
 * @package App\Helpers\CheckEmail\Task
 */
class ManagerReplacement implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var \App\Helpers\Service\ManagerResolverService
     */
    private $managerResolverService;
    /**
     * @var \App\Helpers\CheckEmail\Entity\Contact
     */
    private $contact;
    /**
     * @var \App\Helpers\CheckEmail\Entity\Input
     */
    private $input;

    /**
     * ManagerReplacement constructor.
     * @param \App\Helpers\Service\ManagerResolverService $managerResolverService
     */
    public function __construct(ManagerResolverService $managerResolverService)
    {
        $this->managerResolverService = $managerResolverService;
    }

    /**
     * @return \App\Helpers\CheckEmail\Task\AddingTask
     * @throws \Throwable
     */
    public function execute()
    {
        if (! $this->contact instanceof Contact) {
            throw new \RuntimeException('Use method "setContact" for set contact');
        }
        if (! $this->input instanceof Input) {
            throw new \RuntimeException('Use method "setInput" for set input');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        $application = new Application();
        $application->site_id = $this->input->getSite()->id;
        $application->form_name = $this->input->getFormName();
        $application->page_url = $this->input->getPageUrl();
        $application->page_title = $this->input->getPageTitle();
        $application->utm_source = $this->input->getUtmSource();
        $application->utm_content = $this->input->getUtmContent();
        $application->utm_campaign = $this->input->getUtmCampaign();
        $application->utm_medium = $this->input->getUtmMedium();
        $application->sub_id = $this->input->getSubId();

        try {
            $manager = $this->managerResolverService->resolve($application);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if ($manager instanceof Manager) {
            try {
                $client = $this->clientFactory->create($this->crm);
                $api = new ApiContact($client);
                $api->update(
                    $this->contact->getId(),
                    ['ASSIGNED_BY_ID' => $manager->bitrix24_user_id],
                    ['REGISTER_SONET_EVENT' => 'Y']
                );
            } catch (\Throwable $exception) {
                throw $exception;
            }
            $this->contact = new Contact(
                $this->contact->getId(),
                $manager->bitrix24_user_id
            );
        }

        try {
            $task = resolve(AddingTask::class);
        } catch (\Exception $exception) {
            throw $exception;
        }
        $task->setContact($this->contact);
        $task->setInput($this->input);

        return $task;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Contact $contact
     */
    public function setContact(Contact $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Input $input
     */
    public function setInput(Input $input): void
    {
        $this->input = $input;
    }
}