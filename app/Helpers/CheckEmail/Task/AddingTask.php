<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 22.02.2019
 * Time: 14:23
 */

namespace App\Helpers\CheckEmail\Task;

use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\CheckEmail\Entity\Contact;
use App\Helpers\CheckEmail\Entity\Input;
use App\Models\CRM;
use Bitrix24\Bitrix24;
use Bitrix24\Task\Item;
use Illuminate\Support\Carbon;

/**
 * Class AddingTask
 * @package App\Helpers\CheckEmail\Task
 */
class AddingTask implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var \App\Helpers\CheckEmail\Entity\Contact
     */
    private $contact;
    /**
     * @var \App\Helpers\CheckEmail\Entity\Input
     */
    private $input;

    /**
     * @return bool
     * @throws \Throwable
     */
    public function execute()
    {
        if (! $this->contact instanceof Contact) {
            throw new \RuntimeException('Use method "setContact" for set contact');
        }
        if (! $this->input instanceof Input) {
            throw new \RuntimeException('Use method "setInput" for set input');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        try {
            $client = $this->clientFactory->create($this->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $api = new Item($client);

        $taskData = [
            'TITLE' => 'Созвониться с клиентом',
            'RESPONSIBLE_ID' => $this->contact->getManagerId(),
            'DEADLINE' => Carbon::now()->addDay()->toIso8601String(),
            'UF_CRM_TASK' => ['C_' . $this->contact->getId()],
        ];
        $url = $this->input->getPageUrl();
        if (is_string($url)) {
            $title = $this->input->getPageTitle();
            if (! is_string($title)) {
                $title = $url;
            }
            $taskData['DESCRIPTION'] = sprintf(
                'Оставил заявку на "[URL=%s]%s[/URL]"',
                $url,
                $title
            );
        }
        try {
            $api->add($taskData);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return false;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Contact $contact
     */
    public function setContact(Contact $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Input $input
     */
    public function setInput(Input $input): void
    {
        $this->input = $input;
    }
}