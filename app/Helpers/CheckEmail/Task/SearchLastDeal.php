<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 21.02.2019
 * Time: 17:51
 */

namespace App\Helpers\CheckEmail\Task;

use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\CheckEmail\Entity\Contact;
use App\Helpers\CheckEmail\Entity\Input;
use App\Helpers\CheckEmail\Entity\Lead;
use App\Models\CRM;
use Bitrix24\CRM\Deal\Deal;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class SearchLastDeal
 * @package App\Helpers\CheckEmail\Task
 */
class SearchLastDeal implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var \Illuminate\Support\Collection
     */
    private $contacts;
    /**
     * @var \App\Helpers\CheckEmail\Entity\Lead|null
     */
    private $lead;
    /**
     * @var \App\Helpers\CheckEmail\Entity\Input
     */
    private $input;

    /**
     * @return \App\Helpers\CheckEmail\Task\ManagerVerification|bool
     * @throws \Throwable
     */
    public function execute()
    {
        if (! $this->contacts instanceof Collection || $this->contacts->isEmpty()) {
            throw new \RuntimeException('Use method "setContacts" for set contacts');
        }
        if (! $this->input instanceof Input) {
            throw new \RuntimeException('Use method "setInput" for set input');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        try {
            $client = $this->clientFactory->create($this->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $api = new Deal($client);

        $contactIds = $this->contacts->map(function (Contact $contact) {
            return $contact->getId();
        })->toArray();
        try {
            $result = $api->getList(['DATE_CREATE' => 'DESC'], ['CONTACT_ID' => $contactIds], ['DATE_CREATE', 'CONTACT_ID']);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $deals = Arr::get($result, 'result', []);
        if (count($deals) > 0) {
            $deal = Arr::first($deals);
            $createdAt = Carbon::parse($deal['DATE_CREATE']);
            if ($createdAt > $this->input->getDate()) {
                return true;
            }
            $contactId = (int) $deal['CONTACT_ID'];

            if ($this->lead instanceof Lead && $this->lead->getCreatedAt() > $createdAt) {
                $contactId = $this->lead->getContactId();
            }
        } elseif ($this->lead instanceof Lead) {
            $contactId = $this->lead->getContactId();
        } else {
            return true;
        }

        $contact = $this->contacts->first(function (Contact $contact) use ($contactId) {
            return ($contact->getId() === $contactId);
        });

        try {
            $task = resolve(ManagerVerification::class);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $task->setContact($contact);
        $task->setInput($this->input);

        return $task;
    }

    /**
     * @param \Illuminate\Support\Collection $contacts
     */
    public function setContacts(Collection $contacts): void
    {
        $this->contacts = $contacts;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Lead $lead
     */
    public function setLead(Lead $lead): void
    {
        $this->lead = $lead;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Input $input
     */
    public function setInput(Input $input): void
    {
        $this->input = $input;
    }
}