<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 21.02.2019
 * Time: 14:34
 */

namespace App\Helpers\CheckEmail\Task;

use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\CheckEmail\Entity\Contact;
use App\Helpers\CheckEmail\Entity\Input;
use App\Models\CRM;
use App\Models\Project;
use Bitrix24\CRM\Contact as ApiContact;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * Class SearchContacts
 * @package App\Helpers\CheckEmail\Task
 */
class SearchContacts implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var \App\Helpers\CheckEmail\Entity\Input
     */
    private $input;

    /**
     * @return \App\Helpers\CheckEmail\Task\SearchLastLead|bool
     * @throws \Throwable
     */
    public function execute()
    {
        if (! $this->input instanceof Input) {
            throw new \RuntimeException('Use method "setInput" for set site');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        $site = $this->input->getSite();
        $project = $site->project;
        if (! $project instanceof Project) {
            throw new \RuntimeException("Project with ID={$project->id} not found for site with ID={$site->id}");
        }

        try {
            $client = $this->clientFactory->create($this->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $api = new ApiContact($client);
        $start = 0;
        try {
            do {
                $result = $api->getList(
                    [],
                    [
                        'EMAIL' => $this->input->getEmail(),
                        Arr::get($this->crm->config, 'contact.identity') => $project->contact_identity,
                    ],
                    [
                        'ID',
                        'ASSIGNED_BY_ID',
                    ],
                    $start
                );
                $start = Arr::get($result, 'result.next', false);
            } while ($start !== false);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $contacts = Arr::get($result, 'result', []);
        $contacts = (new Collection($contacts))->map(function (array $contact) {
            $id = (int) $contact['ID'];
            $managerId = (int) $contact['ASSIGNED_BY_ID'];
            return new Contact($id, $managerId);
        });
        if ($contacts->isEmpty()) {
            return true;
        }

        try {
            $task = resolve(SearchLastLead::class);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $task->setContacts($contacts);
        $task->setInput($this->input);

        return $task;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Input $input
     */
    public function setInput(Input $input): void
    {
        $this->input = $input;
    }
}