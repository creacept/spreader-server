<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 21.02.2019
 * Time: 16:01
 */

namespace App\Helpers\CheckEmail\Task;

use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\ClientAwareTrait;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\CheckEmail\Entity\Contact;
use App\Helpers\CheckEmail\Entity\Input;
use App\Helpers\CheckEmail\Entity\Lead;
use App\Models\CRM;
use Bitrix24\CRM\Lead as ApiLead;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class SearchLastLead
 * @package App\Helpers\CheckEmail\Task
 */
class SearchLastLead implements TaskInterface, ClientAwareInterface
{
    use ClientAwareTrait;

    /**
     * @var \Illuminate\Support\Collection
     */
    private $contacts;
    /**
     * @var \App\Helpers\CheckEmail\Entity\Input
     */
    private $input;

    /**
     * @return \App\Helpers\CheckEmail\Task\SearchLastDeal|bool
     * @throws \Throwable
     */
    public function execute()
    {
        if (! $this->contacts instanceof Collection || $this->contacts->isEmpty()) {
            throw new \RuntimeException('Use method "setContacts" for set contacts');
        }
        if (! $this->input instanceof Input) {
            throw new \RuntimeException('Use method "setInput" for set input');
        }
        if (! $this->clientFactory instanceof ClientFactory) {
            throw new \RuntimeException('Use method "setClientFactory" for set client factory');
        }
        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        try {
            $client = $this->clientFactory->create($this->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $api = new ApiLead($client);

        $contactIds = $this->contacts->map(function (Contact $contact) {
            return $contact->getId();
        })->toArray();
        try {
            $result = $api->getList(['DATE_CREATE' => 'DESC'], ['CONTACT_ID' => $contactIds], ['DATE_CREATE', 'CONTACT_ID']);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        try {
            $task = resolve(SearchLastDeal::class);
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $task->setContacts($this->contacts);
        $task->setInput($this->input);

        $leads = Arr::get($result, 'result', []);
        if (count($leads) > 0) {
            $lead = Arr::first($leads);
            $createdAt = Carbon::parse($lead['DATE_CREATE']);
            if ($createdAt > $this->input->getDate()) {
                return true;
            }

            $contactId = (int) $lead['CONTACT_ID'];
            $lead = new Lead($contactId, $createdAt);
            $task->setLead($lead);
        }

        return $task;
    }

    /**
     * @param \Illuminate\Support\Collection $contacts
     */
    public function setContacts(Collection $contacts): void
    {
        $this->contacts = $contacts;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Input $input
     */
    public function setInput(Input $input): void
    {
        $this->input = $input;
    }
}