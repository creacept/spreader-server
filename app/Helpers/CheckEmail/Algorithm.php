<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 21.02.2019
 * Time: 14:18
 */

namespace App\Helpers\CheckEmail;

use App\Helpers\Algorithm\AbstractAlgorithm;
use App\Helpers\Algorithm\ClientAwareInterface;
use App\Helpers\Algorithm\TaskInterface;
use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\CheckEmail\Entity\Input;
use App\Helpers\CheckEmail\Task\SearchContacts;
use App\Models\CRM;

/**
 * Class Algorithm
 * @package App\Helpers\CheckEmail
 */
class Algorithm extends AbstractAlgorithm
{
    /**
     * @var \App\Helpers\CheckEmail\Task\SearchContacts
     */
    private $task;
    /**
     * @var \App\Helpers\Bitrix\ClientFactory
     */
    private $clientFactory;
    /**
     * @var \App\Helpers\CheckEmail\Entity\Input
     */
    private $input;
    /**
     * @var \App\Models\CRM
     */
    private $crm;

    /**
     * Algorithm constructor.
     * @param \App\Helpers\CheckEmail\Task\SearchContacts $task
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     */
    public function __construct(SearchContacts $task, ClientFactory $clientFactory)
    {
        $this->task = $task;
        $this->clientFactory = $clientFactory;
    }

    /**
     * @param \App\Helpers\CheckEmail\Entity\Input $input
     */
    public function setInput(Input $input): void
    {
        $this->input = $input;
    }

    /**
     * @param \App\Models\CRM $crm
     */
    public function setCrm(CRM $crm): void
    {
        $this->crm = $crm;
    }

    /**
     * @return TaskInterface
     * @throws \Throwable
     */
    protected function getFirstTask(): TaskInterface
    {
        if (! $this->input instanceof Input) {
            throw new \RuntimeException('Use method "setInput" for set input');
        }

        $task = $this->task;
        $task->setInput($this->input);

        return $task;
    }

    /**
     * @param \App\Helpers\Algorithm\TaskInterface $task
     * @return \App\Helpers\Algorithm\TaskInterface
     */
    protected function prepareTask(TaskInterface $task): TaskInterface
    {
        if (! $task instanceof ClientAwareInterface) {
            return $task;
        }

        if (! $this->crm instanceof CRM) {
            throw new \RuntimeException('Use method "setCrm" for set CRM');
        }

        $task->setClientFactory($this->clientFactory);
        $task->setCrm($this->crm);

        return $task;
    }
}