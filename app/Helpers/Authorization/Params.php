<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 18.07.2018
 * Time: 11:50
 */

namespace App\Helpers\Authorization;

/**
 * Class Params
 * @package App\Helpers\Authentication
 */
class Params
{
    /**
     * @var string
     */
    private $memberId;
    /**
     * @var string
     */
    private $accessToken;
    /**
     * @var string
     */
    private $refreshToken;

    /**
     * Params constructor.
     * @param string $memberId
     * @param string $accessToken
     * @param string $refreshToken
     */
    public function __construct(string $memberId, string $accessToken, string $refreshToken)
    {
        $this->memberId = $memberId;
        $this->accessToken = $accessToken;
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return string
     */
    public function getMemberId(): string
    {
        return $this->memberId;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }
}
