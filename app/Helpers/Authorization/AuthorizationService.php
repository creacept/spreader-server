<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 18.07.2018
 * Time: 15:13
 */

namespace App\Helpers\Authorization;

use App\Helpers\Bitrix\ApiClient;
use App\Helpers\Bitrix\ClientFactory;
use App\Models\CRM;

/**
 * Class AuthorizationService
 * @package App\Helpers\Authorization
 */
class AuthorizationService
{
    /**
     * @var \App\Helpers\Authorization\StorageFactory
     */
    private $storageFactory;

    /**
     * Service constructor.
     * @param \App\Helpers\Authorization\StorageFactory $storageFactory
     */
    public function __construct(StorageFactory $storageFactory)
    {
        $this->storageFactory = $storageFactory;
    }

    /**
     * @param \App\Helpers\Bitrix\ApiClient $client
     * @param \App\Models\CRM $crm
     * @param string $code
     * @return void
     * @throws \Throwable
     */
    public function authorize(ApiClient $client, CRM $crm, string $code): void
    {
        try {
            $data = $client->getFirstAccessToken($code);
            $params = $this->createParams($data);

            $storage = $this->storageFactory->create($crm);
            $storage->setParams($params);
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param \App\Helpers\Bitrix\ApiClient $client
     * @param \App\Models\CRM $crm
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return \App\Helpers\Bitrix\ApiClient
     * @throws \Throwable
     */
    public function populateClient(ApiClient $client, CRM $crm, ClientFactory $clientFactory): ApiClient
    {
        try {
            $storage = $this->storageFactory->create($crm);
            $params = $storage->getParams();

            $memberId = $params->getMemberId();
            $client->setMemberId($memberId);
            $accessToken = $params->getAccessToken();
            $client->setAccessToken($accessToken);
            $client->setOnExpiredToken(function (ApiClient $client) use ($crm, $clientFactory) {
                $authorizationClient = $clientFactory->createApiForAuthorization($crm);
                $params = $this->refreshToken($authorizationClient, $crm);

                $memberId = $params->getMemberId();
                $client->setMemberId($memberId);
                $accessToken = $params->getAccessToken();
                $client->setAccessToken($accessToken);

                return true;
            });
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return $client;
    }

    /**
     * @param \App\Helpers\Bitrix\ApiClient $client
     * @param \App\Models\CRM $crm
     * @return Params
     * @throws \Throwable
     */
    private function refreshToken(ApiClient $client, CRM $crm): Params
    {
        try {
            $storage = $this->storageFactory->create($crm);
            $params = $storage->getParams();

            $refreshToken = $params->getRefreshToken();
            $client->setRefreshToken($refreshToken);
            $data = $client->getNewAccessToken();
            $params = $this->createParams($data);

            $storage->setParams($params);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return $params;
    }

    /**
     * @param array $data
     * @return Params
     */
    private function createParams(array $data): Params
    {
        $memberId = $data['member_id'];
        $accessToken = $data['access_token'];
        $refreshToken = $data['refresh_token'];

        return new Params($memberId, $accessToken, $refreshToken);
    }
}