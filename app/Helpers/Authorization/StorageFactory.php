<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 19.03.2019
 * Time: 12:15
 */

namespace App\Helpers\Authorization;

use App\Models\CRM;
use Illuminate\Contracts\Filesystem\Filesystem;

/**
 * Class StorageFactory
 * @package App\Helpers\Authorization
 */
class StorageFactory
{
    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    private $disk;

    /**
     * StorageFactory constructor.
     * @param \Illuminate\Contracts\Filesystem\Filesystem $disk
     */
    public function __construct(Filesystem $disk)
    {
        $this->disk = $disk;
    }

    /**
     * @param \App\Models\CRM $crm
     * @return \App\Helpers\Authorization\Storage
     */
    public function create(CRM $crm)
    {
        return new Storage($this->disk, $crm);
    }
}