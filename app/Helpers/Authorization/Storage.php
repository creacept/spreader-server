<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 18.07.2018
 * Time: 11:58
 */

namespace App\Helpers\Authorization;

use App\Models\CRM;
use Illuminate\Contracts\Filesystem\Filesystem;
use Throwable;

/**
 * Class Storage
 * @package App\Helpers\Authorization
 */
class Storage
{
    private const MEMBER_ID = 'member_id';
    private const ACCESS_TOKEN = 'access_token';
    private const REFRESH_TOKEN = 'refresh_token';

    /**
     * @var Filesystem
     */
    private $disk;
    /**
     * @var \App\Models\CRM
     */
    private $crm;

    /**
     * Storage constructor.
     * @param Filesystem $disk
     */
    public function __construct(Filesystem $disk, CRM $crm)
    {
        $this->disk = $disk;
        $this->crm = $crm;
    }

    /**
     * @return Params
     * @throws Throwable
     */
    public function getParams(): Params
    {
        $path = $this->getPath();
        try {
            $contents = $this->disk->get($path);
        } catch (Throwable $exception) {
            throw $exception;
        }

        $data = json_decode($contents, true);

        $memberId = $data[self::MEMBER_ID];
        $accessToken = $data[self::ACCESS_TOKEN];
        $refreshToken = $data[self::REFRESH_TOKEN];
        $params = new Params($memberId, $accessToken, $refreshToken);

        return $params;
    }

    /**
     * @param Params $params
     * @throws Throwable
     */
    public function setParams(Params $params): void
    {
        $data = [];
        $data[self::MEMBER_ID] = $params->getMemberId();
        $data[self::ACCESS_TOKEN] = $params->getAccessToken();
        $data[self::REFRESH_TOKEN] = $params->getRefreshToken();

        $contents = json_encode($data);

        $path = $this->getPath();
        try {
            $this->disk->put($path, $contents);
        } catch (Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @return string
     */
    private function getPath()
    {
        return "params.{$this->crm->id}.json";
    }
}