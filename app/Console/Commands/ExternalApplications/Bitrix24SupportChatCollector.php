<?php

namespace App\Console\Commands\ExternalApplications;

use App\Jobs\ExternalApplications\Bitrix24SupportChatCollector as CollectorJob;
use Illuminate\Console\Command;

class Bitrix24SupportChatCollector extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'external-collector:bitrix24supportchat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start collecting of application from Bitrix24\'s support chat';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        CollectorJob::dispatch()->onQueue(CollectorJob::QUEUE);
    }
}
