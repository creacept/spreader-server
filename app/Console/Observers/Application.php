<?php

namespace App\Console\Observers;

use App\Jobs\ApplicationObserver;
use Illuminate\Console\Command;

/**
 * Class Application
 * @package App\Console\Observers
 */
class Application extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'observer:application';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start queue observer for processing of applications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() : void
    {
        if (! $this->confirm('Do you want start new application observer?')) {
            $this->error('Start of application observer is canceled');
            return;
        }

        ApplicationObserver::dispatch()->onQueue(ApplicationObserver::QUEUE);
        $this->info('Application observer is started');
    }
}
