<?php

namespace App\Console\Observers;

use App\Jobs\CollectingObserver;
use Illuminate\Console\Command;

class Collecting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'observer:collecting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start queue observer for collecting of applications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() : void
    {
        if (! $this->confirm('Do you want start new collecting observer?')) {
            $this->error('Start of collecting observer is canceled');
            return;
        }

        CollectingObserver::dispatch()->onQueue(CollectingObserver::QUEUE);
        $this->info('Collecting observer is started');
    }
}
