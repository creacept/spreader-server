<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

/**
 * Class LogoutUnpublishedUser
 * @package App\Http\Middleware
 */
class LogoutUnpublishedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user instanceof User && ! $user->publish) {
            \Auth::logout();
        }

        return $next($request);
    }
}
