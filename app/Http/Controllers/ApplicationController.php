<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Project;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class ApplicationController
 * @package App\Http\Controllers
 */
class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws Throwable
     */
    public function index(Project $project)
    {
        if (\Gate::denies('application.list', [$project])) {
            \App::abort(403);
        }

        try {
            $paginator = Application::with('site')
                ->whereHas('site', function (Builder $query) use ($project) {
                    $query->where('project_id', $project->id);
                })
                ->orderBy('created_at', 'desc')
                ->paginate();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return view('application.index', [
            'project' => $project,
            'paginator' => $paginator,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Project $project
     * @return void
     */
    public function create(Project $project)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Project $project
     * @return void
     */
    public function store(Request $request, Project $project)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Application $application
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Project $project, Application $application)
    {
        if (\Gate::denies('application.view', [$project, $application])) {
            \App::abort(403);
        }

        if ($application->site->project_id !== $project->id) {
            \App::abort(404);
        }

        return view('application.show', [
            'project' => $project,
            'application' => $application,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Application $application
     * @return void
     */
    public function edit(Project $project, Application $application)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Project $project
     * @param  \App\Models\Application $application
     * @return void
     */
    public function update(Request $request, Project $project, Application $application)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Application $application
     * @return void
     */
    public function destroy(Project $project, Application $application)
    {
        throw new \BadMethodCallException('Method not implemented');
    }
}
