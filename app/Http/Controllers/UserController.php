<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\PublishRequest;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\Import;
use App\Models\Project;
use App\Models\User;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        if (\Gate::denies('user.list')) {
            \App::abort(403);
        }

        $isSuperAdmin = $this->isSuperAdmin();

        $query = User::orderBy('name');
        if (! $isSuperAdmin) {
            $query->whereSuperAdmin(false);
        }
        $users = $query->get();

        if ($isSuperAdmin) {
            $countPublishSuperAdmin = $this->countPublishSuperAdmin();
        } else {
            $countPublishSuperAdmin = -1;
        }

        return \View::make('user.index', [
            'isSuperAdmin' => $isSuperAdmin,
            'countPublishSuperAdmin' => $countPublishSuperAdmin,
            'users' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function create()
    {
        if (\Gate::denies('user.create')) {
            \App::abort(403);
        }

        try {
            $projects = Project::orderBy('name')->get();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return \View::make('user.create', [
            'projects' => $projects,
            'isSuperAdmin' => $this->isSuperAdmin(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(StoreRequest $request)
    {
        $attributes = $request->validated();
        $attributes['password'] = \Hash::make($attributes['password']);
        if (array_key_exists('permissions', $attributes)) {
            $attributes['permissions'] = array_values($attributes['permissions']);
        } else {
            $attributes['permissions'] = [];
        }

        $user = new User();
        $user->fill($attributes);
        try {
            $user->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if (\Gate::allows('user.update', [$user])) {
            $redirect = \URL::route('users.edit', ['user' => $user]);
        } else {
            $redirect = \URL::route('users.create');
        }

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return void
     * @throws \Throwable
     */
    public function show(User $user)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function edit(User $user)
    {
        if (\Gate::denies('user.update', [$user])) {
            \App::abort(403);
        }

        $isSuperAdmin = $this->isSuperAdmin();
        if ($user->super_admin && ! $isSuperAdmin) {
            \App::abort(403);
        }

        try {
            $projects = Project::orderBy('name')->get();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return \View::make('user.edit', [
            'projects' => $projects,
            'isSuperAdmin' => $this->isSuperAdmin(),
            'countPublishSuperAdmin' => $this->countPublishSuperAdmin(),
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateRequest $request, User $user)
    {
        $isSuperAdmin = $this->isSuperAdmin();
        if ($user->super_admin && ! $isSuperAdmin) {
            \App::abort(403);
        }

        $attributes = $request->validated();
        if (is_string($attributes['password'])) {
            $attributes['password'] = \Hash::make($attributes['password']);
        } else {
            unset($attributes['password']);
        }
        if (array_key_exists('permissions', $attributes)) {
            $attributes['permissions'] = array_values($attributes['permissions']);
        } else {
            $attributes['permissions'] = [];
        }

        $user->fill($attributes);
        try {
            $user->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $redirect = \URL::route('users.edit', ['user' => $user]);

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse|null
     * @throws \Throwable
     */
    public function destroy(User $user)
    {
        if (\Gate::denies('user.delete', [$user])) {
            \App::abort(403);
        }

        $isSuperAdmin = $this->isSuperAdmin();
        if ($user->super_admin && ! $isSuperAdmin) {
            \App::abort(403);
        }

        if ($user->super_admin && $user->publish) {
            $countPublishSuperAdmin = $this->countPublishSuperAdmin();
            if ($countPublishSuperAdmin === 1) {
                return \Response::json([
                    'message' => "Нельзя удалить пользователя \"{$user->name}\", потому что это единственный активный Super Admin.",
                    'redirect' => \URL::route('users.index'),
                ]);
            }
        }

        try {
            $hasImports = Import::where('user_id', $user->id)->exists();
            if ($hasImports) {
                $user->delete();
            } else {
                $user->forceDelete();
            }
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return null;
    }

    /**
     * @param PublishRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse|null
     * @throws \Throwable
     */
    public function publish(PublishRequest $request, User $user)
    {
        $isSuperAdmin = $this->isSuperAdmin();
        if ($user->super_admin && ! $isSuperAdmin) {
            \App::abort(403);
        }

        $publish = (bool) $request->input('publish');
        if ($publish === $user->publish) {
            return null;
        }

        $countPublishSuperAdmin = $this->countPublishSuperAdmin();
        if ($user->super_admin && $user->publish && ! $publish && $countPublishSuperAdmin === 1) {
            return \Response::json([
                'message' => "Нельзя выключить пользователя \"{$user->name}\", потому что это единственный активный Super Admin.",
                'redirect' => \URL::route('users.index'),
            ]);
        }

        $user->publish = $publish;
        try {
            $user->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if (! $user->super_admin) {
            return null;
        }

        $previousCountPublishSuperAdmin = $countPublishSuperAdmin;
        $countPublishSuperAdmin = $this->countPublishSuperAdmin();
        if (
            ($previousCountPublishSuperAdmin === 1 && $countPublishSuperAdmin > 1) ||
            ($previousCountPublishSuperAdmin > 1 && $countPublishSuperAdmin === 1)
        ) {
            $redirect = \URL::route('users.index');
            return \Response::json(['redirect' => $redirect]);
        }

        return null;
    }

    /**
     * @return bool
     */
    private function isSuperAdmin()
    {
        $user = \Auth::user();
        $isSuperAdmin = ($user instanceof User && $user->super_admin);
        return $isSuperAdmin;
    }

    /**
     * @return int
     */
    private function countPublishSuperAdmin()
    {
        return User::whereSuperAdmin(true)->wherePublish(true)->count();
    }
}
