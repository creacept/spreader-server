<?php

namespace App\Http\Controllers;

use App\Helpers\Bitrix\WebhookConfig;
use App\Helpers\Service\WebhookService;
use App\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Throwable;

/**
 * Class WebhooksController
 * @package App\Http\Controllers
 */
class WebhookController extends Controller
{
    /**
     * @var \App\Helpers\Service\WebhookService
     */
    private $webhookService;

    /**
     * WebhookController constructor.
     * @param \App\Helpers\Service\WebhookService $webhookService
     */
    public function __construct(WebhookService $webhookService)
    {
        $this->webhookService = $webhookService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @throws \libphonenumber\NumberParseException
     */
    public function onContactAdded(Request $request)
    {
        $webhookFields = $request->all();

        $webhookToken = Arr::get($webhookFields, 'auth.application_token');
        if ($webhookToken !== WebhookConfig::ACCESS_TOKEN_CONTACT_ADD) {
            Log::error('Webhook access token is wrong');
            return;
        }

        $contactId = Arr::get($webhookFields, 'data.FIELDS.ID');
        if ($contactId == null) {
            return;
        }

        $contact = $this->webhookService->getContact($contactId);
        if (count($contact) === 0) {
            return;
        }

        $application = $this->webhookService->createApplicationFromContact($contact);
        if (! $application instanceof Application) {
            return;
        }

        $isContactDeleted = $this->webhookService->deleteContact($contactId);
        if (! $isContactDeleted) {
            Log::warning("Contact with ID {$contactId} wasn't deleted after creating of application");
        }
    }

    public function onSurveyReceived(Request $request)
    {
        $requestData = $request->post();
        $validator = Validator::make(
            $requestData,
            [
                'phone' => 'required|phone:AUTO,mobile',
                'crm_id' => 'required|exists:crm,id',
            ],
            [
                'phone.required' => 'valid phone number',
                'crm_id.required' => 'valid ID of CRM',
                'phone' => 'valid phone number',
                'exists' => 'valid ID of CRM',
            ]
        );
        if ($validator->fails()) {
            $failedFields = implode(', ', $validator->errors()->all());
            return;
        }

        try {
            $matchedContacts = $this->webhookService->findContactByPhone($requestData['phone'], $requestData['crm_id']);
        } catch (Throwable $e) {
            $errorMessage = $e->getMessage();
            return;
        }

        $matchedContacts = Arr::get($matchedContacts, 'result', []);
        if (! is_array($matchedContacts) || count($matchedContacts) <= 0) {
            return;
        }

        $contact = array_shift($matchedContacts);
        $contactId = (int) $contact['ID'];

        $updated = $this->webhookService->updateContactData($contactId, $requestData, $requestData['crm_id']);
        return;
    }
}
