<?php

namespace App\Http\Controllers;

use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\Bitrix\DealCategory;
use App\Http\Requests\CRM\StoreRequest;
use App\Http\Requests\CRM\UpdateRequest;
use App\Http\Requests\CRM\UpdateSettingsRequest;
use App\Models\CRM;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

/**
 * Class CRMController
 * @package App\Http\Controllers
 */
class CRMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function index()
    {
        if (Gate::denies('crm.list')) {
            App::abort(403);
        }

        try {
            $collection = CRM::with(['projects'])->orderBy('name')->get();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return View::make('crm.index', [
            'collection' => $collection,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('crm.create')) {
            App::abort(403);
        }

        return View::make('crm.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CRM\StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        $config = $data['config'];
        unset($config['auth']['domain']);
        unset($config['auth']['client_id']);
        unset($config['auth']['client_secret']);
        unset($config['auth']['webhook']);
        if ($config['auth']['type'] === CRM::AUTH_OAUTH2) {
            $config['auth']['domain'] = $data['config']['auth']['domain'];
            $config['auth']['client_id'] = $data['config']['auth']['client_id'];
            $config['auth']['client_secret'] = $data['config']['auth']['client_secret'];
        } elseif ($config['auth']['type'] === CRM::AUTH_WEBHOOK) {
            $config['auth']['webhook'] = $data['config']['auth']['webhook'];
        }

        $crm = new CRM();
        $crm->name = $data['name'];
        $crm->config = $config;
        $crm->settings = [];
        try {
            $crm->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if (Gate::allows('crm.update', [$crm])) {
            $redirect = URL::route('crm.edit', ['crm' => $crm]);
        } else {
            $redirect = URL::route('crm.create');
        }

        return Response::json(['redirect' => $redirect]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CRM $crm
     * @return void
     */
    public function show(CRM $crm)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CRM  $crm
     * @return \Illuminate\Http\Response
     */
    public function edit(CRM $crm)
    {
        if (Gate::denies('crm.update', [$crm])) {
            App::abort(403);
        }

        return View::make('crm.edit', [
            'crm' => $crm,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\CRM\UpdateRequest $request
     * @param  \App\Models\CRM $crm
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateRequest $request, CRM $crm)
    {
        $data = $request->validated();

        $config = $data['config'];
        unset($config['auth']['domain']);
        unset($config['auth']['client_id']);
        unset($config['auth']['client_secret']);
        unset($config['auth']['webhook']);
        if ($config['auth']['type'] === CRM::AUTH_OAUTH2) {
            $config['auth']['domain'] = $data['config']['auth']['domain'];
            $config['auth']['client_id'] = $data['config']['auth']['client_id'];
            $config['auth']['client_secret'] = $data['config']['auth']['client_secret'];
        } elseif ($config['auth']['type'] === CRM::AUTH_WEBHOOK) {
            $config['auth']['webhook'] = $data['config']['auth']['webhook'];
        }
        if (! is_string($config['db']['password'])) {
            unset($config['db']['password']);
        }

        $crm->name = $data['name'];
        $crm->config = array_replace_recursive($crm->config, $config);
        try {
            $crm->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $redirect = route('crm.edit', ['crm' => $crm]);

        return Response::json(['redirect' => $redirect]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CRM $crm
     * @return void
     * @throws \Throwable
     */
    public function destroy(CRM $crm)
    {
        if (Gate::denies('crm.delete', [$crm])) {
            App::abort(403);
        }

        if ($crm->projects->count() > 0) {
            App::abort(422);
        }

        try {
            $crm->delete();
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param \App\Models\CRM $crm
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function editSettings(CRM $crm, ClientFactory $clientFactory)
    {
        if (Gate::denies('crm.settings', [$crm])) {
            App::abort(403);
        }

        try {
            $client = $clientFactory->create($crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $dealCategories = [];
        $api = new DealCategory($client);
        $start = 0;
        do {
            try {
                $result = $api->getList(['NAME' => 'ASC'], [], ['ID', 'NAME'], $start);
            } catch (\Throwable $exception) {
                throw $exception;
            }
            $start = $result['next'] ?? false;

            foreach ($result['result'] as $data) {
                $dealCategories[] = [
                    'id' => (int) $data['ID'],
                    'name' => $data['NAME'],
                ];
            }
        } while($start !== false);

        return View::make('crm.settings', [
            'crm' => $crm,
            'dealCategories' => $dealCategories,
        ]);
    }

    /**
     * @param \App\Http\Requests\CRM\UpdateSettingsRequest $request
     * @param \App\Models\CRM $crm
     * @throws \Throwable
     */
    public function updateSettings(UpdateSettingsRequest $request, CRM $crm)
    {
        $data = $request->validated();

        $settings = [];
        if (array_key_exists('ignored_deal_categories', $data)) {
            $values = array_values($data['ignored_deal_categories']);
            $values = array_map('intval', $values);
            $settings['ignored_deal_categories'] = $values;
        }

        $crm->settings = $settings;
        try {
            $crm->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }
}
