<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        $user = \Auth::user();
        if (! $user instanceof User) {
            App::abort(403);
        }

        $projects = Project::all();
        $projects = $projects->filter(function (Project $project) {
            return (
                Gate::allows('manager.list', [$project]) ||
                Gate::allows('group.list', [$project]) ||
                Gate::allows('site.list', [$project]) ||
                Gate::allows('application.list', [$project]) ||
                Gate::allows('import.list', [$project])
            );
        });

        if ($projects->count() > 1) {
            return $this->createDefaultResponse();
        }

        if ($projects->count() === 1) {
            $project = $projects->first();
            if (Gate::allows('manager.list', [$project])) {
                return \Response::redirectToRoute('projects.managers.index', ['project' => $project]);
            }
            if (Gate::allows('group.list', [$project])) {
                return \Response::redirectToRoute('projects.groups.index', ['project' => $project]);
            }
            if (Gate::allows('site.list', [$project])) {
                return \Response::redirectToRoute('projects.sites.index', ['project' => $project]);
            }
            if (Gate::allows('application.list', [$project])) {
                return \Response::redirectToRoute('projects.applications.index', ['project' => $project]);
            }
            if (Gate::allows('import.list', [$project])) {
                return \Response::redirectToRoute('projects.imports.index', ['project' => $project]);
            }
        }

        $count = 0;
        $canUser = Gate::allows('user.list');
        $canCRM = Gate::allows('crm.list');
        if ($canUser || $canCRM) {
            $count++;
        }
        $canAuthorization = Gate::allows('authorization');
        if ($canAuthorization) {
            $count++;
        }
        $canHorizon = Gate::allows('horizon');
        if ($canHorizon) {
            $count++;
        }

        if ($count === 1) {
            if ($canUser) {
                return Response::redirectToRoute('users.index');
            }
            if ($canCRM) {
                return Response::redirectToRoute('crm.index');
            }
            if ($canAuthorization) {
                return Response::redirectToRoute('authorization');
            }
            if ($canHorizon) {
                return Response::redirectToRoute('horizon.index');
            }
        }

        return $this->createDefaultResponse();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    private function createDefaultResponse()
    {
        return Response::redirectToRoute('projects.index');
    }
}
