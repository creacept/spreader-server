<?php

namespace App\Http\Controllers;

use App\Http\Requests\Rule\PublishRequest;
use App\Http\Requests\Rule\ReorderRequest;
use App\Http\Requests\Rule\StoreRequest;
use App\Http\Requests\Rule\UpdateRequest;
use App\Models\Group;
use App\Models\Project;
use App\Models\Rule;
use App\Models\Site;
use Throwable;

/**
 * Class RuleController
 * @package App\Http\Controllers
 */
class RuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Site $site
     * @return \Illuminate\Http\Response
     * @throws Throwable
     */
    public function index(Site $site)
    {
        if (\Gate::denies('rule.list', [$site])) {
            \App::abort(403);
        }

        $project = $site->project;
        if (! $project instanceof Project) {
            \App::abort(404);
        }

        try {
            $rules = Rule::with(['group'])
                ->whereSiteId($site->id)
                ->orderBy('priority')
                ->get();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return \View::make('rule.index', [
            'project' => $project,
            'site' => $site,
            'rules' => $rules,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Site $site
     * @return \Illuminate\Http\Response
     * @throws Throwable
     */
    public function create(Site $site)
    {
        if (\Gate::denies('rule.create', [$site])) {
            \App::abort(403);
        }

        $project = $site->project;
        if (! $project instanceof Project) {
            \App::abort(404);
        }

        try {
            $groups = Group::whereProjectId($project->id)
                ->orderBy('name')
                ->get();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return \View::make('rule.create', [
            'project' => $project,
            'site' => $site,
            'groups' => $groups,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @param  \App\Models\Site $site
     * @return \Illuminate\Http\JsonResponse
     * @throws Throwable
     */
    public function store(StoreRequest $request, Site $site)
    {
        if (! $site->project instanceof Project) {
            \App::abort(404);
        }

        $attributes = $request->validated();
        if (
            $attributes['form_name'] === null &&
            $attributes['page_url_domain'] === null &&
            $attributes['page_url_path'] === null &&
            $attributes['page_title'] === null &&
            $attributes['utm_source'] === null &&
            $attributes['utm_content'] === null &&
            $attributes['utm_campaign'] === null &&
            $attributes['utm_medium'] === null &&
            $attributes['sub_id'] === null
        ) {
            return \Response::json(['message' => 'Должно быть задано хотя бы одно условие.']);
        }

        $rule = new Rule();
        $rule->site_id = $site->id;
        $rule->fill($attributes);

        try {
            Rule::whereSiteId($site->id)->increment('priority');
            $rule->saveOrFail();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (\Gate::allows('rule.update', [$site, $rule])) {
            $redirect = \URL::route('sites.rules.edit', ['site' => $site, 'rule' => $rule]);
        } else {
            $redirect = \URL::route('sites.rules.create', ['site' => $site]);
        }

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Site $site
     * @param  \App\Models\Rule $rule
     * @return void
     */
    public function show(Site $site, Rule $rule)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Site $site
     * @param  \App\Models\Rule $rule
     * @return \Illuminate\Http\Response
     * @throws Throwable
     */
    public function edit(Site $site, Rule $rule)
    {
        if (\Gate::denies('rule.update', [$site, $rule])) {
            \App::abort(403);
        }

        $project = $site->project;
        if (! $project instanceof Project || $rule->site_id !== $site->id) {
            abort(404);
        }

        try {
            $groups = Group::whereProjectId($project->id)
                ->orderBy('name')
                ->get();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return \View::make('rule.edit', [
            'project' => $project,
            'site' => $site,
            'rule' => $rule,
            'groups' => $groups,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest $request
     * @param  \App\Models\Site $site
     * @param  \App\Models\Rule $rule
     * @return \Illuminate\Http\JsonResponse
     * @throws Throwable
     */
    public function update(UpdateRequest $request, Site $site, Rule $rule)
    {
        if (! $site->project instanceof Project || $rule->site_id !== $site->id) {
            \App::abort(404);
        }

        $attributes = $request->validated();
        if (
            $attributes['form_name'] === null &&
            $attributes['page_url_domain'] === null &&
            $attributes['page_url_path'] === null &&
            $attributes['page_title'] === null &&
            $attributes['utm_source'] === null &&
            $attributes['utm_content'] === null &&
            $attributes['utm_campaign'] === null &&
            $attributes['utm_medium'] === null &&
            $attributes['sub_id'] === null
        ) {
            return \Response::json(['message' => 'Должно быть задано хотя бы одно условие.']);
        }

        try {
            $rule->update($attributes);
        } catch (Throwable $exception) {
            throw $exception;
        }

        return null;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Site $site
     * @param  \App\Models\Rule $rule
     * @return void
     * @throws Throwable
     */
    public function destroy(Site $site, Rule $rule)
    {
        if (\Gate::denies('rule.delete', [$site, $rule])) {
            \App::abort(403);
        }

        if (! $site->project instanceof Project || $rule->site_id !== $site->id) {
            \App::abort(404);
        }

        try {
            Rule::whereSiteId($site->id)
                ->where('priority', '>', $rule->priority)
                ->decrement('priority');
            $rule->delete();
        } catch (Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param PublishRequest $request
     * @param Site $site
     * @param Rule $rule
     * @throws Throwable
     */
    public function publish(PublishRequest $request, Site $site, Rule $rule)
    {
        if (! $site->project instanceof Project || $rule->site_id !== $site->id) {
            \App::abort(404);
        }

        $publish = (bool) $request->input('publish');
        if ($publish == $rule->publish) {
            return;
        }

        $rule->publish = $publish;
        try {
            $rule->saveOrFail();
        } catch (Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param ReorderRequest $request
     * @param Site $site
     * @param Rule $rule
     * @return \Illuminate\Http\JsonResponse|null
     * @throws Throwable
     */
    public function reorder(ReorderRequest $request, Site $site, Rule $rule)
    {
        if (! $site->project instanceof Project || $rule->site_id !== $site->id) {
            \App::abort(404);
        }

        $offset = (int) $request->input('offset');
        $priority = $rule->priority;
        $reload = false;

        if ($priority + $offset < 0) {
            $offset = -1 * $priority;
            $reload = true;
        }

        try {
            $count = Rule::whereSiteId($site->getKey())->count();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ($priority + $offset >= $count) {
            $offset = $count - $priority - 1;
            $reload = true;
        }

        if ($offset === 0) {
            return null;
        }

        $rule->priority += $offset;
        try {
            $rule->saveOrFail();
            if ($offset > 0) {
                Rule::whereSiteId($site->id)
                    ->whereKeyNot($rule->id)
                    ->whereBetween('priority', [$priority, $priority + $offset])
                    ->decrement('priority');
            } else {
                Rule::whereSiteId($site->id)
                    ->whereKeyNot($rule->id)
                    ->whereBetween('priority', [$priority + $offset, $priority])
                    ->increment('priority');
            }
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (! $reload) {
            return null;
        }

        $redirect = \URL::route('sites.rules.index', ['site' => $site]);

        return \Response::json(['redirect' => $redirect]);
    }
}
