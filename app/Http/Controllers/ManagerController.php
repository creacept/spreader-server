<?php

namespace App\Http\Controllers;

use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\Bitrix\User;
use App\Http\Requests\Manager\PublishRequest;
use App\Http\Requests\Manager\StoreRequest;
use App\Http\Requests\Manager\UpdateRequest;
use App\Models\Group;
use App\Models\Manager;
use App\Models\Project;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

/**
 * Class ManagerController
 * @package App\Http\Controllers
 */
class ManagerController extends Controller
{
    /**
     * @var \App\Helpers\Bitrix\ClientFactory
     */
    private $clientFactory;

    /**
     * ManagerController constructor.
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     */
    public function __construct(ClientFactory $clientFactory)
    {
        $this->clientFactory = $clientFactory;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\View\View
     * @throws \Throwable
     */
    public function index(Project $project)
    {
        if (Gate::denies('manager.list', [$project])) {
            App::abort(403);
        }

        try {
            $managers = Manager::with('groups')->whereProjectId($project->id)->get();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $managers = $managers->keyBy(function (Manager $manager) {
            return $manager->bitrix24_user_id;
        });

        try {
            $client = $this->clientFactory->create($project->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $api = new User($client);
        $bitrix24Users = new Collection();
        $bitrix24UserIdChunks = $managers->keys()->chunk(50);
        foreach ($bitrix24UserIdChunks as $bitrix24UserIds) {
            /** @var Collection $bitrix24UserIds */
            try {
                $result = $api->get('ID', 'ASC', ['ID' => $bitrix24UserIds->toArray()]);
            } catch (\Throwable $exception) {
                throw $exception;
            }

            foreach ($result['result'] as $data) {
                $user = [
                    'id' => $data['ID'],
                    'name' => $this->formatName($data),
                    'manager' => $managers->get($data['ID']),
                ];
                $bitrix24Users->push($user);
            }
        }
        $bitrix24Users = $bitrix24Users->sortBy('name');

        return View::make('manager.index', [
            'project' => $project,
            'bitrix24Users' => $bitrix24Users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\View\View
     * @throws \Throwable
     */
    public function create(Project $project)
    {
        if (Gate::denies('manager.create', [$project])) {
            App::abort(403);
        }

        try {
            $managers = Manager::whereProjectId($project->id)->get();
        } catch (\Throwable $exception) {
            throw $exception;
        }
        $managers = $managers->keyBy(function (Manager $manager) {
            return $manager->bitrix24_user_id;
        });

        try {
            $client = $this->clientFactory->create($project->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $api = new User($client);
        $bitrix24Users = new Collection();
        $start = 0;
        do {
            try {
                $result = $api->get('', '', ['ACTIVE' => true], $start);
            } catch (\Throwable $exception) {
                throw $exception;
            }
            $start = $result['next'] ?? false;

            foreach ($result['result'] as $data) {
                $bitrix24Users->push([
                    'id' => $data['ID'],
                    'name' => $this->formatName($data),
                    'exists' => $managers->has($data['ID']),
                ]);
            }
        } while ($start !== false);
        $bitrix24Users = $bitrix24Users->sortBy('name');

        return View::make('manager.create', [
            'project' => $project,
            'bitrix24Users' => $bitrix24Users,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @param  \App\Models\Project $project
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(StoreRequest $request, Project $project)
    {
        $bitrix24UserIds = $request->input('bitrix24_user_id');
        if (! is_array($bitrix24UserIds) || count($bitrix24UserIds) === 0) {
            return Response::json(['message' => 'Должен быть выбран хотя бы один пользователь Битрикс24.']);
        }

        foreach ($bitrix24UserIds as $bitrix24UserId) {
            $manager = new Manager();
            $manager->project_id = $project->id;
            $manager->bitrix24_user_id = $bitrix24UserId;
            try {
                $manager->saveOrFail();
            } catch (\Throwable $exception) {
                throw $exception;
            }
        }

        if (Gate::allows('manager.update', [$project, $manager])) {
            $redirect = URL::route('projects.managers.index', ['project' => $project]);
        } else {
            $redirect = URL::route('projects.managers.create', ['project' => $project]);
        }

        return Response::json(['redirect' => $redirect]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param Manager $manager
     * @throws \Throwable
     */
    public function show(Project $project, Manager $manager)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Manager $manager
     * @return \Illuminate\View\View
     * @throws \Throwable
     */
    public function edit(Project $project, Manager $manager)
    {
        if (Gate::denies('manager.update', [$project, $manager])) {
            App::abort(403);
        }

        if ($manager->project_id !== $project->id) {
            App::abort(404);
        }

        try {
            $client = $this->clientFactory->create($project->crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $api = new User($client);
        try {
            $result = $api->get('', '', ['ID' => $manager->bitrix24_user_id]);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $managerName = "!!! НЕИЗВЕСТНЫЙ #{$manager->bitrix24_user_id} !!!";
        if (! empty($result['result'])) {
            $data = reset($result['result']);
            $managerName = $this->formatName($data);
        }

        try {
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            $groups = Group::whereProjectId($project->id)->orderBy('name')->get();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return View::make('manager.edit', [
            'project' => $project,
            'manager' => $manager,
            'managerName' => $managerName,
            'groups' => $groups,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  \App\Models\Project $project
     * @param  \App\Models\Manager $manager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateRequest $request, Project $project, Manager $manager)
    {
        if ($manager->project_id !== $project->id) {
            App::abort(404);
        }

        $groupIds = $request->input('group_id', []);
        $attributes = $request->validated();
        $originalPublish = $manager->publish;
        $originalRation = $manager->ratio;

        try {
            $manager->update($attributes);
            $manager->groups()->sync($groupIds);

            Group::whereProjectId($project->id)
                ->whereDoesntHave('managers')
                ->update(['publish' => false]);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if (
            $manager->publish && (! $originalPublish ||$manager->ratio !== $originalRation)
        ){
            try {
                $this->resetApplicationNumber();
            } catch (\Throwable $exception) {
                throw $exception;
            }
        }

        $redirect = URL::route('projects.managers.edit', ['project' => $project, 'manager' => $manager]);

        return Response::json(['redirect' => $redirect]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Manager $manager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(Project $project, Manager $manager)
    {
        if (Gate::denies('manager.delete', [$project, $manager])) {
            App::abort(403);
        }

        if ($manager->project_id !== $project->id) {
            App::abort(404);
        }

        try {
            $manager->delete();

            Group::whereProjectId($project->id)
                ->whereDoesntHave('managers')
                ->update(['publish' => false]);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $redirect = URL::route('projects.managers.index', ['project' => $project]);

        return Response::json(['redirect' => $redirect]);
    }

    /**
     * @param PublishRequest $request
     * @param Project $project
     * @param Manager $manager
     * @return \Illuminate\Http\JsonResponse|null
     * @throws \Throwable
     */
    public function publish(PublishRequest $request, Project $project, Manager $manager)
    {
        if ($manager->project_id !== $project->id) {
            App::abort(404);
        }

        $publish = (bool) $request->input('publish');
        if ($publish === $manager->publish) {
            return null;
        }

        $manager->publish = $publish;
        try {
            $manager->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        try {
            if ($publish) {
                $this->resetApplicationNumber();
            } else {
                Group::whereProjectId($project->id)
                    ->whereDoesntHave('managers')
                    ->update(['publish' => false]);
            }
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $redirect = URL::route('projects.managers.index', ['project' => $project]);

        return Response::json(['redirect' => $redirect]);
    }

    /**
     * @param array $data
     * @return string
     */
    private function formatName(array $data) : string
    {
        $names = [];

        if (! empty($data['LAST_NAME'])) {
            $names[] = $data['LAST_NAME'];
        }
        if (! empty($data['NAME'])) {
            $names[] = $data['NAME'];
        }
        if (! empty($data['SECOND_NAME'])) {
            $names[] = $data['SECOND_NAME'];
        }

        return implode(' ', $names);
    }

    /**
     * @throws \Throwable
     */
    private function resetApplicationNumber() : void
    {
        try {
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            Manager::where('application_number', '>', 0)->update(['application_number' => 0]);
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }
}
