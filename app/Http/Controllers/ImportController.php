<?php

namespace App\Http\Controllers;

use App\Helpers\Service\MoodleService;
use App\Http\Requests\ImportStoreRequest;
use App\Jobs\Import as Job;
use App\Models\Import;
use App\Models\Project;
use App\Models\Site;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Class ImportController
 * @package App\Http\Controllers
 */
class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function index(Project $project)
    {
        if (\Gate::denies('import.list', [$project])) {
            \App::abort(403);
        }

        try {
            $paginator = Import::with(['site', 'user'])
                ->withCount(['applications', 'errors'])
                ->whereHas('site', function (Builder $query) use ($project) {
                    $query->where('project_id', $project->id);
                })
                ->orderBy('created_at', 'desc')
                ->paginate();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return \View::make('import.index', [
            'project' => $project,
            'paginator' => $paginator,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\Response
     * @throws \Throwable
     */
    public function create(Project $project)
    {
        if (\Gate::denies('import.create', [$project])) {
            \App::abort(404);
        }

        try {
            $sites = Site::whereProjectId($project->id)
                ->whereType(Site::IMPORT_TYPE)
                ->orderBy('name')
                ->get();
        } catch (\Throwable $exception) {
            throw  $exception;
        }

        $moodleGroups = [];
        $baseUrl = Arr::get($project->moodle, 'base_url');
        $accessToken = Arr::get($project->moodle, 'access_token');
        if ($baseUrl!== null && $accessToken !== null) {
            /** @var \App\Helpers\Service\MoodleService $moodleService */
            $moodleService = \App::makeWith(MoodleService::class, ['baseUrl' => $baseUrl, 'accessToken' => $accessToken]);
            if ($moodleService instanceof MoodleService) {
                try {
                    $moodleCourses = $moodleService->getCourses(MoodleService::FREE_COURSES_CATEGORIES_IDS);
                    foreach ($moodleCourses as $course) {
                        $moodleGroups[] = [
                            'name' => $course['fullname'],
                            'value' => $course['id'] . ':',
                        ];
                        foreach ($course['groups'] as $group) {
                            $moodleGroups[] = [
                                'name' => "{$course['fullname']}, группа {$group['name']}",
                                'value'=> $course['id'] . ':' . $group['id'],
                            ];
                        }
                    }
                } catch (GuzzleException $e) {}
            }
        }

        return \View::make('import.create', [
            'project' => $project,
            'sites' => $sites,
            'moodleGroups' => $moodleGroups,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ImportStoreRequest $request
     * @param  \App\Models\Project $project
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(ImportStoreRequest $request, Project $project)
    {
        $file = $request->file('file');
        $disk = \Storage::disk(Import::DISK);
        $fileName = $disk->putFile('', $file);
        if ($fileName === false) {
            $message = 'Не удалось сохранить файл на сервере. Попробуйте отправить форму ещё раз.';
            return \Response::json(['message' => $message]);
        }

        $data = $request->validated();
        $siteId = $data['site_id'];
        $priority = $data['priority'];

        $moodleData = ['course_id' => null, 'group_id' => null];
        $moodleCourseGroup = Arr::get($data, 'moodle_course');
        if (is_string($moodleCourseGroup)) {
            $moodleCourseGroup = array_filter(explode(':', $moodleCourseGroup));
            $moodleData['course_id'] = $moodleCourseGroup[0];
            if (count($moodleCourseGroup) === 2) {
                $moodleData['group_id'] = $moodleCourseGroup[1];
            }
        }

        $user = \Auth::user();

        $import = new Import();
        $import->file_name = $fileName;
        $import->site_id = $siteId;
        $import->user_id = $user->id;
        $import->status = Import::PENDING_STATUS;
        $import->moodle_data = $moodleData;
        $import->priority = $priority;

        try {
            $import->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        Job::dispatch($import);

        $redirect = \URL::route('projects.imports.create', ['project' => $project]);
        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Import $import
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Project $project, Import $import)
    {
        if (\Gate::denies('import.view', [$project, $import])) {
            \App::abort(403);
        }

        $rows = [];
        foreach ($import->applications as $application) {
            $index = $application->pivot->index;
            $rows[$index] = $application;
        }
        foreach ($import->errors as $error) {
            $index = $error->index;
            $rows[$index] = $error;
        }
        ksort($rows);

        return \View::make('import.show', [
            'project' => $project,
            'import' => $import,
            'rows' => $rows,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Import $import
     * @return void
     */
    public function edit(Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Project $project
     * @param  \App\Models\Import $import
     * @return void
     */
    public function update(Request $request, Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Import $import
     * @return void
     */
    public function destroy(Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function pattern(Project $project)
    {
        if (\Gate::denies('import.create', [$project])) {
            \App::abort(403);
        }

        $path = \Storage::disk('public')->path('Import - Pattern.xlsx');

        return \Response::download($path, 'Импорт - Шаблон.xlsx');
    }
}
