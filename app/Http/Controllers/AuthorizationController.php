<?php

namespace App\Http\Controllers;

use App\Helpers\Authorization\AuthorizationService;
use App\Helpers\Bitrix\ClientFactory;
use App\Http\Requests\AuthorizationFinishRequest;
use App\Models\CRM;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;
use Throwable;

/**
 * Class AuthorizationController
 * @package App\Http\Controllers
 */
class AuthorizationController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index()
    {
        if (Gate::denies('authorization')) {
            App::abort(403);
        }

        try {
            $collection = CRM::orderBy('name')->get();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return View::make('authorization.index', [
            'collection' => $collection,
        ]);
    }

    /**
     * @param \App\Models\CRM $crm
     * @return \Illuminate\Contracts\View\View
     */
    public function show(CRM $crm)
    {
        if (Gate::denies('authorization')) {
            App::abort(403);
        }

        return View::make('authorization.show', [
            'crm' => $crm,
        ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function start(CRM $crm)
    {
        if (Gate::denies('authorization')) {
            App::abort(403);
        }

        if (Arr::get($crm->config, 'auth.type') !== CRM::AUTH_OAUTH2) {
            return Response::redirectToRoute('authorization.show', ['crm' => $crm])->withErrors(
                new MessageBag([
                    'Авторизация нужна только для CRM, которые авторизуются через OAuth 2.0.',
                ])
            );
        }

        $domain = Arr::get($crm->config, 'auth.domain');
        $clientId = Arr::get($crm->config, 'auth.client_id');
        $query = http_build_query([
            'response_type' => 'code',
            'client_id' => $clientId,
        ]);
        $path = "https://{$domain}/oauth/authorize/?{$query}";

        return Response::redirectTo($path);
    }

    /**
     * @param AuthorizationFinishRequest $request
     * @param \App\Models\CRM $crm
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @param AuthorizationService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function finish(AuthorizationFinishRequest $request, CRM $crm, ClientFactory $clientFactory, AuthorizationService $service)
    {
        $data = $request->validated();
        $code = $data['code'];

        $errors = new MessageBag();
        try {
            $client = $clientFactory->createApiForAuthorization($crm);
            $service->authorize($client, $crm, $code);

            Session::flash("authorization:{$crm->id}", 'Авторизация выполнена!');
        } catch (Throwable $exception) {
            report($exception);
            $errors->add('exception', $exception->getMessage());
        }

        return Response::redirectToRoute('authorization.show', ['crm' => $crm])->withErrors($errors);
    }
}
