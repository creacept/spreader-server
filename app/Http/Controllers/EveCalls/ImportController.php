<?php

namespace App\Http\Controllers\EveCalls;

use App\Http\Controllers\Controller;
use App\Http\Requests\Evecalls\ImportRequest;
use App\Models\EveCalls\Import;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Class ImportController
 * @package App\Http\Controllers\EveCalls
 */
class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function index(Project $project)
    {
        if (\Gate::denies('evecalls-import.list', [$project])) {
            \App::abort(403);
        }

        try {
            $paginator = Import::with(['user'])
                ->withCount(['applications', 'errors'])
                ->where('project_id', $project->id)
                ->orderBy('created_at', 'desc')
                ->paginate();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return \View::make('evecalls-import.index', [
            'project' => $project,
            'paginator' => $paginator,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\Response
     * @throws \Throwable
     */
    public function create(Project $project)
    {
        if (\Gate::denies('evecalls-import.create', [$project])) {
            \App::abort(404);
        }

        return \View::make('evecalls-import.create', [
            'project' => $project,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Evecalls\ImportRequest $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(ImportRequest $request, Project $project)
    {
        $file = $request->file('file');
        $disk = \Storage::disk(Import::DISK);
        $fileName = $disk->putFile('', $file);
        if ($fileName === false) {
            $message = 'Не удалось сохранить файл на сервере. Попробуйте отправить форму ещё раз.';
            return \Response::json(['message' => $message]);
        }

        $data = $request->validated();

        $user = \Auth::user();

        $import = new Import();
        $import->file_name = $fileName;
        $import->user_id = $user->id;
        $import->status = Import::PENDING_STATUS;
        $import->project()->associate($project);

        $leadName = Arr::get($data, 'lead_name');
        $import->lead_name = ($leadName !== null) ? $leadName : Import::DEFAULT_LEAD_NAME;

        try {
            $import->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        \App\Jobs\EveCalls\Import::dispatch($import);

        $redirect = \URL::route('projects.evecalls-imports.create', ['project' => $project]);
        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Project $project
     * @param \App\Models\EveCalls\Import $import
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Project $project, Import $import)
    {
        if (\Gate::denies('evecalls-import.view', [$project, $import])) {
            \App::abort(403);
        }

        $crmRecordingDateField = Arr::get($project->evecalls, 'date_field');
        $crmRecordingUrlField = Arr::get($project->evecalls, 'recording_field');

        return \View::make('evecalls-import.show', [
            'project' => $project,
            'import' => $import,
            'crm_date_field' => $crmRecordingDateField,
            'crm_recording_field' => $crmRecordingUrlField,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Project $project
     * @param \App\Models\EveCalls\Import $import
     * @return void
     */
    public function edit(Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @param \App\Models\EveCalls\Import $import
     * @return void
     */
    public function update(Request $request, Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Project $project
     * @param \App\Models\EveCalls\Import $import
     * @return void
     */
    public function destroy(Project $project, Import $import)
    {
        throw new \BadMethodCallException('Method not implemented');
    }
}
