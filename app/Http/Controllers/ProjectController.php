<?php

namespace App\Http\Controllers;

use App\Helpers\Bitrix\ClientFactory;
use App\Http\Requests\Project\PublishRequest;
use App\Http\Requests\Project\StoreRequest;
use App\Http\Requests\Project\UpdateRequest;
use App\Models\Application;
use App\Models\CRM;
use App\Models\Import;
use App\Models\Project;
use App\Models\Rule;
use App\Models\Site;
use App\Models\User;
use Bitrix24\CRM\Contact\UserField as ContactUserField;
use Bitrix24\CRM\Lead\UserField as LeadUserField;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

/**
 * Class ProjectController
 * @package App\Http\Controllers
 */
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function index()
    {
        try {
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            $projects = Project::orderBy('name')->get();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return \View::make('project.index', [
            'projects' => $projects,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function create()
    {
        if (Gate::denies('project.create')) {
            App::abort(403);
        }

        try {
            $collection = CRM::orderBy('name')->get();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return \View::make('project.create', [
            'collection' => $collection,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(StoreRequest $request, ClientFactory $clientFactory)
    {
        $attributes = $request->validated();

        try {
            $this->validateIdentities($attributes, $clientFactory);
            $project = Project::create($attributes);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if (Gate::allows('project.update', [$project])) {
            $redirect = URL::route('projects.edit', ['project' => $project]);
        } else {
            $redirect = URL::route('projects.index');
        }

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project $project
     * @return void
     */
    public function show(Project $project)
    {
        throw new \BadMethodCallException('Method not implemented');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function edit(Project $project, ClientFactory $clientFactory)
    {
        if (Gate::denies('project.update', [$project])) {
            App::abort(403);
        }

        $contactIdentities = [];
        $leadIdentities = [];
        try {
            $collection = CRM::orderBy('name')->get();
            $crm = $project->crm;
            if ($crm instanceof CRM) {
                $contactIdentities = $this->getContactIdentityList($project->crm, $clientFactory);
                $leadIdentities = $this->getLeadIdentityList($project->crm, $clientFactory);
            }
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return \View::make('project.edit', [
            'project' => $project,
            'collection' => $collection,
            'contactIdentities' => $contactIdentities,
            'leadIdentities' => $leadIdentities,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  \App\Models\Project $project
     * @return void
     * @throws \Throwable
     */
    public function update(UpdateRequest $request, Project $project, ClientFactory $clientFactory)
    {
        $attributes = $request->validated();

        try {
            $this->validateIdentities($attributes, $clientFactory);
            $project->update($attributes);
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project $project
     * @return void
     * @throws \Throwable
     */
    public function destroy(Project $project)
    {
        if (Gate::denies('project.delete', [$project])) {
            App::abort(403);
        }

        try {
            /* Start delete imports */
            Import::whereHas('site', function (Builder $query) use ($project) {
                $query->where('project_id', $project->id);
            })->delete();
            /* End delete imports */

            /* Start delete users */
            $userIds = Import::select(['user_id'])
                ->distinct('user_id')
                ->getQuery()
                ->pluck('user_id');
            User::onlyTrashed()->whereNotIn('id', $userIds)->forceDelete();
            /* End delete users */

            /* Start delete sites */
            $siteIds = Application::select(['site_id'])
                ->distinct('site_id')
                ->getQuery()
                ->pluck('site_id');

            Site::whereProjectId($project->id)
                ->whereNotIn('id', $siteIds)
                ->forceDelete();

            Site::whereProjectId($project->id)->update(['publish' => false]);
            Rule::whereHas('site', function (Builder $query) use ($project) {
                $query->where('project_id', $project->id);
            })->delete();
            Site::whereProjectId($project->id)->delete();
            /* End delete sites */

            $project->delete();
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param PublishRequest $request
     * @param Project $project
     * @throws \Throwable
     */
    public function publish(PublishRequest $request, Project $project)
    {
        $publish = (bool) $request->input('publish');

        $project->publish = $publish;
        try {
            $project->saveOrFail();
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param \App\Models\CRM $crm
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function crm(CRM $crm, ClientFactory $clientFactory)
    {
        if (! Auth::check()) {
            App::abort(403);
        }

        $data = [];
        try {
            $data['contactIdentities'] = $this->getContactIdentityList($crm, $clientFactory);
            $data['leadIdentities'] = $this->getLeadIdentityList($crm, $clientFactory);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return Response::json($data);
    }

    /**
     * @param array $attributes
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @throws \Throwable
     */
    private function validateIdentities(array $attributes, ClientFactory $clientFactory)
    {
        /** @var \App\Models\CRM $crm */
        try {
            $crm = CRM::findOrFail($attributes['crm_id']);
            $contactIdentities = $this->getContactIdentityList($crm, $clientFactory);
            $leadIdentities = $this->getLeadIdentityList($crm, $clientFactory);

            Validator::validate($attributes, [
                'contact_identity' => [
                    function ($attribute, $value, $fail) use ($contactIdentities) {
                        $data = Arr::first($contactIdentities, function ($data) use ($value) {
                            return ($data['id'] === (int)$value);
                        });

                        if (! is_array($data)) {
                            $fail('Invalid identity');
                        }
                    },
                ],
                'lead_identity' => [
                    function ($attribute, $value, $fail) use ($leadIdentities) {
                        $data = Arr::first($leadIdentities, function ($data) use ($value) {
                            return ($data['id'] === (int)$value);
                        });

                        if (! is_array($data)) {
                            $fail('Invalid identity');
                        }
                    },
                ]
            ]);
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param \App\Models\CRM $crm
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return array
     * @throws \Throwable
     */
    private function getContactIdentityList(CRM $crm, ClientFactory $clientFactory)
    {
        try {
            $client = $clientFactory->create($crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $api = new ContactUserField($client);
        try {
            $id = Arr::get($crm->config, 'field.contact_identity');
            $result = $api->get($id);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $identityList = [];
        foreach (Arr::get($result,'result.LIST') as $data) {
            $id = (int) Arr::get($data, 'ID');
            $text = Arr::get($data, 'VALUE');
            $identityList[] = ['id' => $id, 'text' => $text];
        }
        $identityList = Arr::sort($identityList, function (array $data) {
            return $data['text'];
        });

        return array_values($identityList);
    }

    /**
     * @param \App\Models\CRM $crm
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return array
     * @throws \Throwable
     */
    private function getLeadIdentityList(CRM $crm, ClientFactory $clientFactory)
    {
        try {
            $client = $clientFactory->create($crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $api = new LeadUserField($client);
        try {
            $id = Arr::get($crm->config, 'field.lead_identity');
            $result = $api->get($id);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $identityList = [];
        foreach (Arr::get($result,'result.LIST') as $data) {
            $id = (int) Arr::get($data, 'ID');
            $text = Arr::get($data, 'VALUE');
            $identityList[] = ['id' => $id, 'text' => $text];
        }
        $identityList = Arr::sort($identityList, function (array $data) {
            return $data['text'];
        });

        return array_values($identityList);
    }
}
