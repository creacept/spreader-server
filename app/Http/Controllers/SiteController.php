<?php

namespace App\Http\Controllers;

use App\Http\Requests\Site\PublishRequest;
use App\Http\Requests\Site\StoreImportRequest;
use App\Http\Requests\Site\StoreRequest;
use App\Http\Requests\Site\UpdateImportRequest;
use App\Http\Requests\Site\UpdateRequest;
use App\Models\Application;
use App\Models\Group;
use App\Models\Import;
use App\Models\Rule;
use App\Models\Site;
use App\Models\Project;
use BadMethodCallException;
use Illuminate\Support\Arr;
use Throwable;

/**
 * Class SiteController
 * @package App\Http\Controllers
 */
class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Contracts\View\View
     * @throws Throwable
     */
    public function index(Project $project)
    {
        if (\Gate::denies('site.list', [$project])) {
            \App::abort(403);
        }

        try {
            $sites = Site::with(['groups'])
                ->whereProjectId($project->id)
                ->orderBy('name')
                ->get();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return \View::make('site.index', [
            'project' => $project,
            'sites' => $sites,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Contracts\View\View
     * @throws Throwable
     */
    public function create(Project $project)
    {
        if (\Gate::denies('site.create', [$project])) {
            \App::abort(403);
        }

        try {
            $groups = Group::whereProjectId($project->id)->orderBy('name')->get();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return \View::make('site.create', [
            'project' => $project,
            'groups' => $groups,
        ]);
    }

    /**
     * @param Project $project
     * @return \Illuminate\Contracts\View\View
     * @throws Throwable
     */
    public function createImport(Project $project)
    {
        if (\Gate::denies('site.create', [$project])) {
            \App::abort(403);
        }

        try {
            $groups = Group::whereProjectId($project->id)->orderBy('name')->get();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return \View::make('site.create_import', [
            'project' => $project,
            'groups' => $groups,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @param  \App\Models\Project $project
     * @return \Illuminate\Http\JsonResponse
     * @throws Throwable
     */
    public function store(StoreRequest $request, Project $project)
    {
        $attributes = $request->validated();
        $groupIds = Arr::get($attributes, 'group_id', []);

        $site = new Site();
        $site->project_id = $project->id;
        $site->type = Site::SITE_TYPE;
        $site->fill($attributes);
        try {
            $site->saveOrFail();
            $site->groups()->sync($groupIds);
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (\Gate::allows('site.update', [$project, $site])) {
            $redirect = \URL::route('projects.sites.edit', ['project' => $project, 'site' => $site]);
        } else {
            $redirect = \URL::route('projects.sites.create', ['project' => $project]);
        }

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * @param StoreImportRequest $request
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     * @throws Throwable
     */
    public function storeImport(StoreImportRequest $request, Project $project)
    {
        $attributes = $request->validated();
        $groupIds = Arr::get($attributes, 'group_id', []);
        $attributes['url'] = '';
        $attributes['password'] = '';

        $site = new Site();
        $site->project_id = $project->id;
        $site->type = Site::IMPORT_TYPE;
        $site->fill($attributes);
        try {
            $site->saveOrFail();
            $site->groups()->sync($groupIds);
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (\Gate::allows('site.update', [$project, $site])) {
            $redirect = \URL::route('projects.sites.edit_import', ['project' => $project, 'site' => $site]);
        } else {
            $redirect = \URL::route('projects.sites.create_import', ['project' => $project]);
        }

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Site $site
     * @return void
     * @throws Throwable
     */
    public function show(Project $project, Site $site)
    {
        throw new BadMethodCallException('Method not implemented');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Site $site
     * @return \Illuminate\Contracts\View\View
     * @throws Throwable
     */
    public function edit(Project $project, Site $site)
    {
        if (\Gate::denies('site.update', [$project, $site])) {
            \App::abort(403);
        }

        if ($site->project_id !== $project->id || $site->type !== Site::SITE_TYPE) {
            \App::abort(404);
        }

        try {
            $groups = Group::whereProjectId($project->id)->orderBy('name')->get();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return \View::make('site.edit', [
            'project' => $project,
            'site' => $site,
            'groups' => $groups,
        ]);
    }

    /**
     * @param Project $project
     * @param Site $site
     * @return \Illuminate\Contracts\View\View
     * @throws Throwable
     */
    public function editImport(Project $project, Site $site)
    {
        if (\Gate::denies('site.update', [$project, $site])) {
            \App::abort(403);
        }

        if ($site->project_id !== $project->id || $site->type !== Site::IMPORT_TYPE) {
            \App::abort(404);
        }

        try {
            $groups = Group::whereProjectId($project->id)->orderBy('name')->get();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return \View::make('site.edit_import', [
            'project' => $project,
            'site' => $site,
            'groups' => $groups,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  \App\Models\Project $project
     * @param  \App\Models\Site $site
     * @return \Illuminate\Http\JsonResponse|null
     * @throws Throwable
     */
    public function update(UpdateRequest $request, Project $project, Site $site)
    {
        if ($site->project_id !== $project->id || $site->type !== Site::SITE_TYPE) {
            \App::abort(404);
        }

        $attributes = $request->validated();
        $groupIds = Arr::get($attributes, 'group_id', []);
        if (! is_string($attributes['password'])) {
            unset($attributes['password']);
        }

        try {
            $site->update($attributes);
            $site->groups()->sync($groupIds);
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ($project->publish) {
            return null;
        }

        $redirect = \URL::route('projects.sites.edit', ['project' => $project, 'site' => $site]);

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * @param UpdateImportRequest $request
     * @param Project $project
     * @param Site $site
     * @return \Illuminate\Http\JsonResponse|null
     * @throws Throwable
     */
    public function updateImport(UpdateImportRequest $request, Project $project, Site $site)
    {
        if ($site->project_id !== $project->id || $site->type !== Site::IMPORT_TYPE) {
            \App::abort(404);
        }

        $attributes = $request->validated();
        $groupIds = Arr::get($attributes, 'group_id', []);

        try {
            $site->update($attributes);
            $site->groups()->sync($groupIds);
        } catch (Throwable $exception) {
            throw $exception;
        }

        if ($project->publish) {
            return null;
        }

        $redirect = \URL::route('projects.sites.edit_import', ['project' => $project, 'site' => $site]);

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @param  \App\Models\Site $site
     * @return void
     * @throws Throwable
     */
    public function destroy(Project $project, Site $site)
    {
        if (\Gate::denies('site.delete', [$project, $site])) {
            \App::abort(403);
        }

        if ($site->project_id !== $project->id) {
            abort(404);
        }

        try {
            $siteIds = Application::select(['site_id'])
                ->distinct('site_id')
                ->getQuery()
                ->pluck('site_id');
            $isUsed = $siteIds->contains($site->id);

            if (! $isUsed) {
                $siteIds = Import::select(['site_id'])
                    ->distinct('site_id')
                    ->getQuery()
                    ->pluck('site_id');
                $isUsed = $siteIds->contains($site->id);
            }

            if ($isUsed) {
                Rule::whereSiteId($site->id)->delete();

                $site->publish = false;
                $site->saveOrFail();
                $site->delete();
            } else {
                $site->forceDelete();
            }
        } catch (Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param PublishRequest $request
     * @param Project $project
     * @param Site $site
     * @return \Illuminate\Http\JsonResponse|null
     * @throws Throwable
     */
    public function publish(PublishRequest $request, Project $project, Site $site)
    {
        if ($site->project_id !== $project->id) {
            abort(404);
        }

        $publish = (bool) $request->input('publish');
        if ($publish === $site->publish) {
            return null;
        }

        $site->publish = $publish;
        try {
            $site->saveOrFail();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return null;
    }
}
