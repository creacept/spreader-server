<?php
namespace App\Http\Controllers;


use App;
use App\Http\Requests\UrlsCommentsRequest;
use App\Models\UrlsComment;
use Auth;
use Gate;

/**
 * Class UrlsCommentsController
 * @package App\Http\Controllers
 */
class UrlsCommentsController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private $user;

    /**
     * UrlsCommentsController constructor.
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        if (Gate::denies('urlscomment.list', [$this->user])) {
            App::abort(403);
        }

        $urlsComments = UrlsComment::all()->toArray();

        return view('urls_comments.index', ['urls_comments' => $urlsComments]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        if (Gate::denies('urlscomment.add', [$this->user])) {
            App::abort(403);
        }

        return view('urls_comments.create');
    }

    /**
     * @param UrlsComment $urlsComment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editRecord(UrlsComment $urlsComment)
    {
        if (Gate::denies('urlscomment.update', [$this->user])) {
            App::abort(403);
        }

        $data = $urlsComment->toArray();

        return view('urls_comments.edit')->with(['record' => $data]);
    }

    /**
     * @param UrlsComment $urlsComment
     * @param UrlsCommentsRequest $request
     */
    public function editRecordPost(UrlsComment $urlsComment, UrlsCommentsRequest $request)
    {
        if (Gate::denies('urlscomment.update', [$this->user])) {
            App::abort(403);
        }

        $dirtyData = $request->all();
        $dirtyUrls = explode(PHP_EOL, $dirtyData['urls']);
        $dirtyComment = $dirtyData['comment'];
        $clearData = $this->getClearData($dirtyUrls);
        $clearComment = trim($dirtyComment);

        try {
            $urlsComment->update([
                'urls' => $clearData,
                'comment' => $clearComment,
            ]);
        } catch (\Throwable $exception) {
            report($exception);
            App::abort(500);
        }
    }

    /**
     * @param UrlsComment $urlsComment
     */
    public function deleteRecord(UrlsComment $urlsComment)
    {
        if (Gate::denies('urlscomment.delete', [$this->user])) {
            App::abort(403);
        }

        try {
            $urlsComment->delete();
        } catch (\Exception $e) {
            report($e);
            abort(404);
        }
    }

    /**
     * @param UrlsCommentsRequest $request
     */
    public function store(UrlsCommentsRequest $request)
    {
        if (Gate::denies('urlscomment.add', [$this->user])) {
            App::abort(403);
        }

        $dirtyData = $request->all();
        $dirtyUrls = explode(PHP_EOL, $dirtyData['urls']);
        $dirtyComment = $dirtyData['comment'];
        $clearData = $this->getClearData($dirtyUrls);
        $clearComment = trim($dirtyComment);

        try {
            $urlsComment = new UrlsComment();
            $urlsComment->urls = $clearData;
            $urlsComment->comment = $clearComment;
            $urlsComment->saveOrFail();
        } catch (\Throwable $exception) {
            report($exception);
            App::abort(500, 'Ошибка сохранения в базе данных');
        }
    }

    /**
     * @param array $dirtyData
     * @return array
     */
    private function getClearData(array $dirtyData)
    {
        $clearData = [];
        foreach ($dirtyData as $url){
            $clearData[] = trim($url);
        }

        return $clearData;
    }
}
