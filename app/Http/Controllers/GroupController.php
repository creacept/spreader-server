<?php

namespace App\Http\Controllers;

use App\Http\Requests\Group\PublishRequest;
use App\Http\Requests\Group\StoreRequest;
use App\Http\Requests\Group\UpdateRequest;
use App\Models\Group;
use App\Models\Project;
use BadMethodCallException;
use Throwable;

/**
 * Class GroupController
 * @package App\Http\Controllers
 */
class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws Throwable
     */
    public function index(Project $project)
    {
        if (\Gate::denies('group.list', [$project])) {
            \App::abort(403);
        }

        try {
            $groups = Group::withCount(['managers'])
                ->whereProjectId($project->id)
                ->orderBy('name')
                ->get();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return view('group.index', [
            'project' => $project,
            'groups' => $groups,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Project $project)
    {
        if (\Gate::denies('group.create', [$project])) {
            \App::abort(403);
        }

        return view('group.create', [
            'project' => $project,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @param  \App\Models\Project $project
     * @return \Illuminate\Http\JsonResponse
     * @throws Throwable
     */
    public function store(StoreRequest $request, Project $project)
    {
        $attributes = $request->validated();

        $group = new Group();
        $group->project_id = $project->id;
        $group->fill($attributes);
        $group->publish = false;
        try {
            $group->saveOrFail();
        } catch (Throwable $exception) {
            throw $exception;
        }

        try {
            $this->resetApplicationNumber();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (\Gate::allows('group.update', [$project, $group])) {
            $redirect = \URL::route('projects.groups.edit', ['project' => $project, 'group' => $group]);
        } else {
            $redirect = \URL::route('projects.groups.create', ['project' => $project]);
        }

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @param  \App\Models\Group  $group
     * @throws Throwable
     */
    public function show(Project $project, Group $group)
    {
        throw new BadMethodCallException('Method not implemented');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Group $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Project $project, Group $group)
    {
        if (\Gate::denies('group.update', [$project, $group])) {
            \App::abort(403);
        }

        if ($group->project_id !== $project->id) {
            \App::abort(404);
        }

        return view('group.edit', [
            'project' => $project,
            'group' => $group,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  \App\Models\Project $project
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\JsonResponse|null
     * @throws Throwable
     */
    public function update(UpdateRequest $request, Project $project, Group $group)
    {
        if ($group->project_id !== $project->id) {
            \App::abort(404);
        }

        $attributes = $request->validated();
        $originalPublish = $group->publish;
        $originalRation = $group->ratio;

        $group->fill($attributes);
        if ($group->managers->isEmpty()) {
            $group->publish = false;
        }

        try {
            $group->saveOrFail();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (
            $group->publish && (! $originalPublish ||$group->ratio !== $originalRation)
        ){
            try {
                $this->resetApplicationNumber();
            } catch (Throwable $exception) {
                throw $exception;
            }
        }

        $redirect = \URL::route('projects.groups.edit', ['project' => $project, 'group' => $group]);

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project $project
     * @param  \App\Models\Group $group
     * @return void
     * @throws Throwable
     */
    public function destroy(Project $project, Group $group)
    {
        if (\Gate::denies('group.delete', [$project, $group])) {
            \App::abort(403);
        }

        if ($group->project_id !== $project->id) {
            \App::abort(404);
        }

        try {
            $group->delete();
        } catch (Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param PublishRequest $request
     * @param Project $project
     * @param Group $group
     * @return \Illuminate\Http\JsonResponse|null
     * @throws Throwable
     */
    public function publish(PublishRequest $request, Project $project, Group $group)
    {
        if ($group->project_id !== $project->id) {
            \App::abort(404);
        }
        $redirect = \URL::route('projects.groups.index', ['project' => $project]);

        $publish = (bool) $request->input('publish');
        if ($publish && $group->managers->isEmpty()) {
            return \Response::json([
                'result' => false,
                'message' => "Нельзя включить группу \"{$group->name}\", потому что она не содержит активных менеджеров.",
                'redirect' => route('projects.groups.index', ['project' => $project]),
            ]);
        }

        if ($publish === $group->publish) {
            return null;
        }

        $group->publish = $publish;
        try {
            $group->saveOrFail();
        } catch (Throwable $exception) {
            throw $exception;
        }

        if (! $publish) {
            return null;
        }

        try {
            $this->resetApplicationNumber();
        } catch (Throwable $exception) {
            throw $exception;
        }

        return \Response::json(['redirect' => $redirect]);
    }

    /**
     * @throws Throwable
     */
    private function resetApplicationNumber() : void
    {
        try {
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            Group::where('application_number', '>', 0)->update(['application_number' => 0]);
        } catch (Throwable $exception) {
            throw $exception;
        }
    }
}
