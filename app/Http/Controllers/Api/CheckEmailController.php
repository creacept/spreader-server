<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CheckEmail\Algorithm;
use App\Helpers\CheckEmail\Entity\Input;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CheckEmailRequest;
use App\Models\Project;
use App\Models\Site;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;

/**
 * Class CheckEmailController
 * @package App\Http\Controllers\Api
 */
class CheckEmailController extends Controller
{
    /**
     * @var \App\Helpers\CheckEmail\Algorithm
     */
    private $algorithm;

    /**
     * CheckEmailController constructor.
     * @param \App\Helpers\CheckEmail\Algorithm $algorithm
     */
    public function __construct(Algorithm $algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @param \App\Http\Requests\Api\CheckEmailRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function __invoke(CheckEmailRequest $request)
    {
        $values = $request->validated();
        $auth = $values['auth'];
        $data = $values['data'];

        try {
            $site = Site::whereUrl($auth['url'])->wherePassword($auth['password'])->first();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        if (! $site instanceof Site || ! $site->publish) {
            App::abort(403);
        }

        $project = $site->project;
        if (! $project instanceof Project || ! $project->publish) {
            App::abort(403);
        }

        $input = new Input(
            $site,
            $data['email'],
            Carbon::parse($data['date']),
            $data['form_name'],
            $data['page_url'],
            $data['page_title'],
            $data['utm_source'],
            $data['utm_content'],
            $data['utm_campaign'],
            $data['utm_medium'],
            $data['sub_id']
        );
        $algorithm = $this->algorithm;
        $algorithm->setInput($input);
        $algorithm->setCrm($project->crm);

        try {
            $result = $algorithm->execute();
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return Response::json($result);
    }
}
