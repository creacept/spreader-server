<?php

namespace App\Http\Requests\CRM;

use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\Bitrix\DealCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

/**
 * Class UpdateSettingsRequest
 * @package App\Http\Requests\CRM
 */
class UpdateSettingsRequest extends FormRequest
{
    /**
     * @var int[]
     */
    private $dealCategories;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $crm = $this->getCrm();
        return Gate::allows('crm.settings', [$crm]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return array
     */
    public function rules(ClientFactory $clientFactory)
    {
        return [
            'ignored_deal_categories.*' => [
                'required',
                'integer',
                function ($attribute, $value, $fail) use ($clientFactory) {
                    try {
                        $dealCategories = $this->getDealCategories($clientFactory);
                    } catch (\Throwable $exception) {
                        throw $exception;
                    }

                    if (! in_array($value, $dealCategories)) {
                        return $fail('Invalid entity of deal category');
                    }

                    return null;
                },
            ],
        ];
    }

    /**
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return array
     * @throws \Throwable
     */
    private function getDealCategories(ClientFactory $clientFactory) : array
    {
        if (is_array($this->dealCategories)) {
            return $this->dealCategories;
        }

        try {
            $crm = $this->getCrm();
            $client = $clientFactory->create($crm);
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $dealCategories = [];
        $api = new DealCategory($client);
        $start = 0;
        do {
            try {
                $result = $api->getList([], [], ['ID'], $start);
            } catch (\Throwable $exception) {
                throw $exception;
            }
            $start = $result['next'] ?? false;

            foreach ($result['result'] as $data) {
                $dealCategories[] = (int) $data['ID'];
            }
        } while($start !== false);

        $this->dealCategories = $dealCategories;

        return $this->dealCategories;
    }

    /**
     * @return \App\Models\CRM
     */
    private function getCrm()
    {
        /** @var \App\Models\CRM $crm */
        $crm = $this->route('crm');
        return $crm;
    }
}
