<?php

namespace App\Http\Requests\CRM;

use App\Models\CRM;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AbstractRequest
 * @package App\Http\Requests\CRM
 */
class AbstractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
            ],
            'config.auth.type' => [
                'required',
                Rule::in(CRM::AUTH_OAUTH2, CRM::AUTH_WEBHOOK),
            ],
            'config.auth.domain' => [
                'nullable',
                'required_if:config.auth.type,' . CRM::AUTH_OAUTH2,
                'string',
            ],
            'config.auth.client_id' => [
                'nullable',
                'required_if:config.auth.type,' . CRM::AUTH_OAUTH2,
                'string',
            ],
            'config.auth.client_secret' => [
                'nullable',
                'required_if:config.auth.type,' . CRM::AUTH_OAUTH2,
                'string',
            ],
            'config.auth.webhook' => [
                'nullable',
                'required_if:config.auth.type,' . CRM::AUTH_WEBHOOK,
                'url',
            ],
            'config.db.host' => [
                'required',
                'string',
            ],
            'config.db.port' => [
                'required',
                'string',
                'digits_between:1,65535',
            ],
            'config.db.database' => [
                'required',
                'string',
            ],
            'config.db.login' => [
                'required',
                'string',
            ],
            'config.db.password' => [
                'required',
                'string',
            ],
            'config.contact.identity' => [
                'required',
                'string',
            ],
            'config.contact.worked_at' => [
                'required',
                'string',
            ],
            'config.contact.vk' => [
                'required',
                'string',
            ],
            'config.lead.identity' => [
                'required',
                'string',
            ],
            'config.lead.url' => [
                'required',
                'string',
            ],
            'config.lead.title' => [
                'required',
                'string',
            ],
            'config.lead.sub_id' => [
                'required',
                'string',
            ],
            'config.lead.is_new' => [
                'required',
                'string',
            ],
            'config.lead.contact_id' => [
                'required',
                'string',
            ],
            'config.field.contact_identity' => [
                'required',
                'integer',
                'min:1',
            ],
            'config.field.lead_identity' => [
                'required',
                'integer',
                'min:1',
            ],
            'config.workflow.after_create_lead' => [
                'required',
                'integer',
                'min:1',
            ],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => '"Название"',
            'config.auth.type' => '"Тип"',
            'config.auth.domain' => '"Домен"',
            'config.auth.client_id' => '"Код приложения"',
            'config.auth.client_secret' => '"Ключ приложения"',
            'config.auth.webhook' => '"Webhook"',
            'config.db.host' => '"Хост"',
            'config.db.port' => '"Порт"',
            'config.db.database' => '"База данных"',
            'config.db.login' => '"Логин"',
            'config.db.password' => '"Пароль"',
            'config.contact.identity' => '"Проект"',
            'config.contact.worked_at' => '"Дата последнего обращения"',
            'config.contact.vk' => '"Страница VK"',
            'config.lead.identity' => '"Проект"',
            'config.lead.url' => '"URL"',
            'config.lead.title' => '"Заголовок страницы"',
            'config.lead.sub_id' => '"SUB ID"',
            'config.lead.is_new' => '"Новорег"',
            'config.field.contact_identity' => '"Проект (контакты)"',
            'config.field.lead_identity' => '"Проект (лиды)"',
            'config.workflow.after_create_lead' => '"После создания лида"',
        ];
    }
}
