<?php

namespace App\Http\Requests\CRM;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\CRM
 */
class UpdateRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var \App\Models\CRM $crm */
        $crm = $this->route('crm');

        return \Gate::allows('crm.update', [$crm]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $password = $rules['config.db.password'];
        array_shift($password);
        array_unshift($password, 'nullable');
        $rules['config.db.password'] = $password;

        return $rules;
    }
}
