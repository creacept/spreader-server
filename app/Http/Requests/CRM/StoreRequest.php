<?php

namespace App\Http\Requests\CRM;

use Illuminate\Support\Facades\Gate;

/**
 * Class StoreRequest
 * @package App\Http\Requests\CRM
 */
class StoreRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('crm.create');
    }
}
