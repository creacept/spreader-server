<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 23.10.2018
 * Time: 16:34
 */

namespace App\Http\Requests\Site;

/**
 * Class AbstractImportRequest
 * @package App\Http\Requests\Site
 */
abstract class AbstractImportRequest extends AbstractRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        unset($rules['url']);
        unset($rules['password']);

        return $rules;
    }
}