<?php

namespace App\Http\Requests\Site;

use App\Http\Requests\ProjectTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PublishRequest
 * @package App\Http\Requests\Site
 */
class PublishRequest extends FormRequest
{
    use ProjectTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();
        /** @var \App\Models\Site $site */
        $site = $this->route('site');

        return \Gate::allows('site.update', [$project, $site]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'publish' => [
                'required',
                'boolean',
            ],
        ];
    }
}
