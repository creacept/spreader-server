<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 23.10.2018
 * Time: 16:36
 */

namespace App\Http\Requests\Site;

use App\Http\Requests\ProjectTrait;

/**
 * Class StoreImportRequest
 * @package App\Http\Requests\Site
 */
class StoreImportRequest extends AbstractImportRequest
{
    use ProjectTrait;

    /**
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();

        return \Gate::allows('site.create', [$project]);
    }
}