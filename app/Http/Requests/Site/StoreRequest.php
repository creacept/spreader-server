<?php

namespace App\Http\Requests\Site;

use App\Http\Requests\ProjectTrait;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Site
 */
class StoreRequest extends AbstractRequest
{
    use ProjectTrait;

    /**
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();

        return \Gate::allows('site.create', [$project]);
    }
}
