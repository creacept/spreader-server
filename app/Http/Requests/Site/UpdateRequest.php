<?php

namespace App\Http\Requests\Site;

use App\Http\Requests\ProjectTrait;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Site
 */
class UpdateRequest extends AbstractRequest
{
    use ProjectTrait;

    /**
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();
        /** @var \App\Models\Site $site */
        $site = $this->route('site');

        return \Gate::allows('site.update', [$project, $site]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $password = $rules['password'];
        array_shift($password);
        array_unshift($password, 'nullable');
        $rules['password'] = $password;

        return $rules;
    }
}
