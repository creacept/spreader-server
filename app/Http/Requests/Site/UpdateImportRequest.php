<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 23.10.2018
 * Time: 16:37
 */

namespace App\Http\Requests\Site;

use App\Http\Requests\ProjectTrait;

/**
 * Class UpdateImportRequest
 * @package App\Http\Requests\Site
 */
class UpdateImportRequest extends AbstractImportRequest
{
    use ProjectTrait;

    /**
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();
        /** @var \App\Models\Site $site */
        $site = $this->route('site');

        return \Gate::allows('site.update', [$project, $site]);
    }
}