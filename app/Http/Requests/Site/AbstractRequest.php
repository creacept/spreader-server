<?php

namespace App\Http\Requests\Site;

use App\Http\Requests\ProjectTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AbstractRequest
 * @package App\Http\Requests\Site
 */
abstract class AbstractRequest extends FormRequest
{
    use ProjectTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $project = $this->getProject();

        return [
            'name' => [
                'required',
                'string',
            ],
            'url' => [
                'required',
                'url',
            ],
            'password' => [
                'required',
                'string',
                'min:16',
            ],
            'publish' => [
                'required',
                'boolean',
            ],
            'group_id.*' => [
                'bail',
                'required',
                'integer',
                Rule::exists('groups', 'id')->where('project_id', $project->id),
            ],
        ];
    }
}
