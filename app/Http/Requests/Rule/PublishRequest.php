<?php

namespace App\Http\Requests\Rule;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PublishRequest
 * @package App\Http\Requests\Rule
 */
class PublishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var \App\Models\Site $site */
        $site = $this->route('site');
        /** @var \App\Models\Rule $rule */
        $rule = $this->route('rule');

        return \Gate::allows('rule.update', [$site, $rule]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'publish' => [
                'required',
                'boolean',
            ],
        ];
    }
}
