<?php

namespace App\Http\Requests\Rule;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AbstractRequest
 * @package App\Http\Requests\Rule
 */
abstract class AbstractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** @var \App\Models\Site $site */
        $site = $this->route('site');

        return [
            'group_id' => [
                'bail',
                'nullable',
                'integer',
                Rule::exists('groups', 'id')->where('project_id', $site->project_id),
            ],
            'publish' => [
                'required',
                'boolean',
            ],
            'form_name' => [
                'nullable',
                'string',
            ],
            'page_url_domain' => [
                'nullable',
                'string',
            ],
            'page_url_path' => [
                'nullable',
                'string',
            ],
            'page_title' => [
                'nullable',
                'string',
            ],
            'utm_source' => [
                'nullable',
                'string',
            ],
            'utm_content' => [
                'nullable',
                'string',
            ],
            'utm_campaign' => [
                'nullable',
                'string',
            ],
            'utm_medium' => [
                'nullable',
                'string',
            ],
            'sub_id' => [
                'nullable',
                'string',
            ],
        ];
    }
}
