<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 22.10.2018
 * Time: 15:25
 */

namespace App\Http\Requests\Rule;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Rule
 */
class UpdateRequest extends AbstractRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        /** @var \App\Models\Site $site */
        $site = $this->route('site');
        /** @var \App\Models\Rule $rule */
        $rule = $this->route('rule');

        return \Gate::allows('rule.update', [$site, $rule]);
    }
}