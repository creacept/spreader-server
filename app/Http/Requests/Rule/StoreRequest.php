<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 22.10.2018
 * Time: 15:24
 */

namespace App\Http\Requests\Rule;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Rule
 */
class StoreRequest extends AbstractRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        /** @var \App\Models\Site $site */
        $site = $this->route('site');

        return \Gate::allows('rule.create', [$site]);
    }
}