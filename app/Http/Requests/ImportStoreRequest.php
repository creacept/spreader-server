<?php

namespace App\Http\Requests;

use App\Models\Import;
use App\Models\Site;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ImportStoreRequest
 * @package App\Http\Requests
 */
class ImportStoreRequest extends FormRequest
{
    use ProjectTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();

        return \Gate::allows('import.create', [$project]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $project = $this->getProject();

        return [
            'site_id' => [
                'bail',
                'required',
                'integer',
                Rule::exists('sites', 'id')
                    ->where('type', Site::IMPORT_TYPE)
                    ->where('project_id', $project->id),
            ],
            'priority' => [
                'required',
                'integer',
                Rule::in([Import::PRIORITY_NORMAL, Import::PRIORITY_HIGH]),
            ],
            'file' => [
                'required',
                'file',
                'mimes:xls,xlsx',
                'mimetypes:application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ],
            'moodle_course' => [
                'nullable',
                'string',
            ]
        ];
    }
}
