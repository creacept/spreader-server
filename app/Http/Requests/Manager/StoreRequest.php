<?php

namespace App\Http\Requests\Manager;

use App\Helpers\Bitrix\ClientFactory;
use App\Helpers\Bitrix\User;
use App\Http\Requests\ProjectTrait;
use App\Models\Manager;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Throwable;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Manager
 */
class StoreRequest extends FormRequest
{
    use ProjectTrait;

    /**
     * @var
     */
    private $validBitrix24UserIds;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();

        return Gate::allows('manager.create', [$project]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return array
     */
    public function rules(ClientFactory $clientFactory)
    {
        return [
            'bitrix24_user_id.*' => [
                'required',
                'integer',
                function ($attribute, $value, $fail) use ($clientFactory) {
                    try {
                        $validBitrix24UserIds = $this->getValidBitrix24UsersIds($clientFactory);
                    } catch (Throwable $exception) {
                        throw $exception;
                    }

                    if (! in_array($value, $validBitrix24UserIds)) {
                        return $fail('Invalid manager entity');
                    }

                    return null;
                },
            ],
        ];
    }

    /**
     * @param \App\Helpers\Bitrix\ClientFactory $clientFactory
     * @return array
     * @throws \Throwable
     */
    private function getValidBitrix24UsersIds(ClientFactory $clientFactory) : array
    {
        if (is_array($this->validBitrix24UserIds)) {
            return $this->validBitrix24UserIds;
        }

        $project = $this->getProject();

        try {
            $managers = Manager::whereProjectId($project->id)->get();
        } catch (Throwable $exception) {
            throw $exception;
        }
        $managers = $managers->keyBy(function (Manager $manager) {
            return $manager->bitrix24_user_id;
        });

        try {
            $client = $clientFactory->create($project->crm);
        } catch (Throwable $exception) {
            throw $exception;
        }
        $api = new User($client);

        $validBitrix24UserIds = [];
        $start = 0;
        do {
            try {
                $result = $api->get('', '', ['ACTIVE' => true], $start);
            } catch (Throwable $exception) {
                throw $exception;
            }
            $start = $result['next'] ?? false;

            foreach ($result['result'] as $data) {
                $id = $data['ID'];
                if ($managers->has($id)) {
                    continue;
                }
                $validBitrix24UserIds[] = $id;
            }
        } while ($start !== false);

        $this->validBitrix24UserIds = $validBitrix24UserIds;

        return $this->validBitrix24UserIds;
    }
}
