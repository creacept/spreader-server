<?php

namespace App\Http\Requests\Manager;

use App\Http\Requests\ProjectTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Manager
 */
class UpdateRequest extends FormRequest
{
    use ProjectTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();
        /** @var \App\Models\Manager $manager */
        $manager = $this->route('manager');

        return Gate::allows('manager.update', [$project, $manager]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $project = $this->getProject();

        return [
            'group_id.*' => [
                'bail',
                'required',
                'integer',
                Rule::exists('groups', 'id')->where('project_id', $project->id),
            ],
            'publish' => [
                'required',
                'boolean',
            ],
            'ratio' => [
                'required',
                'integer',
                'min:1',
            ],
        ];
    }
}
