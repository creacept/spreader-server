<?php

namespace App\Http\Requests\Manager;

use App\Http\Requests\ProjectTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

/**
 * Class PublishRequest
 * @package App\Http\Requests\Manager
 */
class PublishRequest extends FormRequest
{
    use ProjectTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();
        /** @var \App\Models\Manager $manager */
        $manager = $this->route('manager');

        return Gate::allows('manager.update', [$project, $manager]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'publish' => [
                'required',
                'boolean',
            ],
        ];
    }
}
