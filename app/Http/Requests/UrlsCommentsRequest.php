<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UrlsCommentsRequest
 * @package App\Http\Requests
 */
class UrlsCommentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'urls' => [
                'required',
                function ($attribute, $value, $fail) {
                    $urls = explode(PHP_EOL, $value);
                    foreach ($urls as $url) {
                        $url = trim($url);
                        if (! filter_var($url, FILTER_VALIDATE_URL)) {
                            $fail($attribute . ' is invalid.');
                        }
                    }
                }
            ],
            'comment' => ['required', 'string', 'max:140'],
        ];
    }
}
