<?php

namespace App\Http\Requests\Group;

use App\Http\Requests\ProjectTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PublishRequest
 * @package App\Http\Requests\Group
 */
class PublishRequest extends FormRequest
{
    use ProjectTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();
        /** @var \App\Models\Group $group */
        $group = $this->route('group');

        return \Gate::allows('group.update', [$project, $group]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'publish' => [
                'required',
                'boolean',
            ],
        ];
    }
}
