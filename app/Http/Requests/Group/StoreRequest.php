<?php

namespace App\Http\Requests\Group;

use App\Http\Requests\ProjectTrait;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Group
 */
class StoreRequest extends AbstractRequest
{
    use ProjectTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();

        return \Gate::allows('group.create', [$project]);
    }
}
