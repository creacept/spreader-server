<?php

namespace App\Http\Requests\Group;

use App\Http\Requests\ProjectTrait;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Group
 */
class UpdateRequest extends AbstractRequest
{
    use ProjectTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();
        /** @var \App\Models\Group $group */
        $group = $this->route('group');

        return \Gate::allows('group.update', [$project, $group]);
    }
}
