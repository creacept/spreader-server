<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Response;

/**
 * Class CheckEmailRequest
 * @package App\Http\Requests\Api
 */
class CheckEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'auth.url' => [
                'required',
                'string',
                'url',
            ],
            'auth.password' => [
                'required',
                'string',
            ],
            'data.email' => [
                'required',
                'string',
                'email',
            ],
            'data.date' => [
                'required',
                'string',
                'date',
            ],
            'data.form_name' => [
                'required',
                'string',
            ],
            'data.page_url' => [
                'nullable',
                'string',
                'url',
            ],
            'data.page_title' => [
                'nullable',
                'string',
            ],
            'utm_source' => [
                'nullable',
                'string',
            ],
            'utm_content' => [
                'nullable',
                'string',
            ],
            'utm_campaign' => [
                'nullable',
                'string',
            ],
            'utm_medium' => [
                'nullable',
                'string',
            ],
            'sub_id' => [
                'nullable',
                'string',
            ],
        ];
    }

    /**
     * @param \Illuminate\Contracts\Validation\Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $response = Response::json($validator->errors(), 422);
        throw new HttpResponseException($response);
    }
}
