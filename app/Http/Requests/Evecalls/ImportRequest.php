<?php

namespace App\Http\Requests\Evecalls;

use App\Http\Requests\ProjectTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ImportRequest extends FormRequest
{
    use ProjectTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = $this->getProject();

        return \Gate::allows('evecalls-import.create', [$project]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $project = $this->getProject();

        return [
            'lead_name' => [
                'nullable',
                'string',
            ],
            'file' => [
                'required',
                'file',
                'mimes:xls,xlsx',
                'mimetypes:application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ],
        ];
    }
}
