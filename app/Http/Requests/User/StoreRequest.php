<?php

namespace App\Http\Requests\User;

/**
 * Class StoreRequest
 * @package App\Http\Requests\User
 */
class StoreRequest extends AbstractRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('user.create');
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['email'][] = 'unique:users,email';

        $password = $rules['password'];
        array_unshift($password, 'required');
        $rules['password'] = $password;

        return $rules;
    }
}
