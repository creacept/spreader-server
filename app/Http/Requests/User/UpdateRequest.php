<?php

namespace App\Http\Requests\User;

use App\Models\User;
use Illuminate\Validation\Rule;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\User
 */
class UpdateRequest extends AbstractRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        /** @var \App\Models\User $user */
        $user = $this->route('user');

        return \Gate::allows('user.update', [$user]);
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        /** @var User $user */
        $user = $this->route('user');

        $rules['email'][] = Rule::unique('users', 'email')->ignore($user->id);

        $password = $rules['password'];
        array_unshift($password, 'nullable');
        $rules['password'] = $password;

        if ($user->super_admin && $user->publish) {
            $countPublishSuperAdmin = $this->countPublishSuperAdmin();
            if ($countPublishSuperAdmin === 1) {
                unset($rules['super_admin']);
                unset($rules['publish']);
            }
        }

        return $rules;
    }
}
