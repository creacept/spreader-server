<?php

namespace App\Http\Requests\User;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AbstractRequest
 * @package App\Http\Requests\User
 */
abstract class AbstractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $permissions = $this->getPermissions();

        $rules = [
            'name' => [
                'required',
                'string',
            ],
            'email' => [
                'bail',
                'required',
                'string',
                'email',
            ],
            'password' => [
                'string',
                'min:16',
            ],
            'publish' => [
                'required',
                'boolean',
            ],
            'permissions' => [
                'nullable',
                'array',
            ],
            'permissions.*' => [
                'string',
                function ($attribute, $value, $fail) use ($permissions) {
                    if (! in_array($value, $permissions)) {
                        return $fail('Invalid permission');
                    }

                    return null;
                },
            ],
        ];

        $isSuperAdmin = $this->isSuperAdmin();
        if ($isSuperAdmin) {
            $rules['super_admin'] = [
                'required',
                'boolean',
            ];
        }

        return $rules;
    }

    /**
     * @return bool
     */
    protected function isSuperAdmin()
    {
        $user = \Auth::user();
        $isSuperAdmin = ($user instanceof User && $user->super_admin);
        return $isSuperAdmin;
    }

    /**
     * @return int
     */
    protected function countPublishSuperAdmin()
    {
        return User::whereSuperAdmin(true)->wherePublish(true)->count();
    }

    /**
     * @return array
     */
    private function getPermissions()
    {
        $permissions = [
            'project.create',
            'project.update',
            'project.delete',

            'user.list',
            'user.create',
            'user.update',
            'user.delete',

            'crm.list',
            'crm.create',
            'crm.update',
            'crm.delete',
            'crm.settings',

            'authorization',
            'horizon',

            'urlscomment.create',
            'urlscomment.update',
            'urlscomment.delete',
            'urlscomment.list',
        ];

        $projects = Project::all();
        foreach ($projects as $project) {
            $permissions = array_merge($permissions, [
                "project_{$project->id}.manager.list",
                "project_{$project->id}.manager.create",
                "project_{$project->id}.manager.update",
                "project_{$project->id}.manager.delete",

                "project_{$project->id}.group.list",
                "project_{$project->id}.group.create",
                "project_{$project->id}.group.update",
                "project_{$project->id}.group.delete",

                "project_{$project->id}.site.list",
                "project_{$project->id}.site.create",
                "project_{$project->id}.site.update",
                "project_{$project->id}.site.delete",

                "project_{$project->id}.rule.list",
                "project_{$project->id}.rule.create",
                "project_{$project->id}.rule.update",
                "project_{$project->id}.rule.delete",

                "project_{$project->id}.application.list",
                "project_{$project->id}.application.view",

                "project_{$project->id}.import.list",
                "project_{$project->id}.import.create",
                "project_{$project->id}.import.view",

                "project_{$project->id}.evecalls-import.list",
                "project_{$project->id}.evecalls-import.create",
                "project_{$project->id}.evecalls-import.view",
            ]);
        }

        return $permissions;
    }
}
