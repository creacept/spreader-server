<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 22.10.2018
 * Time: 12:28
 */

namespace App\Http\Requests;

use App\Models\Project;

/**
 * Trait ProjectTrait
 * @package App\Http\Requests
 */
trait ProjectTrait
{
    /**
     * @return Project
     */
    private function getProject()
    {
        /** @var \Illuminate\Foundation\Http\FormRequest $this */
        $route = \Route::getCurrentRoute();
        $project = $route->parameter('project');
        if (! $project instanceof Project) {
            throw new \RuntimeException('Current route does not contain the project');
        }
        return $project;
    }
}