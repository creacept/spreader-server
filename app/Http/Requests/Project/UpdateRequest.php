<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 19.10.2018
 * Time: 17:19
 */

namespace App\Http\Requests\Project;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Project
 */
class UpdateRequest extends AbstractRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        /** @var \App\Models\Project $project */
        $project = $this->route('project');

        return \Gate::allows('project.update', [$project]);
    }
}