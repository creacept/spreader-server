<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 19.10.2018
 * Time: 17:18
 */

namespace App\Http\Requests\Project;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Project
 */
class StoreRequest extends AbstractRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('project.create');
    }
}