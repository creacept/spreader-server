<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AbstractRequest
 * @package App\Http\Requests\Project
 */
abstract class AbstractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
            ],
            'crm_id' => [
                'bail',
                'required',
                'integer',
                'exists:crm,id',
            ],
            'contact_identity' => [
                'required',
                'integer',
            ],
            'lead_identity' => [
                'required',
                'integer',
            ],
            'publish' => [
                'required',
                'boolean',
            ],
            'moodle' => [
                'nullable',
                'array',
            ],
            'evecalls' => [
                'nullable',
                'array',
            ],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => '"Название"',
            'crm_id' => '"CRM"',
            'contact_identity' => '"Идентификатор контакта"',
            'lead_identity' => '"Идентификатор лида"',
            'moodle' => '"Параметры Moodle"',
            'evecalls' => '"Параметры EVE.calls"',
        ];
    }
}
