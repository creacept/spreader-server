<?php
/**
 * Created by PhpStorm.
 * User: sokolov_nn
 * Date: 28.07.2018
 * Time: 11:34
 */

namespace App\Http\ViewComposers;

use App\Models\Application;
use Illuminate\View\View;

/**
 * Class ApplicationComposer
 * @package App\Http\ViewComposers
 */
class ApplicationComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view) : void
    {
        $view->with('statusHelper', function (Application $application) : string {
            return $this->statusToText($application->status);
        });
    }

    /**
     * @param int $status
     * @return string
     */
    private function statusToText(int $status) : string
    {
        $text = 'Неизвестный';

        if ($status === Application::RAW_STATUS) {
            $text = 'Новый';
        } elseif ($status === Application::CONFIRMED_STATUS) {
            $text = 'Подтверждён';
        } elseif ($status === Application::SUCCESS_STATUS) {
            $text = 'Обработан';
        }

        return $text;
    }
}