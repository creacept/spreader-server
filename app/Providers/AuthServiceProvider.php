<?php

namespace App\Providers;

use App\Models\User;
use App\Policies;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Horizon\Horizon;

/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
//        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function (User $user) {
            if ($user->super_admin) {
                return true;
            }

            return null;
        });

        Gate::resource('project', Policies\ProjectPolicy::class);

        Gate::define('manager.list', 'App\Policies\ManagerPolicy@list');
        Gate::resource('manager', Policies\ManagerPolicy::class);

        Gate::define('group.list', 'App\Policies\GroupPolicy@list');
        Gate::resource('group', Policies\GroupPolicy::class);

        Gate::define('site.list', 'App\Policies\SitePolicy@list');
        Gate::resource('site', Policies\SitePolicy::class);

        Gate::define('rule.list', 'App\Policies\RulePolicy@list');
        Gate::resource('rule', Policies\RulePolicy::class);

        Gate::define('application.list', 'App\Policies\ApplicationPolicy@list');
        Gate::resource('application', Policies\ApplicationPolicy::class);

        Gate::define('user.list', 'App\Policies\UserPolicy@list');
        Gate::resource('user', Policies\UserPolicy::class);

        Gate::define('authorization', function (User $user) {
            return in_array('authorization', $user->permissions);
        });

        Gate::define('horizon', function (User $user) {
            return in_array('horizon', $user->permissions);
        });

        Gate::define('import.list', 'App\Policies\ImportPolicy@list');
        Gate::resource('import', Policies\ImportPolicy::class);

        Gate::define('crm.list', 'App\Policies\CRMPolicy@list');
        Gate::define('crm.settings', 'App\Policies\CRMPolicy@settings');
        Gate::resource('crm', Policies\CRMPolicy::class);

        Gate::define('urlscomment.list', 'App\Policies\UrlsCommentPolicy@list');
        Gate::define('urlscomment.add', 'App\Policies\UrlsCommentPolicy@create');
        Gate::define('urlscomment.update', 'App\Policies\UrlsCommentPolicy@update');
        Gate::define('urlscomment.delete', 'App\Policies\UrlsCommentPolicy@delete');

        Gate::resource('evecalls-import', Policies\EveCalls\ImportPolicy::class);

        Horizon::auth(function () {
            return Gate::allows('horizon');
        });
    }
}
