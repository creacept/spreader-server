<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Psr\Container\ContainerInterface;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Helpers\Service\WebhookService::class);
        $this->app->singleton(\App\Helpers\Service\ManagerResolverService::class);
        $this->app->singleton(\App\Helpers\Bitrix\ClientFactory::class);
        $this->app->singleton(\App\Helpers\Bitrix\WebhookConfig::class);
        $this->app->singleton(\App\Helpers\Authorization\AuthorizationService::class);

        $this->app->singleton(\App\Services\ExternalApplications\ExternalApplicationsCollector::class);
        $this->app->singleton(\App\Services\ExternalApplications\Sources\Bitrix24Telephony::class);
        $this->app->singleton(\App\Services\ExternalApplications\Sources\Bitrix24CrmForm::class);

        $this->app->singleton(
            \App\Helpers\Authorization\StorageFactory::class,
            function (ContainerInterface $container) {
                /** @var \Illuminate\Filesystem\FilesystemManager $filesystem */
                $filesystem = $container->get(\Illuminate\Filesystem\FilesystemManager::class);
                $disk = $filesystem->disk('authorization');
                return new \App\Helpers\Authorization\StorageFactory($disk);
            }
        );

        $this->app->singleton(\App\Helpers\Service\MoodleService::class, function ($app, $params) {
            return new \App\Helpers\Service\MoodleService(
                $params['baseUrl'],
                $params['accessToken']
            );
        });

        $this->app->singleton(
            'validator.application',
            function () {
                $validator = \Illuminate\Support\Facades\Validator::make(
                    [],
                    [
                        'id' => [
                            'required',
                            'integer',
                        ],
                        'submitted_at' => [
                            'nullable',
                            'date',
                        ],
                        'form_name' => [
                            'required',
                            'string',
                        ],
                        'sender_phone' => [
                            'nullable',
                            'string',
                        ],
                        'sender_name' => [
                            'nullable',
                            'string',
                        ],
                        'sender_email' => [
                            'required',
                            'string',
                            'email',
                        ],
                        'sender_vk' => [
                            'nullable',
                            'string',
                        ],
                        'page_url' => [
                            'nullable',
                            'string',
                            'url',
                        ],
                        'page_title' => [
                            'nullable',
                            'string',
                        ],
                        'utm_source' => [
                            'nullable',
                            'string',
                        ],
                        'utm_content' => [
                            'nullable',
                            'string',
                        ],
                        'utm_campaign' => [
                            'nullable',
                            'string',
                        ],
                        'utm_medium' => [
                            'nullable',
                            'string',
                        ],
                        'sub_id' => [
                            'nullable',
                            'string',
                        ],
                        'comment' => [
                            'nullable',
                            'string',
                        ],
                        'extra_fields' => [
                            'nullable',
                            'string',
                        ],
                        'lead_extra_fields' => [
                            'nullable',
                            'array',
                        ],
                    ],
                    [],
                    [
                        'id' => 'ID заявки',
                        'submitted_at' => 'Дата отправки',
                        'form_name' => 'Название лида',
                        'sender_phone' => 'Телефон',
                        'sender_name' => 'ФИО',
                        'sender_email' => 'E-mail',
                        'sender_vk' => 'ВКонтакте',
                        'page_url' => 'URL страницы',
                        'page_title' => 'Заголовок страницы',
                        'utm_source' => 'utm_source',
                        'utm_campaign' => 'utm_campaign',
                        'utm_content' => 'utm_content',
                        'utm_medium' => 'utm_medium',
                        'sub_id' => 'sub_id',
                        'comment' => 'Комментарий',
                        'extra_fields' => 'Доп. поля',
                        'lead_extra_fields' => 'Доп. поля лида',
                    ]
                );

                return $validator;
            }
        );
    }
}
