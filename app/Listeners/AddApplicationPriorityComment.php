<?php

namespace App\Listeners;

use App\Events\ApplicationStored;
use App\Models\UrlsComment;
use Throwable;

class AddApplicationPriorityComment
{
    /**
     * Handle the event.
     *
     * @param  ApplicationStored  $event
     * @return void
     */
    public function handle(ApplicationStored $event)
    {
        $application = $event->application;
        $url = $application->page_url;

        $comment = UrlsComment::whereJsonContains('urls', $url)->first();
        if (! $comment instanceof UrlsComment) {
            return;
        }

        try {
            $application->comment = $comment['comment'];
            $application->saveOrFail();
        } catch (Throwable $e) {
            report($e);
        }
    }
}
