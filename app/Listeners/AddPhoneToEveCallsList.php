<?php

namespace App\Listeners;

use App\Events\ApplicationStored;
use App\Models\Application;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

/**
 * Class AddPhoneToEveCallsList
 * @package App\Listeners
 */
class AddPhoneToEveCallsList
{
    /**
     * @var int
     */
    private const RU_LIST_ID = 31568;

    /**
     * @var int
     */
    private const UA_LIST_ID = 31570;

    private const EVECALLS_REQUEST_URL = 'https://api.evecalls.com/updateListUsers';

    /**
     * @var string[]
     */
    private const URLS_LIST = [
        'https://sniperfx.ru/sniperx-new',
        'https://sniperfx.ru/sale-sniperx',
        'https://sniperfx.ru/sniperx-free',
        'https://sniperx.academyfx.ru',
        'http://sniperfx.pro',
        'https://basic.academyfx.ru',
        'https://fxpriceaction.ru/sniper-new',
        'https://fxpriceaction.ru/obuchenie',
    ];

    /**
     * Handle the event.
     *
     * @param \App\Events\ApplicationStored $event
     * @return void
     */
    public function handle(ApplicationStored $event)
    {
        /** @var \App\Models\Application $application */
        $application = $event->application;

        if (! in_array($application->page_url, self::URLS_LIST)) {
            return;
        }

        try {
            $phoneUtil = PhoneNumberUtil::getInstance();
            $phone = $phoneUtil->parse($application->sender_phone, 'AUTO');
            $countryCode = $phoneUtil->getRegionCodeForNumber($phone);
        } catch (NumberParseException $e) {
            return;
        }

        switch ($countryCode) {
            case 'RU' :
                $listId = self::RU_LIST_ID;
                break;
            case 'UA' :
                $listId = self::UA_LIST_ID;
                break;
            default :
                $listId = 0;
        }
        $this->addToEveCallsList($application->sender_phone, $listId);

        try {
            $application->status = Application::SUCCESS_STATUS;
            $application->saveOrFail();
        } catch (\Throwable $e) {
            return;
        }
    }

    private function addToEveCallsList(Application $application, int $listId)
    {
        $site = $application->site()->first();
        $project = $site->load('project')->project;

        $apiKey = Arr::get($project->evecalls, 'api_key');
        if ($apiKey === null) {
            return;
        }

        $phone = $application->sender_phone;
        $contactName = $application->sender_name;

        $httpClient = new Client();
        $response = $httpClient->get(self::EVECALLS_REQUEST_URL, [
            'query' => [
                'key' => $apiKey,
                'id' => $listId,
                'users' => [
                    [
                        'phone' => $application->sender_phone,
                        'first_name' => $application->sender_name,
                    ],
                ],
            ]
        ]);
    }
}
