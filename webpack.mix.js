let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .scripts([
        'resources/assets/libs/jquery/dist/jquery.min.js',
        'resources/assets/libs/popper/popper.min.js',
        'resources/assets/libs/bootstrap/dist/js/bootstrap.min.js',
        'resources/assets/libs/select2/dist/js/select2.min.js',
        'resources/assets/libs/datetimepicker/js/moment.min.js',
        'resources/assets/libs/datetimepicker/js/daterangepicker.js',
        'resources/assets/libs/jquery-ui/jquery-ui.min.js',
        'resources/assets/js/common.js'
    ], 'public/js/scripts.min.js')
    .sass('resources/assets/sass/main.sass', 'public/css/main.min.css')
    .copy('resources/assets/fonts', 'public/fonts', false)
    .copy('resources/assets/img', 'public/img', false)
    .version();
