<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** @noinspection PhpUndefinedMethodInspection */
Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index')
        ->name('home');

    /** @noinspection PhpUndefinedMethodInspection */
    Route::name('authorization')
        ->prefix('authorization')
        ->group(function () {
            Route::get('/', 'AuthorizationController@index');
            Route::get('{crm}', 'AuthorizationController@show')->name('.show');
            Route::get('{crm}/start', 'AuthorizationController@start')->name('.start');
            Route::get('{crm}/finish', 'AuthorizationController@finish')->name('.finish');
        });

    Route::resource('projects', 'ProjectController')
        ->except('show');
    Route::patch('projects/{project}/publish', 'ProjectController@publish')
        ->name('projects.publish');
    Route::get('projects/crm/{crm}', 'ProjectController@crm');

    Route::resource('projects.sites', 'SiteController')
        ->except('show');
    Route::patch('projects/{project}/sites/{site}/publish', 'SiteController@publish')
        ->name('projects.sites.publish');
    Route::match(['get', 'head'], 'projects/{project}/sites/import/create', 'SiteController@createImport')
        ->name('projects.sites.create_import');
    Route::post('projects/{project}/sites/import', 'SiteController@storeImport')
        ->name('projects.sites.store_import');
    Route::match(['get', 'head'], 'projects/{project}/sites/import/{site}/edit', 'SiteController@editImport')
        ->name('projects.sites.edit_import');
    Route::match(['put', 'patch'], 'projects/{project}/sites/import/{site}', 'SiteController@updateImport')
        ->name('projects.sites.update_import');

    Route::resource('projects.groups', 'GroupController')
        ->except('show');
    Route::patch('projects/{project}/groups/{group}/publish', 'GroupController@publish')
        ->name('projects.groups.publish');

    Route::resource('projects.applications', 'ApplicationController')
        ->only('index', 'show');

    Route::resource('projects.managers', 'ManagerController')
        ->except('show');
    Route::patch('projects/{project}/managers/{manager}/publish', 'ManagerController@publish')
        ->name('projects.managers.publish');

    Route::resource('sites.rules', 'RuleController')
        ->except('show');
    Route::patch('sites/{site}/rules/{rule}/publish', 'RuleController@publish')
        ->name('sites.rules.publish');
    Route::patch('sites/{site}/rules/{rule}/reorder', 'RuleController@reorder')
        ->name('sites.rules.reorder');

    Route::resource('users', 'UserController')
        ->except('show');
    Route::patch('users/{user}/publish', 'UserController@publish')
        ->name('users.publish');

    Route::get('projects/{project}/imports/pattern', 'ImportController@pattern')
        ->name('projects.imports.pattern');
    Route::resource('projects.imports', 'ImportController')
        ->except(['edit', 'update', 'destroy']);

    Route::resource('crm', 'CRMController')
        ->except('show');
    Route::get('crm/{crm}/settings', 'CRMController@editSettings')
        ->name('crm.edit_settings');
    Route::post('crm/{crm}/settings', 'CRMController@updateSettings')
        ->name('crm.save_settings');

    Route::get('urls-comments', 'UrlsCommentsController@list')
        ->name('urls-comments.index');
    Route::get('urls-comments/add', 'UrlsCommentsController@add')
        ->name('urls-comments.add');
    Route::post('urls-comments/add', 'UrlsCommentsController@store')
        ->name('urls-comments.store');
    Route::get('urls-comments/{urlsComment}/edit', 'UrlsCommentsController@editRecord')
        ->name('urls-comments.edit');
    Route::post('urls-comments/{urlsComment}/edit', 'UrlsCommentsController@editRecordPost')
        ->name('urls-comments.update');
    Route::delete('urls-comments/{urlsComment}/delete', 'UrlsCommentsController@deleteRecord')
        ->name('urls-comments.delete');

    Route::resource('projects.evecalls-imports', 'EveCalls\ImportController')
        ->except(['edit', 'update', 'destroy']);
});

Route::prefix('webhook')->group(function () {
    Route::post('contact-add', 'WebhookController@onContactAdded')
        ->name('webhook.contact.add');
    Route::post('survey', 'WebhookController@onSurveyReceived')
        ->name('webhook.survey');
});
