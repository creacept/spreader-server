(function ($) {
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.status-change.ajax+label').on('click', function (event) {
            event.preventDefault();

            let label = this;
            let checkbox = $('#' + $(label).prop('for'));
            let value = ! $(checkbox).prop('checked');

            $.ajax({
                url: $(checkbox).data('url'),
                type: 'POST',
                data: {
                    _method: 'patch',
                    publish: (value ? 1 : 0),
                },
                beforeSend: function () {
                    $(checkbox).prop('disabled', true);
                },
                success: function (data) {
                    if (typeof(data) !== 'object' || ! data.hasOwnProperty('result') || data.result !== false) {
                        $(checkbox).prop('checked', value);
                    }

                    if (typeof(data) === 'object') {
                        if (data.hasOwnProperty('message')) {
                            alert(data.message);
                        }

                        if (data.hasOwnProperty('redirect')) {
                            window.location.href = data.redirect;
                        }
                    }
                },
                error: function (jqXHR) {
                    if (jqXHR.status === 403) {
                        alert('У вас нет прав для изменения состояния "' + $(label).data('name') + '".')
                    } else {
                        alert('Произошла непредвиденная ошибка при изменении "' + $(label).data('name') + '". Попробуйте ещё раз.');
                    }
                },
                complete: function () {
                    $(checkbox).prop('disabled', false);
                }
            });
        });

        $(document).on('keypress', ':input:not(textarea):not([type="submit"])', function (event) {
            if (event.keyCode === 13) {
                event.preventDefault();
            }
        });

        $('#admin-form').on('submit', function (e) {
            e.preventDefault();

            let form = this;
            let button = $(form).find('[type="submit"]');

            $.ajax({
                url: $(form).prop('action'),
                type: $(form).prop('method'),
                data: new FormData(form),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $(button).prop('disabled', true);
                },
                success: function (data) {
                    $(form).find('.is-invalid').removeClass('is-invalid');
                    $(form).find('.invalid-feedback').remove();

                    if (typeof(data) !== 'object') {
                        return;
                    }

                    if (data.hasOwnProperty('message')) {
                        alert(data.message);
                    }

                    if (data.hasOwnProperty('redirect')) {
                        window.location.href = data.redirect;
                    }
                },
                error: function (jqXHR) {
                    if (jqXHR.status === 403) {
                        alert('У вас нет прав для обработки формы.')
                    } else if (jqXHR.status === 422) {
                        $(form).find('.is-invalid').removeClass('is-invalid');
                        $(form).find('.invalid-feedback').remove();

                        $.each(jqXHR.responseJSON.errors, function (name, errors) {
                            let nameList = name.split('.');
                            name = nameList.shift();
                            for (let i = 0; i < nameList.length; i++) {
                                name += '[' + nameList[i] + ']';
                            }
                            let input = $(form).find('[name="' + name + '"][id]').first();

                            if ($(input).hasClass('status-change')) {
                                $('[for="' + $(input).prop('id') + '"].form-control').addClass('is-invalid');
                            } else {
                                $(input).addClass('is-invalid');
                            }

                            let feedback = $('<div class="invalid-feedback"></div>');
                            $.each(errors, function () {
                                $(feedback).append(this + '<br>');
                            });
                            $(input).parent().append(feedback);
                        });
                    } else {
                        alert("Произошла непредвиденная ошибка. Попробуйте отправить форму ещё раз.");
                    }
                },
                complete: function () {
                    $(button).prop('disabled', false);
                }
            });
        });

        $('#admin-form [name]').on('change', function () {
            let input = this;
            if ($(input).hasClass('status-change')) {
                $('[for="' + $(input).prop('id') + '"].form-control').removeClass('is-invalid');
            } else {
                $(input).removeClass('is-invalid');
            }
            $(input).parent().find('.invalid-feedback').remove();

            let name = $(input).prop('name');
            let length = name.indexOf('[');
            if (length === -1) {
                return;
            }
            name = name.substr(0, length);

            input = $('#admin-form').find('[name="' + name + '"][id]').first();
            $(input).removeClass('is-invalid');
            $(input).parent().find('.invalid-feedback').remove();
        });

        $('.ajax-delete').on('click', function (event) {
            event.preventDefault();

            let button = this;
            if (! confirm('Вы действительно хотите удалить "' + $(button).data('name') +'" ?')) {
                return;
            }

            $.ajax({
                url: $(button).data('url'),
                method: 'POST',
                data: {
                    _method: 'delete'
                },
                beforeSend: function () {
                    $(button).prop('disabled', true);
                },
                success: function (data) {
                    let target = $(button).data('target');
                    $(target).first().remove();

                    if (typeof(data) === 'object') {
                        if (data.hasOwnProperty('message')) {
                            alert(data.message);
                        }

                        if (data.hasOwnProperty('redirect')) {
                            window.location.href = data.redirect;
                        }
                    }
                },
                error: function (jqXHR) {
                    if (jqXHR.status === 403) {
                        alert('У вас нет прав для удаления "' + $(button).data('name') + '".')
                    } else {
                        alert('Произошла непредвиденная ошибка при удалении "' + $(button).data('name') + '". Попробуйте ещё раз.');
                    }
                },
                complete: function () {
                    $(button).prop('disabled', false);
                }
            });
        });

        (function () {
            let startIndex;
            $('#rule-sortable').sortable({
                axis: 'y',
                containment: '.content-center',
                cursor: 'move',
                handle: '.rule-sortable-handle',
                start: function (event, ui) {
                    let element = ui.item[0];
                    startIndex = $('#rule-sortable tr').index(element);
                },
                stop: function (event, ui) {
                    let element = ui.item[0];
                    let stopIndex = $('#rule-sortable tr').index(element);
                    let offset = stopIndex - startIndex;
                    if (offset === 0) {
                        return;
                    }

                    $.ajax({
                        url: $(element).data('url'),
                        method: 'POST',
                        data: {
                            _method: 'patch',
                            offset: offset
                        },
                        success: function (data) {
                            if (typeof(data) !== 'object') {
                                return;
                            }

                            if (data.hasOwnProperty('message')) {
                                alert(data.message);
                            }

                            if (data.hasOwnProperty('redirect')) {
                                window.location.href = data.redirect;
                            }
                        },
                        error: function (jqXHR) {
                            if (jqXHR.status === 403) {
                                alert('У вас нет прав для изменения порядка.')
                            } else {
                                location.reload(true);
                            }
                        }
                    });
                }
            });
        })();

        $('#login-form').on('submit', function (e) {
            e.preventDefault();

            let form = this;
            let button = $(form).find('[type="submit"]');

            $.ajax({
                url: $(form).prop('action'),
                type: $(form).prop('method'),
                data: $(form).serializeArray(),
                beforeSend: function () {
                    $(button).prop('disabled', true);
                },
                success: function (data) {
                    $(form).find('.is-invalid').removeClass('is-invalid');
                    $(form).find('.invalid-feedback').remove();

                    window.location.href = data.redirect;
                },
                error: function (jqXHR) {
                    if (jqXHR.status === 422) {
                        $(form).find('.is-invalid').removeClass('is-invalid');
                        $(form).find('.invalid-feedback').remove();

                        $.each(jqXHR.responseJSON.errors, function (name, errors) {
                            let input = $(form).find('[name="' + name + '"][id]').first();
                            $(input).addClass('is-invalid');
                            let feedback = $('<div class="invalid-feedback"></div>');
                            $.each(errors, function () {
                                $(feedback).append(this + '<br>');
                            });
                            $(input).parent().append(feedback);
                        });
                    } else {
                        alert("Произошла непредвиденная ошибка. Попробуйте отправить форму ещё раз.");
                    }
                },
                complete: function () {
                    $(button).prop('disabled', false);
                }
            });
        });

        $('#login-form [name]').on('change', function () {
            let input = this;
            $(input).removeClass('is-invalid');
            $(input).parent().find('.invalid-feedback').remove();
        });

        $('#logout').on('click', function () {
            let button = this;
            if (! confirm('Вы действительно хотите выйти?')) {
                return;
            }

            $.ajax({
                url: $(button).data('url'),
                method: 'POST',
                beforeSend: function () {
                    $(button).prop('disabled', true);
                },
                success: function (data) {
                    window.location.href = data.redirect;
                },
                error: function () {
                    alert('Произошла непредвиденная ошибка при выходе. Попробуйте ещё раз.');
                },
                complete: function () {
                    $(button).prop('disabled', false);
                }
            });
        });

        $('input[type="radio"].depending-master').on('change', function () {
            var depending = $(this).data('depending');
            var value = $(this).data('depending-value');

            $('.depending-slave[data-depending="' + depending + '"]')
                .not('[data-depending-value="' + value + '"]')
                .addClass('d-none');

            $('.depending-slave[data-depending="' + depending + '"][data-depending-value="' + value + '"]')
                .removeClass('d-none');
        });

        $('#admin-form.project-form [name="crm_id"]').on('change', function () {
            var crmSelect = this;
            var contactIdentitySelect = $('#admin-form.project-form [name="contact_identity"]');
            var leadIdentitySelect = $('#admin-form.project-form [name="lead_identity"]');

            $(contactIdentitySelect).prop('disabled', true);
            $(contactIdentitySelect).find('option')
                .filter(function () {
                    return ($(this).val() !== '');
                })
                .remove();

            $(leadIdentitySelect).prop('disabled', true);
            $(leadIdentitySelect).find('option')
                .filter(function () {
                    return ($(this).val() !== '');
                })
                .remove();

            var id = $(crmSelect).val();
            if (id === '') {
                return;
            }

            $.ajax({
                url: '/projects/crm/' + encodeURI(id),
                method: 'GET',
                beforeSend: function () {
                    $(crmSelect).prop('disabled', true);

                },
                success: function (response) {
                    var contactIdentities = response['contactIdentities'];
                    contactIdentities.forEach(function (element) {
                        $('<option></option>')
                            .val(element['id'])
                            .text(element['text'])
                            .appendTo(contactIdentitySelect);
                    });
                    $(contactIdentitySelect).prop('disabled', false);

                    var leadIdentities = response['leadIdentities'];
                    leadIdentities.forEach(function (element) {
                        $('<option></option>')
                            .val(element['id'])
                            .text(element['text'])
                            .appendTo(leadIdentitySelect);
                    });
                    $(leadIdentitySelect).prop('disabled', false);
                },
                error: function () {
                    alert('Произошла непредвиденная ошибка при выходе. Попробуйте ещё раз.');
                },
                complete: function () {
                    $(crmSelect).prop('disabled', false);
                },
            });
        });

        $('[data-toggle="popover"]').popover();

        $('.show-more-filters').on('click', function (e) {
            e.preventDefault();
            let target = $($(this).data('target'));
            target.slideToggle();
        });

        $('.select2').select2({
            width: '100%'
        });

        $('.select-pagination').select2({
            width: '80px'
        });

        $('.daterange').daterangepicker({
            autoApply: true
        });
    });
})(jQuery);
