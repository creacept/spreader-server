<?php
/** @var \App\Models\Project $project */
/** @var \Illuminate\Support\Collection $bitrix24Users */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Менеджеры@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item active" aria-current="page">Менеджеры</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Список менеджеров</h3>
        @can('manager.create', [$project])
            <a href="{{ route('projects.managers.create', ['project' => $project]) }}" class="btn btn-primary">Импортировать менеджера</a>
        @else
            <span class="btn btn-secondary disabled">Импортировать менеджера</span>
        @endcan
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Состояние</th>
                <th scope="col">Имя</th>
                <th scope="col">Группы</th>
                <th scope="col">Пропорция распределения</th>
                <th scope="col">Количество заявок</th>
                <th scope="col">ID</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
                @foreach($bitrix24Users as $data)
                    <?php /** @var \App\Models\Manager $manager */ ?>
                    @php $manager = $data['manager']; @endphp
                    <tr id="manager-{{ $manager->id }}">
                        <td>
                            @can('manager.update', [$project, $manager])
                                <input type="checkbox"
                                       id="manager-publish-{{ $manager->id }}"
                                       class="status-change ajax"
                                       data-url="{{ route('projects.managers.publish', ['project' => $project, 'manager' => $manager]) }}"
                                       @if($manager->publish) checked @endif>
                                <label class="ajax" for="manager-publish-{{ $manager->id }}" data-name="{{ $data['name'] }}"></label>
                            @else
                                <input type="checkbox"
                                       id="manager-publish-{{ $manager->id }}"
                                       class="status-change"
                                       disabled
                                       @if($manager->publish) checked @endif>
                                <label for="manager-publish-{{ $manager->id }}"></label>
                            @endcan
                        </td>
                        <td>{{ $data['name'] }}</td>
                        <td>
                            @foreach($manager->groups as $group)
                                <span class="p-1 @if($group->publish) bg-primary @else bg-secondary @endif text-white">{{ $group->name }}</span>
                            @endforeach
                        </td>
                        <td>{{ $manager->ratio }}</td>
                        <td>{{ $manager->application_number }}</td>
                        <th scope="row">{{ $data['id'] }}</th>
                        <td>
                            <div class="btn-group" role="group">
                                @can('manager.update', [$project, $manager])
                                    <a href="{{ route('projects.managers.edit', ['project' => $project, 'manager' => $manager]) }}" class="btn btn-outline-primary">
                                        <i class="far fa-edit"></i>
                                    </a>
                                @else
                                    <span class="btn btn-outline-secondary disabled">
                                        <i class="far fa-edit"></i>
                                    </span>
                                @endcan

                                @can('manager.delete', [$project, $manager])
                                    <button type="button"
                                            class="btn btn-outline-danger ajax-delete"
                                            data-url="{{ route('projects.managers.destroy', ['project' => $project, 'manager' => $data['manager']]) }}"
                                            data-target="#manager-{{ $manager->id }}"
                                            data-name="{{ $data['name'] }}">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @else
                                    <button type="button" class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
