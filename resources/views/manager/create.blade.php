<?php
/** @var \App\Models\Project $project */
/** @var \Illuminate\Support\Collection $bitrix24Users */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Импорт менеджеров@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('manager.list', [$project])
            <a href="{{ route('projects.managers.index', ['project' => $project]) }}">Менеджеры</a>
        @else
            <span class="text-white-50">Менеджеры</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Импорт менеджеров</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Импорт менеджеров</h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('projects.managers.store', ['project' => $project]) }}" method="post">
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Пользователи Битрикс24</legend>
                    <div class="col-sm-10">
                        @foreach($bitrix24Users as $data)
                            <div class="form-check">
                                <input type="checkbox"
                                       name="bitrix24_user_id[{{ $data['id'] }}]"
                                       value="{{ $data['id'] }}"
                                       id="bitrix24_user_id-{{ $data['id'] }}-input"
                                       class="form-check-input"
                                       @if($data['exists']) checked disabled @endif>
                                <label class="form-check-label" for="bitrix24_user_id-{{ $data['id'] }}-input">{{ $data['name'] }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </fieldset>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Импортировать</button>
                    @can('manager.list', [$project])
                        <a href="{{ route('projects.managers.index', ['project' => $project]) }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection
