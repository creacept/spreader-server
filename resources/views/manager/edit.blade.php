<?php
/** @var \App\Models\Project $project */
/** @var \App\Models\Manager $manager */
/** @var \ */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Управление менеджером@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('manager.list', [$project])
            <a href="{{ route('projects.managers.index', ['project' => $project]) }}">Менеджеры</a>
        @else
            <span class="text-white-50">Менеджеры</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Управление менеджером</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>
            Управление менеджером &quot;<strong>{{ $managerName }}</strong>&quot;
        </h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('projects.managers.update', ['project' => $project, 'manager' => $manager]) }}" method="post">
            @method('put')
            <div class="form-group row">
                <label for="ratio-input" class="col-sm-2 col-form-label">Пропорция распределения</label>
                <div class="col-sm-10">
                    <input name="ratio" type="number" value="{{ $manager->ratio }}" min="1" id="ratio-input" class="form-control" placeholder="Введите новую пропорцию распределения">
                </div>
            </div>
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Группы</legend>
                    <div class="col-sm-10">
                        @foreach($groups as $group)
                            <div class="form-check">
                                <input type="checkbox"
                                       name="group_id[{{ $group->id }}]"
                                       value="{{ $group->id }}"
                                       id="group_id-{{ $group->id }}-input"
                                       class="form-check-input"
                                       @if($manager->groups->contains($group)) checked @endif>
                                <label class="form-check-label @if($group->publish) text-primary @else text-secondary @endif"
                                       for="group_id-{{ $group->id }}-input">{{ $group->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </fieldset>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="publish-input">Состояние</label>
                <div class="col-sm-10">
                    <input type="hidden" name="publish" value="0">
                    <input type="checkbox" name="publish" value="1" id="publish-input" class="status-change" @if($manager->publish) checked @endif>
                    <label class="form-control" for="publish-input"></label>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Применить</button>
                    @can('manager.list', [$project])
                        <a href="{{ route('projects.managers.index', ['project' => $project]) }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection
