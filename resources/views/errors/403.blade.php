@extends('errors::layout')

@section('title', 'Forbidden')

@section('message', 'You don\'t have permission')
