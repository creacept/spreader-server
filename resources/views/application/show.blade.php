<?php
/** @var \App\Models\Project $project */
/** @var \App\Models\Application $application */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Просмотр заявки@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('application.list', [$project])
            <a href="{{ route('projects.applications.index', ['project' => $project]) }}">Заявки</a>
        @else
            <span class="text-white-50">Заявки</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Просмотр заявки</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Заявка #{{ $application->id }}</h3>
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="w-25" scope="col">Параметр</th>
                    <th class="w-75">Значение</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="col">ID</th>
                    <td>{{ $application->id }}</td>
                </tr>
                <tr>
                    <th scope="col">Создан</th>
                    <td>{{ $application->created_at }}</td>
                </tr>
                <tr>
                    <th scope="col">Сайт</th>
                    <td>
                        @php $site = $application->site; @endphp
                        @if($site->trashed())
                            <s>{{ $site->name }}</s>
                        @else
                            <span class="p-1 @if($site->publish) bg-primary @else bg-secondary @endif text-white">{{ $site->name }}</span>
                        @endif

                        @if($site->type === \App\Models\Site::SITE_TYPE)
                            <span class="badge badge-success">Сайт</span>
                        @elseif($site->type === \App\Models\Site::IMPORT_TYPE)
                            <span class="badge badge-warning">Импорт</span>
                        @else
                            <span class="badge badge-danger">
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                    Неопределён
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                </span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th scope="col">Отправлен</th>
                    <td>{{ $application->submitted_at }}</td>
                </tr>
                <tr>
                    <th scope="col">Статус</th>
                    <td>{{ $statusHelper($application) }}</td>
                </tr>
                <tr>
                    <th scope="col">Название</th>
                    <td>{{ $application->form_name }}</td>
                </tr>
                <tr>
                    <th scope="col">Имя</th>
                    <td>{{ $application->sender_name }}</td>
                </tr>
                <tr>
                    <th scope="col">Телефон</th>
                    <td>{{ $application->sender_phone }}</td>
                </tr>
                <tr>
                    <th scope="col">E-mail</th>
                    <td>{{ $application->sender_email }}</td>
                </tr>
                <tr>
                    <th scope="col">ВКонтакте</th>
                    <td>{{ $application->sender_vk }}</td>
                </tr>
                <tr>
                    <th scope="col">URL страницы</th>
                    <td>{{ $application->page_url }}</td>
                </tr>
                <tr>
                    <th scope="col">Заголовок страницы</th>
                    <td>{{ $application->page_title }}</td>
                </tr>
                <tr>
                    <th scope="col">Комментарий</th>
                    <td>{!! $application->comment !!}</td>
                </tr>
                <tr>
                    <th scope="col">UTM SOURCE</th>
                    <td>{{ $application->utm_source }}</td>
                </tr>
                <tr>
                    <th scope="col">UTM CONTENT</th>
                    <td>{{ $application->utm_content }}</td>
                </tr>
                <tr>
                    <th scope="col">UTM CAMPAIGN</th>
                    <td>{{ $application->utm_campaign }}</td>
                </tr>
                <tr>
                    <th scope="col">UTM MEDIUM</th>
                    <td>{{ $application->utm_medium }}</td>
                </tr>
                <tr>
                    <th scope="col">SUB ID</th>
                    <td>{{ $application->sub_id }}</td>
                </tr>
                <tr>
                    <th scope="col">Дополнительные поля:</th>
                    <td>
                        @if($application->extra_fields)
                            @foreach ($application->extra_fields as $k => $extra_field)
                                <p>{{ $k }}: {{ $extra_field }}</p>
                            @endforeach
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
