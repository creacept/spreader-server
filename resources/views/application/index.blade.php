<?php
/** @var \App\Models\Project $project */
/** @var \Illuminate\Pagination\Paginator $paginator */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Заявки@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item active" aria-current="page">Заявки</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Список заявок</h3>
    </div>

    <div class="content-filter">
        <form action="">
            <div class="form-row">
                <div class="col">
                    <input type="text" class="form-control" placeholder="First name">
                </div>
                <div class="col">
                    <input type="date" class="form-control" placeholder="Date start">
                </div>
                <div class="col">
                    <input type="date" class="form-control" placeholder="Date end">
                </div>
                <div class="col">
                    <select class="select2" class="form-control">
                        <option value="">1</option>
                        <option value="">2</option>
                        <option value="">3</option>
                        <option value="">Надо найти</option>
                    </select>
                </div>
                <div class="col">
                    <button class="btn btn-primary btn-block" type="submit">Найти</button>
                </div>
                <div class="col">
                    <button class="btn btn-outline-primary btn-block show-more-filters"
                            data-target="#more-filter" type="button">еще фильтры
                    </button>
                </div>
            </div>
            <div id="more-filter" class="form-row-hidden">
                <div class="form-row">
                    <div class="col">
                        <input type="text" class="form-control daterange">
                    </div>
                    <div class="col">
                        <input type="date" class="form-control">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Что то найти">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Что то найти">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Что то найти">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Что то найти">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Создан</th>
                    <th scope="col">Сайт</th>
                    <th scope="col">Отправлен</th>
                    <th scope="col">Название</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($paginator->items() as $application)
                    <?php /** @var App\Models\Application $application */ ?>
                    <tr>
                        <th scope="row">{{ $application->id }}</th>
                        <td>{{ $application->created_at }}</td>
                        <td>
                            @php $site = $application->site; @endphp
                            @if($site->trashed())
                                <s>{{ $site->name }}</s>
                            @else
                                <span class="p-1 @if($site->publish) bg-primary @else bg-secondary @endif text-white">{{ $site->name }}</span>
                            @endif

                            @if($site->type === \App\Models\Site::SITE_TYPE)
                                <span class="badge badge-success">Сайт</span>
                            @elseif($site->type === \App\Models\Site::IMPORT_TYPE)
                                <span class="badge badge-warning">Импорт</span>
                            @else
                                <span class="badge badge-danger">
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                    Неопределён
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                </span>
                            @endif
                        </td>
                        <td>{{ $application->submitted_at }}</td>
                        <td>{{ $application->form_name }}</td>
                        <td>{{ $statusHelper($application) }}</td>
                        <td>
                            @can('application.view', [$project, $application])
                                <a href="{{ route('projects.applications.show', ['project' => $project, 'application' => $application]) }}" class="btn btn-outline-primary" target="_blank">
                                    <i class="far fa-eye"></i>
                                </a>
                            @else
                                <span class="btn btn-outline-secondary disabled">
                                    <i class="far fa-eye"></i>
                                </span>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $paginator->links() }}
@endsection