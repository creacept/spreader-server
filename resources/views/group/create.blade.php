<?php
/** @var \App\Models\Project $project */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Создание группы@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('group.list', [$project])
            <a href="{{ route('projects.groups.index', ['project' => $project]) }}">Группы</a>
        @else
            <span class="text-white-50">Группы</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Создание группы</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Создание группы</h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('projects.groups.store', ['project' => $project]) }}" method="post">
            <div class="form-group row">
                <label for="name-input" class="col-sm-2 col-form-label">Название</label>
                <div class="col-sm-10">
                    <input type="text" name="name" id="name-input" class="form-control" placeholder="Введите название группы">
                </div>
            </div>
            <div class="form-group row">
                <label for="ratio-input" class="col-sm-2 col-form-label">Пропорция распределения</label>
                <div class="col-sm-10">
                    <input type="number" name="ratio" min="1" id="ratio-input" class="form-control" placeholder="Введите пропорцию распределения">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="publish-input">Состояние</label>
                <div class="col-sm-10">
                    <input type="hidden" name="publish" value="0">
                    <input type="checkbox" name="publish" value="1" id="publish-input" class="status-change" disabled>
                    <label class="form-control" for="publish-input"></label>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Создать</button>
                    @can('group.list', [$project])
                        <a href="{{ route('projects.groups.index', ['project' => $project]) }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection
