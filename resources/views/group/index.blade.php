<?php
/** @var \App\Models\Project $project */
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Group[] $groups */
?>


@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Группы@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item active" aria-current="page">Группы</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Список групп</h3>
        @can('group.create', [$project])
            <a href="{{ route('projects.groups.create', ['project' => $project]) }}" class="btn btn-primary">Добавить группу</a>
        @else
            <span class="btn btn-secondary disabled">Добавить группу</span>
        @endcan
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Состояние</th>
                <th scope="col">Название</th>
                <th scope="col">Пропорция распределения</th>
                <th scope="col">Количество заявок</th>
                <th scope="col">ID</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
                @foreach($groups as $group)
                    <tr id="group-{{ $group->id }}">
                        <td>
                            @can('group.update', [$project, $group])
                                <input type="checkbox"
                                       id="group-publish-{{ $group->id }}"
                                       class="status-change ajax"
                                       data-url="{{ route('projects.groups.publish', ['project' => $project, 'group' => $group]) }}"
                                       @if ($group->managers_count === 0) disabled @endif
                                       @if ($group->publish) checked @endif>
                                <label @if ($group->managers_count > 0) class="ajax" @endif for="group-publish-{{ $group->id }}" data-name="{{ $group->name }}"></label>
                            @else
                                <input type="checkbox"
                                       id="group-publish-{{ $group->id }}"
                                       class="status-change"
                                       disabled
                                       @if ($group->publish) checked @endif>
                                <label for="group-publish-{{ $group->id }}"></label>
                            @endcan
                        </td>
                        <td>{{ $group->name }}</td>
                        <td>{{ $group->ratio }}</td>
                        <td>{{ $group->application_number }}</td>
                        <th scope="row">{{ $group->id }}</th>
                        <td>
                            <div class="btn-group" role="group">
                                @can('group.update', [$project, $group])
                                    <a href="{{ route('projects.groups.edit', ['project' => $project, 'group' => $group]) }}" class="btn btn-outline-primary">
                                        <i class="far fa-edit"></i>
                                    </a>
                                @else
                                    <span class="btn btn-outline-secondary disabled">
                                        <i class="far fa-edit"></i>
                                    </span>
                                @endcan

                                @can('group.delete', [$project, $group])
                                    <button type="button"
                                            class="btn btn-outline-danger ajax-delete"
                                            data-url="{{ route('projects.groups.destroy', ['project' => $project, 'group' => $group]) }}"
                                            data-target="#group-{{ $group->id }}"
                                            data-name="{{ $group->name }}">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @else
                                    <button type="button" class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
