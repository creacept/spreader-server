@extends('layouts.altsidebar')

@section('sidebar.content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="content-header">
        <h3>Добавление URL и комментария</h3>
        <div class="form-group row">
            <div class="col-sm-10">
                <a class="btn btn-info" href="{{route('urls-comments.index')}}">Назад</a>
            </div>
        </div>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('urls-comments.store') }}" method="post">
            <div class="form-group row">
                <label for="url-input" class="col-sm-2 col-form-label">URL</label>
                <div class="col-sm-10">
                    <textarea type="text" name="urls" id="url-input" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="comment-input" class="col-sm-2 col-form-label">Комментарий</label>
                <div class="col-sm-10">
                    <input type="text" name="comment" id="comment-input" class="form-control"
                           placeholder="Введите комментарий">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </div>
        </form>
    </div>
@endsection