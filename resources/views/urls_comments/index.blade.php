@extends('layouts.altsidebar')

@section('sidebar.content')
    <div class="content-header">
        <h3>Список комментариев к заявкам</h3>
        @can('urlscomment.add')
            <a href="{{ route('urls-comments.add') }}" class="btn btn-primary">Добавить</a>
        @else
            <button type="button" class="btn btn-primary" disabled>Добавить</button>
        @endcan
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">URLs</th>
                <th scope="col">Комментарий</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>

            @if(is_array($urls_comments))
                @foreach($urls_comments as $urlsComment)
                    <tr id="record-{{ $urlsComment['id'] }}">
                        <td>
                            {{ is_array($urlsComment['urls']) ? implode(PHP_EOL, $urlsComment['urls']) : $urlsComment['urls'] }}
                        </td>
                        <td>
                            {{ $urlsComment['comment'] }}
                        </td>

                        <td>
                            <div class="btn-group" role="group">
                                @can('urlscomment.update')
                                    <a href="{{ route('urls-comments.edit', ['urlsComment' => $urlsComment['id'] ]) }}"
                                       id="{{$urlsComment['id']}}"
                                       class="btn btn-outline-primary">
                                        <i class="far fa-edit"></i>
                                    </a>
                                @else
                                    <span class="btn btn-outline-secondary disabled">
                                        <i class="far fa-edit"></i>
                                    </span>
                                @endcan
                                @can('urlscomment.delete')
                                    <button type="button"
                                            class="btn btn-outline-danger ajax-delete"
                                            data-name="{{ $urlsComment['id'] }}"
                                            data-target="#record-{{ $urlsComment['id'] }}"
                                            data-url="{{ route('urls-comments.delete', ['id' => $urlsComment['id']]) }}"
                                    >
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @else
                                    <button type="button" class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection