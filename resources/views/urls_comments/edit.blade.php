@extends('layouts.altsidebar')

@section('sidebar.content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="content-header">
        <h3>Редактирование URL и комментария</h3>
        <div class="form-group row">
            <div class="col-sm-10">
                <a class="btn btn-info" href="{{route('urls-comments.index')}}">Назад</a>
            </div>
        </div>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('urls-comments.update', ['urlsComment' => $record['id'] ]) }}" method="post">
            <div class="form-group row">
                <label for="url-input" class="col-sm-2 col-form-label">Новый URL</label>
                <div class="col-sm-10">
                    <textarea type="text" name="urls" id="url-input"
                              class="form-control">{{ is_array($record['urls']) ? implode(PHP_EOL, $record['urls']) : $record['urls'] }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="comment-input" class="col-sm-2 col-form-label">Новый комментарий</label>
                <div class="col-sm-10">
                    <input type="text" name="comment" id="comment-input" class="form-control"
                           placeholder="Введите новый комментарий" value="{{ $record['comment'] }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                </div>
            </div>
        </form>
    </div>
@endsection