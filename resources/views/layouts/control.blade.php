@extends('layouts.skeleton')

@section('skeleton.title')
    @yield('control.title')
@endsection

@section('skeleton.content')
    <nav class="navbar navbar-expand-lg db-blue">
        <div class="navbar-nav navbar-left">
            <span class="navbar-brand">Управление</span>
            <a class="change-link" href="{{ route('projects.index') }}">Назад</a>
        </div>
        <nav class="navbar-nav mr-auto">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Управление</li>
                @yield('control.breadcrumbs')
            </ol>
        </nav>
        <nav class="navbar-nav">
            <button type="button" id="logout" class="btn btn-outline-light" data-url="{{ route('logout') }}">Выход</button>
        </nav>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="content-menu db-blue">

                @php $routeName = \Route::getCurrentRoute()->getName() @endphp

                <ul>
                    <li>
                        @if (starts_with($routeName, 'users'))
                            <span>Пользователи</span>
                        @else
                            @can('user.list')
                                <a href="{{ route('users.index') }}">Пользователи</a>
                            @else
                                <span class="text-white-50 bg-secondary">Пользователи</span>
                            @endif
                        @endcan
                    </li>

                    <li>
                        @if (starts_with($routeName, 'crm'))
                            <span>CRM</span>
                        @else
                            @can('crm.list')
                                <a href="{{ route('crm.index') }}">CRM</a>
                            @else
                                <span class="text-white-50 bg-secondary">CRM</span>
                            @endif
                        @endcan
                    </li>
                </ul>
            </div>
            <div class="content-center">
                @yield('control.content')
            </div>
        </div>
    </div>
@endsection