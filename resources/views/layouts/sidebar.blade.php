<?php /** @var \App\Models\Project $project */ ?>
@extends('layouts.skeleton')

@section('skeleton.title')
    @yield('sidebar.title')
@endsection

@section('skeleton.content')
    <nav class="navbar navbar-expand-lg db-blue">
        <div class="navbar-nav navbar-left">
            <span class="navbar-brand">{{ $project->name }}</span>
            <a class="change-link" href="{{ route('projects.index') }}">Сменить</a>
        </div>
        <nav class="navbar-nav mr-auto">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">{{ $project->name }}</li>
                @yield('sidebar.breadcrumbs')
            </ol>
        </nav>
        <nav class="navbar-nav">
            <button type="button" id="logout" class="btn btn-outline-light" data-url="{{ route('logout') }}">Выход</button>
        </nav>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="content-menu db-blue">

                @php $routeName = \Route::getCurrentRoute()->getName() @endphp

                <ul>
                    <li>
                        @if (starts_with($routeName, 'projects.managers'))
                            <span>Менеджеры</span>
                        @else
                            @can('manager.list', [$project])
                                <a href="{{ route('projects.managers.index', ['project' => $project]) }}">Менеджеры</a>
                            @else
                                <span class="text-white-50 bg-secondary">Менеджеры</span>
                            @endcan
                        @endif
                    </li>

                    <li>
                        @if (starts_with($routeName, 'projects.groups'))
                            <span>Группы</span>
                        @else
                            @can('group.list', [$project])
                                <a href="{{ route('projects.groups.index', ['project' => $project]) }}">Группы</a>
                            @else
                                <span class="text-white-50 bg-secondary">Группы</span>
                            @endcan
                        @endif
                    </li>

                    <li>
                        @if (starts_with($routeName, 'projects.sites') || starts_with($routeName, 'sites.rules'))
                            <span>Сайты</span>
                        @else
                            @can('site.list', [$project])
                                <a href="{{ route('projects.sites.index', ['project' => $project]) }}">Сайты</a>
                            @else
                                <span class="text-white-50 bg-secondary">Сайты</span>
                            @endcan
                        @endif
                    </li>

                    <li>
                        @if (starts_with($routeName, 'projects.applications'))
                            <span>Заявки</span>
                        @else
                            @can('application.list', [$project])
                                <a href="{{ route('projects.applications.index', ['project' => $project]) }}">Заявки</a>
                            @else
                                <span class="text-white-50 bg-secondary">Заявки</span>
                            @endcan
                        @endif
                    </li>

                    <li>
                        @if (starts_with($routeName, 'projects.imports'))
                            <span>Импорт</span>
                        @else
                            @can('import.list', [$project])
                                <a href="{{ route('projects.imports.index', ['project' => $project]) }}">Импорт</a>
                            @else
                                <span class="text-white-50 bg-secondary">Импорт</span>
                            @endcan
                        @endif
                    </li>

                    <li>
                        @if (starts_with($routeName, 'projects.evecalls-imports'))
                            <span>Импорт EVE.calls</span>
                        @else
                            @can('evecalls-import.list', [$project])
                                <a href="{{ route('projects.evecalls-imports.index', ['project' => $project]) }}">Импорт EVE.calls</a>
                            @else
                                <span class="text-white-50 bg-secondary">Импорт EVE.calls</span>
                            @endcan
                        @endif
                    </li>
                </ul>
            </div>
            <div class="content-center">
                @yield('sidebar.content')
            </div>
        </div>
    </div>
@endsection