@extends('layouts.skeleton')

@section('skeleton.title')
    @yield('sidebar.title')
@endsection

@section('skeleton.content')

    <div class="container-fluid">
        <div class="row">
            <div class="content-menu db-blue">
                <ul>
                    <li>
                        <a href="{{ route('home') }}">Назад к проектам</a>
                    </li>
                </ul>
            </div>
            <div class="content-center">
                @yield('sidebar.content')
            </div>
        </div>
    </div>
@endsection