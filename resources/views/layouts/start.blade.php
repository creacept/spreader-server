@extends('layouts.skeleton')

@section('skeleton.title')
    @yield('start.title')
@endsection

@section('skeleton.content')
    <nav class="navbar navbar-expand-lg navbar-light navbar-login">
        <div class="navbar-nav navbar-left">
            @can('authorization')
                <a class="btn btn-warning" href="{{ route('authorization') }}">Авторизация</a>
            @else
                <span class="btn btn-secondary disabled">Авторизация</span>
            @endcan
        </div>

        <div class="navbar-nav navbar-left">
            @can('horizon')
                <a href="{{ route('horizon.index', ['view' => 'dashboard']) }}" class="btn btn-success" target="_blank">Laravel Horizon</a>
            @else
                <span class="btn btn-secondary disabled">Laravel Horizon</span>
            @endcan
        </div>

        <div class="navbar-nav navbar-right">
            @can('urlscomment.list')
                <a href="{{ route('urls-comments.index') }}" class="btn btn-success">Урлы и комментарии</a>
            @else
                <span class="btn btn-secondary disabled">Урлы и комментарии</span>
            @endcan
        </div>

        <div class="navbar-right">
            @can('user.list')
                <a href="{{ route('users.index') }}" class="btn btn-primary">Управление</a>
            @elsecan('crm.list')
                <a href="{{ route('crm.index') }}" class="btn btn-primary">Управление</a>
            @else
                <span class="btn btn-secondary disabled">Управление</span>
            @endcan
        </div>

        <div class="navbar-right">

            <button type="button" id="logout" class="btn btn-danger" data-url="{{ route('logout') }}">Выход</button>
                {{--<span class="btn btn-secondary disabled">Выход</span>--}}

        </div>
    </nav>

    @yield('start.content')
@endsection
