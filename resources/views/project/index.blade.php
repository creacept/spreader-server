<?php /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects */ ?>

@extends('layouts.start')

@section('start.title')Проекты@endsection

@section('start.content')
    <div class="choose-project">
        <ul class="list-group">
            <li class="list-group-item active">
                Проекты
                @can('project.create')
                    <a href="{{ route('projects.create') }}" class="btn btn-success">
                        Добавить проект
                    </a>
                @else
                    <span class="btn btn-secondary disabled">Добавить проект</span>
                @endcan
            </li>
            @foreach($projects as $project)
                <li id="project-{{ $project->id }}" class="list-group-item">
                    @can('project.update', [$project])
                        <input type="checkbox"
                               id="project-publish-{{ $project->id }}"
                               class="status-change ajax"
                               data-url="{{ route('projects.publish', $project) }}"
                               @if ($project->publish) checked @endif>
                        <label class="ajax" for="project-publish-{{ $project->id }}" data-name="{{ $project->name }}"></label>
                    @else
                        <input type="checkbox"
                               id="project-publish-{{ $project->id }}"
                               class="status-change"
                               disabled
                               @if ($project->publish) checked @endif>
                        <label for="project-publish-{{ $project->id }}"></label>
                    @endcan

                    @can('manager.list', [$project])
                        <a href="{{ route('projects.managers.index', ['project' => $project]) }}">{{ $project->name }}</a>
                    @elsecan('group.list', [$project])
                        <a href="{{ route('projects.groups.index', ['project' => $project]) }}">{{ $project->name }}</a>
                    @elsecan('site.list', [$project])
                        <a href="{{ route('projects.sites.index', ['project' => $project]) }}">{{ $project->name }}</a>
                    @elsecan('application.list', [$project])
                        <a href="{{ route('projects.applications.index', ['project' => $project]) }}">{{ $project->name }}</a>
                    @elsecan('import.list', [$project])
                        <a href="{{ route('projects.imports.index', ['project' => $project]) }}">{{ $project->name }}</a>
                    @else
                        <span class="text-secondary">{{ $project->name }}</span>
                    @endcan

                    <div class="btn-group" role="group">
                        @can('project.update', [$project])
                            <a href="{{ route('projects.edit', ['project' => $project]) }}" class="btn btn-outline-primary">
                                <i class="far fa-edit"></i>
                            </a>
                        @else
                            <span class="btn btn-outline-secondary disabled">
                                <i class="far fa-edit"></i>
                            </span>
                        @endcan

                        @can('project.delete', [$project])
                            <button type="button"
                                    class="btn btn-outline-danger ajax-delete"
                                    data-url="{{ route('projects.destroy', ['project' => $project]) }}"
                                    data-target="#project-{{ $project->id }}"
                                    data-name="{{ $project->name }}">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        @else
                            <button type="button" class="btn btn-outline-secondary" disabled>
                                <i class="far fa-trash-alt"></i>
                            </button>
                        @endcan
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
