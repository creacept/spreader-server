<?php
/** @var \App\Models\Project $project */
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\CRM[] $collection */
/** @var array $contactIdentities */
/** @var array $leadIdentitise */
?>

@extends('layouts.start')

@section('start.title') Редактирование проекта @endsection

@section('start.content')
    <div class="content">
        <div class="choose-project row">
            <ul class="nav nav-pills justify-content-center col-12" id="tabSettings" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="settings_global-tab" data-toggle="tab" href="#settings_global"
                       role="tab" aria-controls="settings_global" aria-selected="true">
                        Глобальные настройки
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="settings_moodle-tab" data-toggle="tab" href="#settings_moodle" role="tab"
                       aria-controls="settings_moodle" aria-selected="false">
                        Настройки Moodle
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="settings_evecalls-tab" data-toggle="tab" href="#settings_evecalls"
                       role="tab"
                       aria-controls="settings_evecalls" aria-selected="false">
                        Настройки EVE.calls
                    </a>
                </li>
            </ul>
            <form id="admin-form" class="project-form"
                  action="{{ route('projects.update', ['project' => $project]) }}" method="post">
                <div class="tab-content" id="myTabContent">
                    @method('put')
                    <div class="tab-pane fade show active" id="settings_global" role="tabpanel"
                         aria-labelledby="settings_global-tab">
                        <h2>Редактирование проекта &quot;<strong>{{ $project->name }}</strong>&quot;</h2>
                        <div class="form-group row">
                            <label for="name-input" class="col-sm-3 col-form-label">Название</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" value="{{ $project->name }}" class="form-control"
                                       id="name-input" placeholder="Введите новое название проекта">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="crm-select" class="col-sm-3 col-form-label">CRM</label>
                            <div class="col-sm-9">
                                <select name="crm_id" id="crm-select" class="form-control">
                                    <option value="">-- Выберите значение --</option>
                                    @foreach($collection as $crm)
                                        <option value="{{ $crm->id }}"
                                                @if($project->crm_id === $crm->id) selected @endif>{{ $crm->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact_identity-select" class="col-sm-3 col-form-label">Идентификатор
                                контакта</label>
                            <div class="col-sm-9">
                                <select name="contact_identity" id="contact_identity-select" class="form-control"
                                        @if(count($contactIdentities) === 0) disabled @endif>
                                    <option value="">-- Выберите значение --</option>
                                    @foreach($contactIdentities as $data)
                                        <option value="{{ $data['id'] }}"
                                                @if($project->contact_identity === $data['id']) selected @endif>{{ $data['text'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lead_identity-select" class="col-sm-3 col-form-label">Идентификатор лида</label>
                            <div class="col-sm-9">
                                <select name="lead_identity" id="lead_identity-select" class="form-control"
                                        @if (count($leadIdentities) === 0) disabled @endif>
                                    <option value="">-- Выберите значение --</option>
                                    @foreach($leadIdentities as $data)
                                        <option value="{{ $data['id'] }}"
                                                @if($project->lead_identity === $data['id']) selected @endif>{{ $data['text'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="publish-input">Состояние</label>
                            <div class="col-sm-9">
                                <input type="hidden" name="publish" value="0">
                                <input type="checkbox" name="publish" value="1" id="publish-input" class="status-change"
                                       @if($project->publish) checked @endif>
                                <label class="form-control" for="publish-input"></label>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="settings_moodle" role="tabpanel"
                         aria-labelledby="settings_moodle-tab">
                        <h2>Настройки Moodle</h2>
                        <div class="form-group row">
                            <label for="moodle-url-input" class="col-sm-3 col-form-label">URL сайта Moodle</label>
                            <div class="col-sm-9">
                                <input type="url" name="moodle[base_url]" class="form-control" id="moodle-url-input"
                                       value="{{ array_get($project->moodle, 'base_url') }}"
                                       placeholder="Например, https://my.academyfx.ru">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="moodle-token-input" class="col-sm-3 col-form-label">Ключ доступа</label>
                            <div class="col-sm-9">
                                <input type="text" name="moodle[access_token]"
                                       value="{{ array_get($project->moodle, 'access_token') }}" class="form-control"
                                       id="moodle-token-input">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="settings_evecalls" role="tabpanel"
                         aria-labelledby="settings_evecalls-tab">
                        <h2>Настройки EVE.calls</h2>
                        <div class="form-group row">
                            <label for="evecalls-api-key" class="col-sm-3 col-form-label">Ключ доступа API</label>
                            <div class="col-sm-9">
                                <input type="text" name="evecalls[api_key]"
                                       value="{{ array_get($project->evecalls, 'api_key') }}" class="form-control"
                                       id="evecalls-api-key">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="evecalls-date-field-id" class="col-sm-3 col-form-label">ID поля "Дата звонка" в CRM</label>
                            <div class="col-sm-9">
                                <input type="text" name="evecalls[date_field]" class="form-control"
                                       value="{{ array_get($project->evecalls, 'date_field') }}"
                                       placeholder="UF_CRM_XXXXXXXXXX"
                                       id="evecalls-date-field-id">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="evecalls-recording-field-id" class="col-sm-3 col-form-label">ID поля "Запись разговора" в CRM</label>
                            <div class="col-sm-9">
                                <input type="text" name="evecalls[recording_field]" class="form-control"
                                       value="{{ array_get($project->evecalls, 'recording_field') }}"
                                       placeholder="UF_CRM_XXXXXXXXXX"
                                       id="evecalls-recording-field-id">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Изменить</button>
                            <a href="{{ route('projects.index') }}" class="btn btn-secondary">Отмена</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
