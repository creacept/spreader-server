<?php
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects */
/** @var array $permissions */
?>

<div class="form-group row">
    <div class="col-sm-2">Разрешения</div>
    <div class="col-sm-10">
        @foreach($projects as $project)
            <div class="border p-2 rounded mb-2">
                <h5>{{ $project->name }}</h5>
                <div class="border p-2 rounded mb-2">
                    <h6>Менеджеры</h6>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.manager.list"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}10]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}10"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}10">Список</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.manager.create"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}11]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}11"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}11">Импорт</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.manager.update"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}12]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}12"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}12">Редактирование</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.manager.delete"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}13]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}13"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}13">Удаление</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border p-2 rounded mb-2">
                    <h6>Группы</h6>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.group.list"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}14]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}14"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}14">Список</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.group.create"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}15]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}15"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}15">Создание</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.group.update"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}16]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}16"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}16">Редактирование</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.group.delete"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}17]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}17"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}17">Удаление</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border p-2 rounded mb-2">
                    <h6>Сайты</h6>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.site.list"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}18]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}18"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}18">Список</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.site.create"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}19]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}19"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}19">Создание</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.site.update"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}20]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}20"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}20">Редактирование</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.site.delete"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}21]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}21"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}21">Удаление</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border p-2 rounded mb-2">
                    <h6>Правила</h6>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.rule.list"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}22]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}22"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}22">Список</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.rule.create"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}23]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}23"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}23">Создание</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.rule.update"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}24]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}24"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}24">Редактирование</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.rule.delete"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}25]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}25"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}25">Удаление</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border p-2 rounded mb-2">
                    <h6>Заявки</h6>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.application.list"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}26]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}26"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}26">Список</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.application.view"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}27]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}27"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}27">Подробно</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border p-2 rounded mb-2">
                    <h6>Импорт</h6>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.import.list"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}28]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}28"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}28">Список</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.import.create"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}29]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}29"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}29">Создание</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.import.view"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}30]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}30"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}30">Результат</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border p-2 rounded mb-2">
                    <h6>Импорт EVE.calls</h6>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.evecalls-import.list"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}31]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}31"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}31">Список</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.evecalls-import.create"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}32]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}32"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}32">Создание</label>
                            </div>
                            <div class="col-sm-3 form-check">
                                @php $permission = "project_{$project->id}.evecalls-import.view"; @endphp
                                <input type="checkbox"
                                       name="permissions[{{ $project->id }}33]"
                                       value="{{ $permission }}"
                                       id="permission-{{ $project->id }}33"
                                       class="form-check-input"
                                       @if(in_array($permission, $permissions)) checked @endif>
                                <label class="form-check-label" for="permission-{{ $project->id }}33">Результат</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="border p-2 rounded mb-2">
            <h5>Проекты</h5>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 form-check">
                        @php $permission = 'project.create'; @endphp
                        <input type="checkbox"
                               name="permissions[6]"
                               value="{{ $permission }}"
                               id="permission-6"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-6">Создание</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'project.update'; @endphp
                        <input type="checkbox"
                               name="permissions[7]"
                               value="{{ $permission }}"
                               id="permission-7"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-7">Редактирование</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'project.delete'; @endphp
                        <input type="checkbox"
                               name="permissions[8]"
                               value="{{ $permission }}"
                               id="permission-8"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-8">Удаление</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="border p-2 rounded mb-2">
            <h5>Пользователи</h5>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 form-check">
                        @php $permission = 'user.list'; @endphp
                        <input type="checkbox"
                               name="permissions[0]"
                               value="{{ $permission }}"
                               id="permission-0"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-0">Список</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'user.create'; @endphp
                        <input type="checkbox"
                               name="permissions[1]"
                               value="{{ $permission }}"
                               id="permission-1"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-1">Создание</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'user.update'; @endphp
                        <input type="checkbox"
                               name="permissions[2]"
                               value="{{ $permission }}"
                               id="permission-2"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-2">Редактирование</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'user.delete'; @endphp
                        <input type="checkbox"
                               name="permissions[3]"
                               value="{{ $permission }}"
                               id="permission-3"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-3">Удаление</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="border p-2 rounded mb-2">
            <h5>CRM</h5>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 form-check">
                        @php $permission = 'crm.list'; @endphp
                        <input type="checkbox"
                               name="permissions[31]"
                               value="{{ $permission }}"
                               id="permission-31"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-31">Список</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'crm.create'; @endphp
                        <input type="checkbox"
                               name="permissions[32]"
                               value="{{ $permission }}"
                               id="permission-32"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-32">Создание</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'crm.update'; @endphp
                        <input type="checkbox"
                               name="permissions[33]"
                               value="{{ $permission }}"
                               id="permission-33"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-33">Редактирование</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'crm.delete'; @endphp
                        <input type="checkbox"
                               name="permissions[34]"
                               value="{{ $permission }}"
                               id="permission-34"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-34">Удаление</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'crm.settings'; @endphp
                        <input type="checkbox"
                               name="permissions[35]"
                               value="{{ $permission }}"
                               id="permission-35"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-35">Настройки</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="border p-2 rounded mb-2">
            <h5>Прочее</h5>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 form-check">
                        @php $permission = 'authorization'; @endphp
                        <input type="checkbox"
                               name="permissions[]"
                               value="{{ $permission }}"
                               id="permission-4"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-4">Авторизация</label>
                    </div>
                    <div class="col-sm-3 form-check">
                        @php $permission = 'horizon'; @endphp
                        <input type="checkbox"
                               name="permissions[5]"
                               value="{{ $permission }}"
                               id="permission-5"
                               class="form-check-input"
                               @if(in_array($permission, $permissions)) checked @endif>
                        <label class="form-check-label" for="permission-5">Laravel Horizon</label>
                    </div>
                </div>
            </div>
        </div>
            <div class="border p-2 rounded mb-2">
                <h5>Добавление комментариев к заявкам</h5>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 form-check">
                            @php $permission = 'urlscomment.list'; @endphp
                            <input type="checkbox"
                                   name="permissions[36]"
                                   value="{{ $permission }}"
                                   id="permission-36"
                                   class="form-check-input"
                                   @if(in_array($permission, $permissions)) checked @endif>
                            <label class="form-check-label" for="permission-36">Просмотр</label>
                        </div>
                        <div class="col-sm-3 form-check">
                            @php $permission = 'urlscomment.create'; @endphp
                            <input type="checkbox"
                                   name="permissions[37]"
                                   value="{{ $permission }}"
                                   id="permission-37"
                                   class="form-check-input"
                                   @if(in_array($permission, $permissions)) checked @endif>
                            <label class="form-check-label" for="permission-37">Добавление</label>
                        </div>
                        <div class="col-sm-3 form-check">
                            @php $permission = 'urlscomment.update'; @endphp
                            <input type="checkbox"
                                   name="permissions[38]"
                                   value="{{ $permission }}"
                                   id="permission-38"
                                   class="form-check-input"
                                   @if(in_array($permission, $permissions)) checked @endif>
                            <label class="form-check-label" for="permission-38">Редактирование</label>
                        </div>
                        <div class="col-sm-3 form-check">
                            @php $permission = 'urlscomment.delete'; @endphp
                            <input type="checkbox"
                                   name="permissions[39]"
                                   value="{{ $permission }}"
                                   id="permission-39"
                                   class="form-check-input"
                                   @if(in_array($permission, $permissions)) checked @endif>
                            <label class="form-check-label" for="permission-39">Удаление</label>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>