<?php
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects */
/** @var bool $isSuperAdmin */
/** @var int $countPublishSuperAdmin */
/** @var \App\Models\User $user */
?>

@extends('layouts.control')

@section('control.title')Редактирование пользователя@endsection

@section('control.breadcrumbs')
    <li class="breadcrumb-item">
        @can('user.list')
            <a href="{{ route('users.index') }}">Пользователи</a>
        @else
            <span class="text-white-50">Пользователи</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Редактирование пользователя</li>
@endsection

@section('control.content')
    <div class="content-header">
        <h3>
            Редактирование пользователя &quot;<strong>{{ $user->name }}</strong>&quot;
        </h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('users.update', ['user' => $user]) }}" method="post">
            @method('put')
            <div class="form-group row">
                <label for="name-input" class="col-sm-2 col-form-label">Имя</label>
                <div class="col-sm-10">
                    <input type="text" name="name" value="{{ $user->name }}" id="name-input" class="form-control" placeholder="Введите новое имя пользователя">
                </div>
            </div>
            <div class="form-group row">
                <label for="email-input" class="col-sm-2 col-form-label">E-mail</label>
                <div class="col-sm-10">
                    <input type="email" name="email" value="{{ $user->email }}" id="email-input" class="form-control" placeholder="Введите новый E-mail">
                </div>
            </div>
            <div class="form-group row">
                <label for="password-input" class="col-sm-2 col-form-label">Пароль</label>
                <div class="col-sm-10">
                    <input type="password" name="password" id="password-input" class="form-control" placeholder="Введите новый пароль">
                </div>
            </div>
            @if($isSuperAdmin)
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="super_admin-input">Super Admin</label>
                    <div class="col-sm-10">
                        <input type="hidden" name="super_admin" value="0">
                        <input type="checkbox"
                               name="super_admin"
                               value="1"
                               id="super_admin-input"
                               class="status-change"
                               @if($user->super_admin && $user->publish && $countPublishSuperAdmin === 1) disabled @endif
                               @if($user->super_admin) checked @endif>
                        <label class="form-control" for="super_admin-input"></label>
                    </div>
                </div>
            @endif
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="publish-input">Состояние</label>
                <div class="col-sm-10">
                    <input type="hidden" name="publish" value="0">
                    <input type="checkbox"
                           name="publish"
                           value="1"
                           id="publish-input"
                           class="status-change"
                           @if($user->super_admin && $user->publish && $countPublishSuperAdmin === 1) disabled @endif
                           @if($user->publish) checked @endif>
                    <label class="form-control" for="publish-input"></label>
                </div>
            </div>
            @include('user.permissions', ['projects' => $projects, 'permissions' => $user->permissions])
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Изменить</button>
                    @can('user.list')
                        <a href="{{ route('users.index') }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection