<?php
/** @var bool $isSuperAdmin */
/** @var int $countPublishSuperAdmin */
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users */
?>

@extends('layouts.control')

@section('control.title')Пользователи@endsection

@section('control.breadcrumbs')
    <li class="breadcrumb-item active" aria-current="page">Пользователи</li>
@endsection

@section('control.content')
    <div class="content-header">
        <h3>Список пользователей</h3>
        @can('user.create')
            <a href="{{ route('users.create') }}" class="btn btn-primary">Добавить пользователя</a>
        @else
            <span class="btn btn-secondary disabled">Добавить пользователя</span>
        @endcan
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Состояние</th>
                <th scope="col">Имя</th>
                <th scope="col">E-mail</th>
                @if($isSuperAdmin)
                    <th scope="col"></th>
                @endif
                <th scope="col">ID</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr id="user-{{ $user->id }}">
                        <td>
                            @can('user.update', [$user])
                                <input type="checkbox"
                                       id="user-publish-{{ $user->id }}"
                                       class="status-change ajax"
                                       data-url="{{ route('users.publish', ['user' => $user]) }}"
                                       @if($user->super_admin && $user->publish && $countPublishSuperAdmin === 1) disabled @endif
                                       @if($user->publish) checked @endif>
                                <label @unless($user->super_admin && $user->publish && $countPublishSuperAdmin === 1) class="ajax" @endunless for="user-publish-{{ $user->id }}" data-name="{{ $user->name }}"></label>
                            @else
                                <input type="checkbox"
                                       id="user-publish-{{ $user->id }}"
                                       class="status-change"
                                       disabled
                                       @if($user->publish) checked @endif>
                                <label for="user-publish-{{ $user->id }}"></label>
                            @endcan
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        @if($isSuperAdmin)
                            <td>
                                @if ($user->super_admin)
                                    <span class="p-1 bg-dark text-warning">Super Admin</span>
                                @endif
                            </td>
                        @endif
                        <th scope="row">{{ $user->id }}</th>
                        <td>
                            <div class="btn-group" role="group">
                                @can('user.update', [$user])
                                    <a href="{{ route('users.edit', ['user' => $user]) }}" class="btn btn-outline-primary">
                                        <i class="far fa-edit"></i>
                                    </a>
                                @else
                                    <span class="btn btn-outline-secondary disabled">
                                        <i class="far fa-edit"></i>
                                    </span>
                                @endcan

                                @unless($user->super_admin && $user->publish && $countPublishSuperAdmin === 1)
                                    @can('user.delete', [$user])
                                        <button type="button"
                                                class="btn btn-outline-danger ajax-delete"
                                                data-url="{{ route('users.destroy', ['user' => $user]) }}"
                                                data-target="#user-{{ $user->id }}"
                                                data-name="{{ $user->name }}">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    @else
                                        <button type="button" class="btn btn-outline-secondary" disabled>
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    @endcan
                                @endunless
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
