<?php
/** @var \App\Models\Project $project */
/** @var \App\Models\EveCalls\Import $import */
/** @var \App\Models\Application[]|\App\Models\EveCalls\ImportError[] $rows */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Просмотр импорта@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('evecalls-import.list', [$project])
            <a href="{{ route('projects.evecalls-imports.index', ['project' => $project]) }}">Импорты EVE.calls</a>
        @else
            <span class="text-white-50">Импорты</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Результат импорта</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Результат импорта #{{ $import->id }}</h3>
    </div>
    <div class="content-table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" class="text-nowrap">Название лида</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">Дата звонка</th>
                    <th scope="col">Запись разговора</th>
                </tr>
            </thead>
            <tbody>
                @foreach($import->applications as $row)
                    <tr class="table-success">
                        <th scope="col">{{ $row->id }}</th>
                        <td>{{ $row->form_name }}</td>
                        <td>{{ $row->sender_phone }}</td>
                        <td>{{ array_get($row->lead_extra_fields, $crm_date_field) }}</td>
                        <td>{{ array_get($row->lead_extra_fields, $crm_recording_field) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
