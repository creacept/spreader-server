<?php
/** @var \App\Models\Project $project */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Создание импорта EVE.calls @endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('evecalls-import.list', [$project])
            <a href="{{ route('projects.evecalls-imports.index', ['project' => $project]) }}">Импорты EVE.calls</a>
        @else
            <span class="text-white-50">Импорты EVE.calls</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Создание импорта</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Создание импорта</h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('projects.evecalls-imports.store', ['project' => $project]) }}" method="post">
            <div class="form-group row">
                <label for="file-input" class="col-sm-2 col-form-label">Файл</label>
                <div class="col-sm-10">
                    <div>
                        <input type="file" name="file" id="file-input" class="form-control form-control-file" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="lead-name-input" class="col-sm-2 col-form-label">Название лида</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="lead_name" id="lead-name-input" placeholder="По умолчанию «{{ \App\Models\EveCalls\Import::DEFAULT_LEAD_NAME }}»">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Создать</button>
                    @can('evecalls-import.list', [$project])
                        <a href="{{ route('projects.evecalls-imports.index', ['project' => $project]) }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection
