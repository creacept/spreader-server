<?php
/** @var \App\Models\CRM $crm */
/** @var array $dealCategories */
?>

@extends('layouts.control')

@section('control.title') Настройки @endsection

@section('control.breadcrumbs')
    <li class="breadcrumb-item">
        @can('crm.list')
            <a href="{{ route('crm.index') }}">CRM</a>
        @else
            <span class="text-white-50">CRM</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Настройки</li>
@endsection

@section('control.content')
    <div class="content-header">
        <h3>
            Настройки для CRM &quot;<strong>{{ $crm->name }}</strong>&quot;
        </h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('crm.save_settings', ['crm' => $crm]) }}" method="post">
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Игнорировать сделки в направлениях</legend>
                    <div class="col-sm-10">
                        @php $ignoredDealCategories = array_get($crm->settings, 'ignored_deal_categories', []); @endphp
                        @foreach($dealCategories as $data)
                            <div class="form-check">
                                <input type="checkbox"
                                       name="ignored_deal_categories[{{ $data['id'] }}]"
                                       value="{{ $data['id'] }}"
                                       id="ignored_deal_categories-{{ $data['id'] }}-input"
                                       class="form-check-input"
                                       @if(in_array($data['id'], $ignoredDealCategories)) checked @endif>
                                <label class="form-check-label" for="ignored_deal_categories-{{ $data['id'] }}-input">{{ $data['name'] }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </fieldset>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    @can('crm.list')
                        <a href="{{ route('crm.index') }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection