@extends('layouts.control')

@section('control.title') Создание CRM @endsection

@section('control.breadcrumbs')
    <li class="breadcrumb-item">
        @can('crm.list')
            <a href="{{ route('crm.index') }}">CRM</a>
        @else
            <span class="text-white-50">CRM</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Создание CRM</li>
@endsection

@section('control.content')
    <div class="content-header">
        <h3>Создание CRM</h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('crm.store') }}" method="post">
            <div class="form-group row">
                <label for="name-input" class="col-sm-2 col-form-label">Название</label>
                <div class="col-sm-10">
                    <input type="text" name="name" id="name-input" class="form-control" placeholder="Введите название">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <h4>Авторизация</h4>
                </div>
            </div>
            <fieldset class="form-group">
                <div class="row">
                    <div class="col-form-label col-sm-2">Тип</div>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input depending-master"
                                   type="radio"
                                   name="config[auth][type]"
                                   id="config-auth-type-oauth2-radio"
                                   value="{{ \App\Models\CRM::AUTH_OAUTH2 }}"
                                   data-depending="auth-type"
                                   data-depending-value="oauth2"
                                   checked>
                            <label class="form-check-label" for="config-auth-type-oauth2-radio">OAuth 2.0</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input depending-master"
                                   type="radio"
                                   name="config[auth][type]"
                                   id="config-auth-type-webhook-radio"
                                   value="{{ \App\Models\CRM::AUTH_WEBHOOK }}"
                                   data-depending="auth-type"
                                   data-depending-value="webhook">
                            <label class="form-check-label" for="config-auth-type-webhook-radio">Webhook</label>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="depending-slave" data-depending="auth-type" data-depending-value="oauth2">
                <div class="form-group row">
                    <label for="config-auth-domain-input" class="col-sm-2 col-form-label">Домен</label>
                    <div class="col-sm-10">
                        <input type="text" name="config[auth][domain]" id="config-auth-domain-input" class="form-control" placeholder="Домен">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="config-auth-client_id-input" class="col-sm-2 col-form-label">Код приложения</label>
                    <div class="col-sm-10">
                        <input type="text" name="config[auth][client_id]" id="config-auth-client_id-input" class="form-control" placeholder="Введите код приложения (client_id)">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="config-auth-client_secret-input" class="col-sm-2 col-form-label">Ключ приложения</label>
                    <div class="col-sm-10">
                        <input type="text" name="config[auth][client_secret]" id="config-auth-client_secret-input" class="form-control" placeholder="Введите ключ приложения (client_secret)">
                    </div>
                </div>
            </div>
            <div class="d-none depending-slave" data-depending="auth-type" data-depending-value="webhook">
                <div class="form-group row">
                    <label for="config-auth-webhook-input" class="col-sm-2 col-form-label">Webhook</label>
                    <div class="col-sm-10">
                        <input type="text" name="config[auth][webhook]" id="config-auth-webhook-input" class="form-control" placeholder="Webhook">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <h4>База данных</h4>
                </div>
            </div>
            <div class="form-group row">
                <label for="config-db-host-input" class="col-sm-2 col-form-label">Хост</label>
                <div class="col-sm-10">
                    <input type="text" name="config[db][host]" id="config-db-host-input" class="form-control" placeholder="Введите хост">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-db-port-input" class="col-sm-2 col-form-label">Порт</label>
                <div class="col-sm-10">
                    <input type="text" name="config[db][port]" id="config-db-port-input" class="form-control" placeholder="Введите порт">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-db-database-input" class="col-sm-2 col-form-label">База данных</label>
                <div class="col-sm-10">
                    <input type="text" name="config[db][database]" id="config-db-database-input" class="form-control" placeholder="Введите базу данных">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-db-login-input" class="col-sm-2 col-form-label">Логин</label>
                <div class="col-sm-10">
                    <input type="text" name="config[db][login]" id="config-db-login-input" class="form-control" placeholder="Введите логин">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-db-password-input" class="col-sm-2 col-form-label">Пароль</label>
                <div class="col-sm-10">
                    <input type="password" name="config[db][password]" id="config-db-password-input" class="form-control" placeholder="Введите пароль">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <h4>Контакты</h4>
                </div>
            </div>
            <div class="form-group row">
                <label for="config-contact-identity-input" class="col-sm-2 col-form-label">Проект</label>
                <div class="col-sm-10">
                    <input type="text" name="config[contact][identity]" id="config-contact-identity-input" class="form-control" placeholder="UF_CRM_XXXXXXXXXX">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-contact-worked_at-input" class="col-sm-2 col-form-label">Дата последнего обращения</label>
                <div class="col-sm-10">
                    <input type="text" name="config[contact][worked_at]" id="config-contact-worked_at-input" class="form-control" placeholder="UF_CRM_XXXXXXXXXX">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-contact-vk-input" class="col-sm-2 col-form-label">Страница VK</label>
                <div class="col-sm-10">
                    <input type="text" name="config[contact][vk]" id="config-contact-vk-input" class="form-control" placeholder="UF_CRM_XXXXXXXXXX">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <h4>Лиды</h4>
                </div>
            </div>
            <div class="form-group row">
                <label for="config-lead-identity-input" class="col-sm-2 col-form-label">Проект</label>
                <div class="col-sm-10">
                    <input type="text" name="config[lead][identity]" id="config-lead-identity-input" class="form-control" placeholder="UF_CRM_XXXXXXXXXX">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-lead-url-input" class="col-sm-2 col-form-label">URL</label>
                <div class="col-sm-10">
                    <input type="text" name="config[lead][url]" id="config-lead-url-input" class="form-control" placeholder="UF_CRM_XXXXXXXXXX">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-lead-title-input" class="col-sm-2 col-form-label">Заголовок страницы</label>
                <div class="col-sm-10">
                    <input type="text" name="config[lead][title]" id="config-lead-title-input" class="form-control" placeholder="UF_CRM_XXXXXXXXXX">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-lead-sub_id-input" class="col-sm-2 col-form-label">SUB ID</label>
                <div class="col-sm-10">
                    <input type="text" name="config[lead][sub_id]" id="config-lead-title-input" class="form-control" placeholder="UF_CRM_XXXXXXXXXX">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-lead-is_new-input" class="col-sm-2 col-form-label">Новорег</label>
                <div class="col-sm-10">
                    <input type="text" name="config[lead][is_new]" id="config-lead-is_new-input" class="form-control" placeholder="UF_CRM_XXXXXXXXXX">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-lead-extra-fields" class="col-sm-2 col-form-label">Дополнительные поля</label>
                <div class="col-sm-10">
                    <input type="text" name="config[lead][extra_fields]" id="config-lead-extra-fields" class="form-control" placeholder="UF_CRM_XXXXXXXXXX">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-lead-contact-id" class="col-sm-2 col-form-label">Идентификатор контакта в спридере *</label>
                <div class="col-sm-10">
                    <input type="text" required
                           name="config[lead][contact_id]"
                           id="config-lead-contact-id"
                           class="form-control"
                           placeholder="UF_CRM_XXXXXXXXXX"
                    />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <h4>Поля</h4>
                </div>
            </div>
            <div class="form-group row">
                <label for="config-field-contact_identity-input" class="col-sm-2 col-form-label">Проект (контакты)</label>
                <div class="col-sm-10">
                    <input type="text" name="config[field][contact_identity]" id="config-field-contact_identity-input" class="form-control" placeholder="Введите ID поля">
                </div>
            </div>
            <div class="form-group row">
                <label for="config-field-lead_identity-input" class="col-sm-2 col-form-label">Проект (лиды)</label>
                <div class="col-sm-10">
                    <input type="text" name="config[field][lead_identity]" id="config-field-lead_identity-input" class="form-control" placeholder="Введите ID поля">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <h4>Бизнес процессы</h4>
                </div>
            </div>
            <div class="form-group row">
                <label for="config-workflow-after_create_lead-input" class="col-sm-2 col-form-label">После создания лида</label>
                <div class="col-sm-10">
                    <input type="text" name="config[workflow][after_create_lead]" id="config-workflow-after_create_lead-input" class="form-control" placeholder="Введите ID процесса">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Создать</button>
                    @can('crm.list')
                        <a href="{{ route('crm.index') }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection
