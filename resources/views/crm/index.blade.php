<?php
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\CRM[] $collection */
?>

@extends('layouts.control')

@section('control.title') CRM @endsection

@section('control.breadcrumbs')
    <li class="breadcrumb-item active" aria-current="page">CRM</li>
@endsection

@section('control.content')
    <div class="content-header">
        <h3>Список CRM</h3>
        @can('crm.create')
            <a href="{{ route('crm.create') }}" class="btn btn-primary">Добавить CRM</a>
        @else
            <span class="btn btn-secondary disabled">Добавить CRM</span>
        @endcan
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Название</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($collection as $crm)
                <tr id="crm-{{ $crm->id }}">
                    <th scope="row">{{ $crm->id }}</th>
                    <td>
                        @can('crm.settings', [$crm])
                            <a href="{{ route('crm.edit_settings', ['crm' => $crm]) }}">
                                {{ $crm->name }}
                            </a>
                        @else
                            <span class="text-secondary">{{ $crm->name }}</span>
                        @endcan
                    </td>
                    <td>
                        <div class="btn-group" role="group">
                            @can('crm.update', [$crm])
                                <a href="{{ route('crm.edit', ['crm' => $crm]) }}" class="btn btn-outline-primary">
                                    <i class="far fa-edit"></i>
                                </a>
                            @else
                                <span class="btn btn-outline-secondary disabled">
                                    <i class="far fa-edit"></i>
                                </span>
                            @endcan

                            @if($crm->projects->count() === 0)
                                @can('crm.delete', [$crm])
                                    <button type="button"
                                            class="btn btn-outline-danger ajax-delete"
                                            data-url="{{ route('crm.destroy', ['crm' => $crm]) }}"
                                            data-target="#crm-{{ $crm->id }}"
                                            data-name="{{ $crm->name }}">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @elsecan
                                    <button type="button" class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @endcan
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
