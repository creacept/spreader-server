<?php
/** @var \App\Models\Project $project */
/** @var \App\Models\Site $site */
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Group[] $groups */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Создание правила@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('site.list', [$project])
            <a href="{{ route('projects.sites.index', ['project' => $project]) }}">Сайты</a>
        @else
            <span class="text-white-50">Сайты</span>
        @endcan
    </li>
    <li class="breadcrumb-item">
        @can('rule.list', [$site])
            <a href="{{ route('sites.rules.index', ['site' => $site]) }}">Правила</a>
        @else
            <span class="text-white-50">Правила</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Создание правила</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>
            Создание правила для сайта &quot;<strong>{{ $site->name }}</strong>&quot;
        </h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('sites.rules.store', ['site' => $site]) }}" method="post">
            <div class="form-group row">
                <label for="group_id-select" class="col-sm-2 col-form-label">Группа</label>
                <div class="col-sm-10">
                    <select name="group_id" id="group_id-select" class="form-control">
                        <option value="">-- Без группы --</option>
                        @foreach($groups as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="publish-input">Состояние</label>
                <div class="col-sm-10">
                    <input type="hidden" name="publish" value="0">
                    <input type="checkbox"
                           name="publish"
                           value="1"
                           id="publish-input"
                           class="status-change">
                    <label class="form-control" for="publish-input"></label>
                </div>
            </div>
            <div class="form-group row">
                <label for="form_name-input" class="col-sm-2 col-form-label">Название формы</label>
                <div class="col-sm-10">
                    <input type="text" name="form_name" id="form_name-input" class="form-control" placeholder="Введите название формы">
                </div>
            </div>
            <div class="form-group row">
                <label for="page_url_domain-input" class="col-sm-2 col-form-label">Домен</label>
                <div class="col-sm-10">
                    <input type="text" name="page_url_domain" id="page_url_domain-input" class="form-control" placeholder="Введите домен">
                </div>
            </div>
            <div class="form-group row">
                <label for="page_url_path-input" class="col-sm-2 col-form-label">URL-путь</label>
                <div class="col-sm-10">
                    <input type="text" name="page_url_path" id="page_url_path-input" class="form-control" placeholder="Введите URL-путь">
                </div>
            </div>
            <div class="form-group row">
                <label for="page_title-input" class="col-sm-2 col-form-label">Заголовок страницы</label>
                <div class="col-sm-10">
                    <input type="text" name="page_title" id="page_title-input" class="form-control" placeholder="Введите заголовок страницы">
                </div>
            </div>
            <div class="form-group row">
                <label for="utm_source-input" class="col-sm-2 col-form-label">
                    <strong>UTM SOURCE</strong> начинается с
                </label>
                <div class="col-sm-10">
                    <input type="text" name="utm_source" id="utm_source-input" class="form-control" placeholder="Введите UTM SOURCE">
                </div>
            </div>
            <div class="form-group row">
                <label for="utm_content-input" class="col-sm-2 col-form-label">
                    <strong>UTM CONTENT</strong> начинается с
                </label>
                <div class="col-sm-10">
                    <input type="text" name="utm_content" id="utm_content-input" class="form-control" placeholder="Введите UTM CONTENT">
                </div>
            </div>
            <div class="form-group row">
                <label for="utm_campaign-input" class="col-sm-2 col-form-label">
                    <strong>UTM CAMPAIGN</strong> начинается с
                </label>
                <div class="col-sm-10">
                    <input type="text" name="utm_campaign" id="utm_campaign-input" class="form-control" placeholder="Введите UTM CAMPAIGN">
                </div>
            </div>
            <div class="form-group row">
                <label for="utm_medium-input" class="col-sm-2 col-form-label">
                    <strong>UTM MEDIUM</strong> начинается с
                </label>
                <div class="col-sm-10">
                    <input type="text" name="utm_medium" id="utm_medium-input" class="form-control" placeholder="Введите UTM MEDIUM">
                </div>
            </div>
            <div class="form-group row">
                <label for="sub_id-input" class="col-sm-2 col-form-label">
                    <strong>SUB ID</strong> начинается с
                </label>
                <div class="col-sm-10">
                    <input type="text" name="sub_id" id="sub_id-input" class="form-control" placeholder="Введите SUB ID">
                </div>
            </div>
            <div class="row">
                <div class="offset-sm-2 col-sm-10">
                    <input type="hidden" name="empty_rules" id="empty_rules" class="form-control" disabled>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Создать</button>
                    @can('rule.list', [$site])
                        <a href="{{ route('sites.rules.index', ['site' => $site]) }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection