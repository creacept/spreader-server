<?php
/** @var \App\Models\Project $project */
/** @var \App\Models\Site $site */
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Rule[] $rules */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Правила@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('site.list', [$project])
            <a href="{{ route('projects.sites.index', ['project' => $project]) }}">Сайты</a>
        @else
            <span class="text-white-50">Сайты</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Правила</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>
            Список правила для сайта &quot;<strong>{{ $site->name }}</strong>&quot;
        </h3>
        @can('rule.create', [$site])
            <a href="{{ route('sites.rules.create', ['site' => $site]) }}" class="btn btn-primary">Добавить правило</a>
        @else
            <span class="btn btn-secondary">Добавить правило</span>
        @endcan
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Состояние</th>
                <th scope="col">ID</th>
                <th scope="col">Группа</th>
                <th scope="col">Условия</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody id="rule-sortable">
                @foreach($rules as $rule)
                    <tr id="rule-{{ $rule->id }}" @can('rule.update', [$site, $rule]) data-url="{{ route('sites.rules.reorder', ['site' => $site, 'rule' => $rule]) }}" @endcan>
                        <td @can('rule.update', [$site, $rule]) class="rule-sortable-handle" @endcan>
                            <i class="fas fa-arrows-alt-v fa-lg @cannot('rule.update', [$site, $rule]) text-secondary @endcannot"></i>
                        </td>
                        <td>
                            @can('rule.update', [$site, $rule])
                                <input type="checkbox"
                                       id="rule-publish-{{ $rule->id }}"
                                       class="status-change ajax"
                                       data-url="{{ route('sites.rules.publish', ['site' => $site, 'rule' => $rule]) }}"
                                       @if($rule->publish) checked @endif>
                                <label class="ajax" for="rule-publish-{{ $rule->id }}" data-name="{{ $rule->id }}"></label>
                            @else
                                <input type="checkbox"
                                       id="rule-publish-{{ $rule->id }}"
                                       class="status-change"
                                       disabled
                                       @if($rule->publish) checked @endif>
                                <label for="rule-publish-{{ $rule->id }}"></label>
                            @endcan
                        </td>
                        <th scope="row">{{ $rule->id }}</th>
                        <td>
                            @if($rule->group === null)
                                <span class="p-1 bg-warning">
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                    БЕЗ ГРУППЫ
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                </span>
                            @else
                                <span class="p-1 @if($rule->group->publish) bg-primary @else bg-secondary @endif text-white">{{ $rule->group->name }}</span>
                            @endif
                        </td>
                        <td>
                            @if(is_string($rule->form_name))
                                <p class="mb-0">
                                    <span class="pr-1">Название формы:</span>
                                    <span>{{ $rule->form_name }}</span>
                                </p>
                            @endif
                            @if(is_string($rule->page_url_domain))
                                <p class="mb-0">
                                    <span class="pr-1">Домен:</span>
                                    <span>{{ $rule->page_url_domain }}</span>
                                </p>
                            @endif
                            @if(is_string($rule->page_url_path))
                                <p class="mb-0">
                                    <span class="pr-1">URL-путь:</span>
                                    <span>{{ $rule->page_url_path }}</span>
                                </p>
                            @endif
                            @if(is_string($rule->page_title))
                                <p class="mb-0">
                                    <span class="pr-1">Заголовок страницы:</span>
                                    <span>{{ $rule->page_title }}</span>
                                </p>
                            @endif
                            @if(is_string($rule->utm_source))
                                <p class="mb-0">
                                    <span class="pr-1">
                                        <strong>UTM SOURCE</strong> начинается с:
                                    </span>
                                    <span>{{ $rule->utm_source }}</span>
                                </p>
                            @endif
                            @if(is_string($rule->utm_content))
                                <p class="mb-0">
                                    <span class="pr-1">
                                        <strong>UTM CONTENT</strong> начинается с:
                                    </span>
                                    <span>{{ $rule->utm_content }}</span>
                                </p>
                            @endif
                            @if(is_string($rule->utm_campaign))
                                <p class="mb-0">
                                    <span class="pr-1">
                                        <strong>UTM CAMPAIGN</strong> начинается с:
                                    </span>
                                    <span>{{ $rule->utm_campaign }}</span>
                                </p>
                            @endif
                            @if(is_string($rule->utm_medium))
                                <p class="mb-0">
                                    <span class="pr-1">
                                        <strong>UTM MEDIUM</strong> начинается с:
                                    </span>
                                    <span>{{ $rule->utm_medium }}</span>
                                </p>
                            @endif
                            @if(is_string($rule->sub_id))
                                <p class="mb-0">
                                    <span class="pr-1">
                                        <strong>SUB ID</strong> начинается с:
                                    </span>
                                    <span>{{ $rule->sub_id}}</span>
                                </p>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group" role="group">
                                @can('rule.update', [$site, $rule])
                                    <a href="{{ route('sites.rules.edit', ['site' => $site, 'rule' => $rule]) }}" class="btn btn-outline-primary">
                                        <i class="far fa-edit"></i>
                                    </a>
                                @else
                                    <span class="btn btn-outline-secondary disabled">
                                        <i class="far fa-edit"></i>
                                    </span>
                                @endcan

                                @can('rule.delete', [$site, $rule])
                                    <button type="button"
                                            class="btn btn-outline-danger ajax-delete"
                                            data-url="{{ route('sites.rules.destroy', ['site' => $site, 'rule' => $rule]) }}"
                                            data-target="#rule-{{ $rule->id }}"
                                            data-name="{{ $rule->id }}">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @else
                                    <button type="button" class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
