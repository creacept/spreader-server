<?php /** @var \App\Models\CRM $crm */ ?>

@extends('layouts.start')

@section('start.title')Авторизация@endsection

@section('start.content')
    <div class="choose-project">
        <div class="authorization-wrapper">
            @if (session()->has("authorization:{$crm->id}"))
                <div class="alert alert-success" role="alert">
                    {{ session()->get("authorization:{$crm->id}") }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    Ошибка! {{ $errors->first() }}
                </div>
            @endif
            <a class="btn btn-primary btn-block" href="{{ route('authorization.start', ['crm' => $crm]) }}">Авторизация</a>
            <p>
                <b>
                    Внимание!<br>
                    Если вы авторизованы в Битрикс24, то обработка будет производится от вашего пользователя. Чтобы сменить пользователя откройте эту страницу в режиме инкогнито.
                </b>
            </p>
            <p>
                Ссылка на ваше приложение в Битрикс24: <b>{{ route('authorization.finish', ['crm' => $crm]) }}</b>
            </p>
            <a href="{{ route('authorization') }}" class="btn btn-secondary">Отмена</a>
        </div>
    </div>
@endsection