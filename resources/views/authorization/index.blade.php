<?php /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\CRM[] $collection */ ?>

@extends('layouts.start')

@section('start.title')Авторизация@endsection

@section('start.content')
    <div class="choose-project">
        <ul class="list-group">
            <li class="list-group-item active">
                CRM
                <a href="{{ route('projects.index') }}" class="btn btn-light">
                    Назад к проектам
                </a>
            </li>
            @foreach($collection as $crm)
                <li class="list-group-item">
                    {{ $crm->name }}

                    @if(array_get($crm->config, 'auth.type') === \App\Models\CRM::AUTH_OAUTH2)
                        <a href="{{ route('authorization.show', ['crm' => $crm]) }}" class="btn btn-success">
                            Авторизоваться
                        </a>
                    @else
                        <span class="btn btn-secondary disabled">
                            Авторизоваться
                        </span>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
@endsection