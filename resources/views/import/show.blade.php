<?php
/** @var \App\Models\Project $project */
/** @var \App\Models\Import $import */
/** @var \App\Models\Application[]|\App\Models\ImportError[] $rows */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Просмотр импорта@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('import.list', [$project])
            <a href="{{ route('projects.imports.index', ['project' => $project]) }}">Импорты</a>
        @else
            <span class="text-white-50">Импорты</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Результат импорта</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Результат импорта #{{ $import->id }}</h3>
    </div>
    <div class="content-table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" class="text-nowrap">Название лида</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">ФИО</th>
                    <th scope="col" class="text-nowrap">E-mail</th>
                    <th scope="col">ВКонтакте</th>
                    <th scope="col">Комментарий</th>
                    <th scope="col">utm_source</th>
                    <th scope="col">utm_campaign</th>
                    <th scope="col">utm_content</th>
                    <th scope="col">utm_medium</th>
                    <th scope="col">sub_id</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rows as $row)
                    @if($row instanceof \App\Models\Application)
                        <tr class="table-success">
                            <th scope="col">{{ $row->pivot->index }}</th>
                            <td>{{ $row->form_name }}</td>
                            <td>{{ $row->sender_phone }}</td>
                            <td>{{ $row->sender_name }}</td>
                            <td>{{ $row->sender_email }}</td>
                            <td>{{ $row->sender_vk }}</td>
                            <th>{{ $row->comment }}</th>
                            <td>{{ $row->utm_source }}</td>
                            <td>{{ $row->utm_campaign }}</td>
                            <td>{{ $row->utm_content }}</td>
                            <td>{{ $row->utm_medium }}</td>
                            <td>{{ $row->sub_id }}</td>
                        </tr>
                    @elseif($row instanceof \App\Models\ImportError)
                        <tr class="table-danger">
                            <th scope="col">{{ $row->index }}</th>
                            <td>{{ array_get($row->data, 'form_name') }}</td>
                            <td>{{ array_get($row->data, 'sender_phone') }}</td>
                            <td>{{ array_get($row->data, 'sender_name') }}</td>
                            <td>{{ array_get($row->data, 'sender_email') }}</td>
                            <td>{{ array_get($row->data, 'sender_vk') }}</td>
                            <td>{{ array_get($row->data, 'comment') }}</td>
                            <td>{{ array_get($row->data, 'utm_source') }}</td>
                            <td>{{ array_get($row->data, 'utm_campaign') }}</td>
                            <td>{{ array_get($row->data, 'utm_content') }}</td>
                            <td>{{ array_get($row->data, 'utm_medium') }}</td>
                            <td>{{ array_get($row->data, 'sub_id') }}</td>
                        </tr>
                        <tr>
                            <td colspan="10">
                                <ul class="list-unstyled mb-0">
                                    @foreach($row->errors as $error)
                                        <li class="text-danger">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
