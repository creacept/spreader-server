<?php
/** @var \App\Models\Project $project */
/** @var \Illuminate\Pagination\Paginator $paginator */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Импорты@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item active" aria-current="page">Импорты</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Список импортов</h3>
        @can('import.create', [$project])
            <a href="{{ route('projects.imports.create', ['project' => $project]) }}" class="btn btn-primary">Добавить импорт</a>
        @else
            <span class="btn btn-secondary disabled">Добавить импорт</span>
        @endcan
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Создан</th>
                    <th scope="col">Пользователь</th>
                    <th scope="col">Сайт</th>
                    <th scope="col">Состояние</th>
                    <th scope="col">Приоритет</th>
                    <th scope="col">Результат</th>
                    <th scope="col">ID</th>
                    <th scope="col">Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($paginator->items() as $import)
                    <?php /** @var App\Models\Import $import */ ?>
                    <tr>
                        <td>
                            <span class="text-nowrap">{{ $import->created_at }}</span>
                        </td>
                        <td>
                            @php $user = $import->user; @endphp
                            @if($user->trashed())
                                <s>{{ $user->name }}</s>
                            @else
                                <span>{{ $user->name }}</span>
                            @endif
                        </td>
                        <td>
                            @php $site = $import->site; @endphp
                            @if($site->trashed())
                                <s>{{ $site->name }}</s>
                            @else
                                <span class="p-1 @if($site->publish) bg-primary @else bg-secondary @endif text-white">{{ $site->name }}</span>
                            @endif
                        </td>
                        <td>
                            @if($import->status === \App\Models\Import::PENDING_STATUS)
                                <span class="badge badge-secondary">Ожидание обработки</span>
                            @elseif($import->status === \App\Models\Import::PROCESSING_STATUS)
                                <span class="badge badge-primary">Обрабатывается</span>
                            @elseif($import->status === \App\Models\Import::COMPLETED_STATUS)
                                <span class="badge badge-success">Завершено</span>
                            @elseif($import->status === \App\Models\Import::ERROR_STATUS)
                                <a href="#"
                                   class="badge badge-danger"
                                   data-container="body"
                                   data-toggle="popover"
                                   data-trigger="focus"
                                   data-placement="left"
                                   data-content="{{ $import->error_text }}">Ошибка!</a>
                            @else
                                <span class="badge badge-warning">
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                    Неопределён
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                </span>
                            @endif
                        </td>
                        <td>@if($import->priority === \App\Models\Import::PRIORITY_NORMAL)
                                <a href="#"
                                   class="badge badge-secondary"
                                   data-container="body"
                                   data-toggle="popover"
                                   data-trigger="focus"
                                   data-placement="left"
                                   data-content="Импорт обрабатывается, если в данный момент заявок с сайтов нет">Обычный</a>
                            @elseif($import->priority === \App\Models\Import::PRIORITY_HIGH || $import->priority === null)
                                <a href="#"
                                   class="badge badge-primary"
                                   data-container="body"
                                   data-toggle="popover"
                                   data-trigger="focus"
                                   data-placement="left"
                                   data-content="Импорт обрабатывается одновременно с заявками с сайтов">Высокий</a>
                            @endif
                        </td>
                        <td>
                            <span class="text-success">{{ $import->applications_count }}</span>
                            /
                            <span class="text-danger">{{ $import->errors_count }}</span>
                            /
                            {{ $import->applications_count + $import->errors_count}}
                        </td>
                        <th scope="row">{{ $import->id }}</th>
                        <td>
                            @can('import.view', [$project, $import])
                                <a href="{{ route('projects.imports.show', ['project' => $project, 'import' => $import]) }}" class="btn btn-outline-primary" target="_blank">
                                    <i class="far fa-eye"></i>
                                </a>
                            @else
                                <span class="btn btn-outline-secondary disabled">
                                    <i class="far fa-eye"></i>
                                </span>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $paginator->links() }}
@endsection