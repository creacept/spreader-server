<?php
/** @var \App\Models\Project $project */
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Site[] $sites */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Создание импорта@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('import.list', [$project])
            <a href="{{ route('projects.imports.index', ['project' => $project]) }}">Импорты</a>
        @else
            <span class="text-white-50">Импорты</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Создание импорта</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Создание импорта</h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('projects.imports.store', ['project' => $project]) }}" method="post">
            <div class="form-group row">
                <label for="site_id-select" class="col-sm-2 col-form-label">Сайт</label>
                <div class="col-sm-10">
                    <select name="site_id" id="site_id-select" class="form-control">
                        <option value="">-- Выберите значение --</option>
                        @foreach($sites as $site)
                            <option class="@if($site->publish) text-primary @else text-secondary @endif" value="{{ $site->id }}">{{ $site->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="priority-select" class="col-sm-2 col-form-label">Приоритет заявок</label>
                <div class="col-sm-10">
                    <select name="priority" id="priority-select" class="form-control">
                        <option value="{{ \App\Models\Import::PRIORITY_NORMAL }}" selected>Обычный</option>
                        <option value="{{ \App\Models\Import::PRIORITY_HIGH }}">Высокий</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="file-input" class="col-sm-2 col-form-label">Файл</label>
                <div class="col-sm-10">
                    <div>
                        <input type="file" name="file" id="file-input" class="form-control form-control-file" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    </div>
                    <a href="{{ route('projects.imports.pattern', ['project' => $project]) }}">Скачать шаблон</a>
                </div>
            </div>
            @if(count($moodleGroups) > 0)
                <div class="form-group row">
                    <label for="moodle-import" class="col-sm-2 col-form-label">Добавить пользователей в курс Moodle</label>
                    <div class="col-sm-10">
                        <select name="moodle_course" id="moodle_course-select" class="form-control">
                            <option value="">-- Выберите курс/группу --</option>
                            <option value="{{ \App\Helpers\Service\MoodleService::ONLY_REGISTRATION_LABEL }}:">Регистрация без добавления в курс</option>
                            @foreach($moodleGroups as $moodleGroup)
                                <option value="{{ $moodleGroup['value'] }}">{{ $moodleGroup['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Создать</button>
                    @can('import.list', [$project])
                        <a href="{{ route('projects.imports.index', ['project' => $project]) }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection
