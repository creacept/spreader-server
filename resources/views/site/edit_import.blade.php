<?php
/** @var \App\Models\Project $project */
/** @var \App\Models\Site $site */
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Group[] $groups */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Редактирование сайта@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item">
        @can('site.list', [$project])
            <a href="{{ route('projects.sites.index', ['project' => $project]) }}">Сайты</a>
        @else
            <span class="text-white-50">Сайты</span>
        @endcan
    </li>
    <li class="breadcrumb-item active" aria-current="page">Редактирование импорта</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>
            Редактирование импорта &quot;<strong>{{ $site->name }}</strong>&quot;
        </h3>
    </div>
    <div class="content-form">
        <form id="admin-form" action="{{ route('projects.sites.update_import', ['project' => $project, 'site' => $site]) }}" method="post">
            @method('put')
            <div class="form-group row">
                <label for="name-input" class="col-sm-2 col-form-label">Название</label>
                <div class="col-sm-10">
                    <input type="text" name="name" value="{{ $site->name }}" id="name-input" class="form-control" placeholder="Введите новое название импорта">
                </div>
            </div>
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Группы</legend>
                    <div class="col-sm-10">
                        @foreach($groups as $group)
                            <div class="form-check">
                                <input type="checkbox"
                                       name="group_id[{{ $group->id }}]"
                                       value="{{ $group->id }}"
                                       id="group_id-{{ $group->id }}-input"
                                       class="form-check-input"
                                       @if($site->groups->contains($group)) checked @endif>
                                <label class="form-check-label @if($group->publish) text-primary @else text-secondary @endif"
                                       for="group_id-{{ $group->id }}-input">{{ $group->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </fieldset>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="publish-input">Состояние</label>
                <div class="col-sm-10">
                    <input type="hidden" name="publish" value="0">
                    <input type="checkbox"
                           name="publish"
                           value="1"
                           id="publish-input"
                           class="status-change"
                           @if($site->publish) checked @endif>
                    <label class="form-control" for="publish-input"></label>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Изменить</button>
                    @can('site.list', [$project])
                        <a href="{{ route('projects.sites.index', ['project' => $project]) }}" class="btn">Отмена</a>
                    @else
                        <span class="btn disabled">Отмена</span>
                    @endcan
                </div>
            </div>
        </form>
    </div>
@endsection