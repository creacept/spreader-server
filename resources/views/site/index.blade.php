<?php
/** @var \App\Models\Project $project */
/** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Site[] $sites */
?>

@extends('layouts.sidebar', ['project' => $project])

@section('sidebar.title')Сайты@endsection

@section('sidebar.breadcrumbs')
    <li class="breadcrumb-item active" aria-current="page">Сайты</li>
@endsection

@section('sidebar.content')
    <div class="content-header">
        <h3>Список сайтов</h3>
        <div class="dropdown">
            @can('site.create', [$project])
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Добавить</button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{{ route('projects.sites.create', ['project' => $project]) }}" class="dropdown-item">Сайт</a>
                    <a href="{{ route('projects.sites.create_import', ['project' => $project]) }}" class="dropdown-item">Импорт</a>
                </div>
            @else
                <button type="button" class="btn btn-secondary dropdown-toggle" disabled>Добавить</button>
            @endcan
        </div>
    </div>
    <div class="content-table">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Состояние</th>
                <th scope="col">Название</th>
                <th scope="col">Группы</th>
                <th scope="col">Тип</th>
                <th scope="col">ID</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
                @foreach($sites as $site)
                    <tr id="site-{{ $site->id }}">
                        <td>
                            @can('site.update', [$project, $site])
                                <input type="checkbox"
                                       id="site-publish-{{ $site->id }}"
                                       class="status-change ajax"
                                       data-url="{{ route('projects.sites.publish', ['project' => $project, 'site' => $site]) }}"
                                       @if($site->publish) checked @endif>
                                <label class="ajax" for="site-publish-{{ $site->id }}" data-name="{{ $site->name }}"></label>
                            @else
                                <input type="checkbox"
                                       id="site-publish-{{ $site->id }}"
                                       class="status-change"
                                       disabled
                                       @if($site->publish) checked @endif>
                                <label for="site-publish-{{ $site->id }}"></label>
                            @endcan
                        </td>
                        <td>
                            @can('rule.list', [$site])
                                <a href="{{ route('sites.rules.index', ['site' => $site]) }}">{{ $site->name }}</a>
                            @else
                                <span class="text-secondary">{{ $site->name }}</span>
                            @endcan
                        </td>
                        <td>
                            @foreach ($site->groups as $group)
                                <span class="p-1 @if($group->publish) bg-primary @else bg-secondary @endif text-white">{{ $group->name }}</span>
                            @endforeach
                        </td>
                        <td>
                            @if($site->type === \App\Models\Site::SITE_TYPE)
                                <span class="badge badge-success">Сайт</span>
                            @elseif($site->type === \App\Models\Site::IMPORT_TYPE)
                                <span class="badge badge-warning">Импорт</span>
                            @else
                                <span class="badge badge-danger">
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                    Неопределён
                                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                                </span>
                            @endif
                        </td>
                        <th scope="row">{{ $site->id }}</th>
                        <td>
                            <div class="btn-group" role="group">
                                @can('site.update', [$project, $site])
                                    @if($site->type === \App\Models\Site::SITE_TYPE)
                                        <a href="{{ route('projects.sites.edit', ['project' => $project, 'site' => $site]) }}" class="btn btn-outline-primary">
                                            <i class="far fa-edit"></i>
                                        </a>
                                    @elseif($site->type === \App\Models\Site::IMPORT_TYPE)
                                        <a href="{{ route('projects.sites.edit_import', ['project' => $project, 'site' => $site]) }}" class="btn btn-outline-primary">
                                            <i class="far fa-edit"></i>
                                        </a>
                                    @else
                                        <span class="btn btn-outline-secondary disabled">
                                            <i class="far fa-edit"></i>
                                        </span>
                                    @endif
                                @else
                                    <span class="btn btn-outline-secondary disabled">
                                        <i class="far fa-edit"></i>
                                    </span>
                                @endcan

                                @can('site.delete', [$project, $site])
                                    <button type="button"
                                            class="btn btn-outline-danger ajax-delete"
                                            data-url="{{ route('projects.sites.destroy', ['project' => $project, 'site' => $site]) }}"
                                            data-target="#site-{{ $site->id }}"
                                            data-name="{{ $site->name }}">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @else
                                    <button type="button" class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
