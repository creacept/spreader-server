<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateImportApplicationTable
 */
class CreateImportApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_application', function (Blueprint $table) {
            $table->unsignedInteger('application_id')->primary();
            $table->unsignedInteger('import_id');
            $table->unsignedSmallInteger('index');
            $table->unique(['import_id', 'index']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_application');
    }
}
