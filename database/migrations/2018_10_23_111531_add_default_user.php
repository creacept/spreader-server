<?php

use Illuminate\Database\Migrations\Migration;

/**
 * Class AddDefaultUser
 */
class AddDefaultUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert([
            'name' => 'root',
            'email' => 'root@creacept.ru',
            'password' => \Hash::make('0123456789abcdef'),
            'publish' => true,
            'super_admin' => true,
            'permissions' => '[]',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')
            ->where('email', 'root@creacept.ru')
            ->delete();
    }
}
