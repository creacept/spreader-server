<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddImportIdForeignKeyForImportErrorTable
 */
class AddImportIdForeignKeyForImportErrorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('import_error', function (Blueprint $table) {
            $table->foreign(['import_id'])->references('id')->on('imports')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('import_error', function (Blueprint $table) {
            $table->dropForeign(['import_id']);
        });
    }
}
