<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddRatioAndApplicationNumberColumnsForManagersTable
 */
class AddRatioAndApplicationNumberColumnsForManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('managers', function (Blueprint $table) {
            $table->unsignedInteger('ratio')->default(100);
            $table->unsignedInteger('application_number')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('managers', function (Blueprint $table) {
            $table->dropColumn('ratio');
            $table->dropColumn('application_number');
        });
    }
}
