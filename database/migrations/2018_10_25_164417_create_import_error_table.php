<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateImportErrorTable
 */
class CreateImportErrorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_error', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('import_id');
            $table->unsignedSmallInteger('index');
            $table->text('data');
            $table->text('errors');
            $table->unique(['import_id', 'index']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_error');
    }
}
