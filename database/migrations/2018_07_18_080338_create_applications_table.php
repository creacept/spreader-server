<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('submitted_at')->useCurrent();
            $table->unsignedInteger('site_id');
            $table->unsignedInteger('source_id');
            $table->tinyInteger('status')->default(0);
            $table->timestamp('handled_at')->useCurrent();
            $table->string('sender_phone');
            $table->string('sender_email')->nullable();
            $table->string('sender_name')->nullable();
            $table->string('form_name');
            $table->string('page_url');
            $table->string('page_title');
            $table->string('utm_source')->nullable();
            $table->string('utm_content')->nullable();
            $table->string('utm_campaign')->nullable();
            $table->string('utm_medium')->nullable();
            $table->string('sub_id')->nullable();
            $table->unique(['site_id', 'source_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
