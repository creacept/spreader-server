<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvecallsImportApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evecallsImport_application', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('evecallsImport_id');
            $table->unsignedInteger('application_id')->unique();
            $table->timestamps();

            $table->foreign('evecallsImport_id')
                ->references('id')
                ->on('evecallsImports')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('application_id')
                ->references('id')
                ->on('applications')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evecallsImport_application', function (Blueprint $table) {
            $table->dropForeign(['evecallsImport_id']);
            $table->dropForeign(['application_id']);
        });
        Schema::dropIfExists('evecallsImport_application');
    }
}
