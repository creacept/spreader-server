<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddApplicationIdForeignKeyForImportApplicationTable
 */
class AddApplicationIdForeignKeyForImportApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('import_application', function (Blueprint $table) {
            $table->foreign(['application_id'])->references('id')->on('applications')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('import_application', function (Blueprint $table) {
            $table->dropForeign(['application_id']);
        });
    }
}
