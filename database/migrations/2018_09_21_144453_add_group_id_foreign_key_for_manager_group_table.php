<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddGroupIdForeignKeyForManagerGroupTable
 */
class AddGroupIdForeignKeyForManagerGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manager_group', function (Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manager_group', function (Blueprint $table) {
            $table->dropForeign(['group_id']);
        });
    }
}
