<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddGroupIdForeignKeyForGroupSiteTable
 */
class AddGroupIdForeignKeyForGroupSiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('group_site', function (Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('group_site', function (Blueprint $table) {
            $table->dropForeign(['group_id']);
        });
    }
}
