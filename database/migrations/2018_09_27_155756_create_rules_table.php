<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRulesTable
 */
class CreateRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('rules', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('site_id');
            $table->boolean('publish')->default(false);
            $table->unsignedInteger('priority')->default(0);
            $table->unsignedInteger('group_id')->nullable();
            $table->string('form_name')->nullable();
            $table->string('page_url_domain')->nullable();
            $table->string('page_url_path')->nullable();
            $table->string('page_title')->nullable();
            $table->string('utm_source')->nullable();
            $table->string('utm_content')->nullable();
            $table->string('utm_campaign')->nullable();
            $table->string('utm_medium')->nullable();
            $table->string('sub_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('rules');
    }
}
