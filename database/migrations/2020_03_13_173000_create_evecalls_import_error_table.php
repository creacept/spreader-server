<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateEvecallsImportErrorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evecallsImport_error', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('evecallsImport_id');
            $table->text('data');
            $table->text('errors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_error');
    }
}
