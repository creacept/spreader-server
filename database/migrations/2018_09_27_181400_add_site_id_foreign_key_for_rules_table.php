<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddSiteIdForeignKeyForRulesTable
 */
class AddSiteIdForeignKeyForRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('rules', function (Blueprint $table) {
            $table->foreign('site_id')->references('id')->on('sites')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('rules', function (Blueprint $table) {
            $table->dropForeign(['site_id']);
        });
    }
}
